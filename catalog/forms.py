# coding=utf-8
from datetime import date

from django import forms
from django.core.urlresolvers import reverse_lazy

from catalog.models import CarText, CarBrand, CarModification, \
    CarModel, Part, AC
from cabinet.widgets import YearWidget

from chosen_complete import registry

from catalog.chosen_autocomplete import CarBrandAutocomplete, \
    CarModelAutocomplete, CarModificationAutocomplete, \
    ACSearchAutocomplete


class ACSearchForm(forms.Form):
    search_query = forms.ModelChoiceField(queryset=Part.objects.none(),
                                          label='',
                                          required=False)

    def __init__(self, *args, **kwargs):
        super(ACSearchForm, self).__init__(*args, **kwargs)

        url = registry.get_url_for_autocomplete(ACSearchAutocomplete)
        self.fields['search_query'].widget.attrs = {
            'data-cacomplete-url': url,
            'data-only_update': 'True',
            'data-use_acsearch_template': 'True',
            'multiple': 'multiple'
        }


class ACSearchCategoryForm(forms.Form):
    # rename with catalog:chosen_automocomplete:24
    categories = forms.CharField(widget=forms.HiddenInput(), required=False)

    def __init__(self, *args, **kwargs):
        super(ACSearchCategoryForm, self).__init__(*args, **kwargs)


class SearchCarForm(forms.Form):
    # ToDo: maybe, add slug field to CarTextType model (and similar) ?
    # then you can filter query in this way CarText.objects.filter(cartexttype__slug_name='body_type')
    body = forms.ModelChoiceField(queryset=CarText.objects.filter(cartexttype=9),
                                  label=u'Кузов', required=False)
    # engine_volume = forms.IntegerField(required=False)
    fuel = forms.ModelChoiceField(queryset=CarText.objects.filter(cartexttype=7),
                                  label=u'Топливо', required=False)


class FastCatalogForm(forms.Form):
    date_production = forms.DateField(
        widget=YearWidget(years=range(date.today().year, 1975, -1),
                          attrs={'class': 'set-chosen'}),
        label=u'Выберите год выпуска автомобиля',
        required=False)

    car_brand = forms.ModelChoiceField(queryset=CarBrand.objects.none(),
                                       label=u'Марка автомобиля',
                                       required=False)

    car_model = forms.ModelChoiceField(queryset=CarModel.objects.none(),
                                       label=u'Модель автомобиля (выбранной марки)',
                                       required=False)

    car_modification = forms.ModelChoiceField(
        queryset=CarModification.objects.none(),
        required=False,
        label=u'Модификация автомобиля (выбранной модели)',
        widget=forms.Select(
            attrs={'data-car_get_prepopulated_url':
                   reverse_lazy('cabinet:client:car_get_prepopulated')}))

    class Meta:
        cacomplete_list = ('car_model', 'car_brand', 'car_modification')

    def __init__(self, *args, **kwargs):
        super(FastCatalogForm, self).__init__(*args, **kwargs)

        url = registry.get_url_for_autocomplete(CarBrandAutocomplete)
        self.fields['car_brand'].widget.attrs = {
            'data-cacomplete-url': url,
            'data-only_update': 'true'
        }

        url = registry.get_url_for_autocomplete(CarModelAutocomplete)
        self.fields['car_model'].widget.attrs = {
            'data-cacomplete-url': url,
            'data-only_update': 'true'
        }

        url = registry.get_url_for_autocomplete(CarModificationAutocomplete)
        self.fields['car_modification'].widget.attrs = {
            'data-cacomplete-url': url,
            'data-only_update': 'true'
        }


class CallBackForm(forms.Form):
    title = u"Заказать обратный звонок"

    price = forms.CharField(widget=forms.HiddenInput(), required=False)
    part_id = forms.CharField(widget=forms.HiddenInput(), required=False)
    spl_id = forms.CharField(widget=forms.HiddenInput(), required=False)
    delivery = forms.CharField(widget=forms.HiddenInput(), required=False)
    mod_id = forms.CharField(widget=forms.HiddenInput(), required=False)

    name = forms.CharField(label=u'Ваше имя',
                           required=True)

    phone = forms.CharField(label=u'Ваш телефон',
                            required=True)
