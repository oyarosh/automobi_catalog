# -*- coding: utf-8 -*-
import os
import datetime
import pytils
import unicodecsv as csv
from decimal import Decimal


from django.db import models
from django.db.models import signals
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic

from mptt.models import MPTTModel, TreeForeignKey

from cabinet.mixins import SerializationMixin, SerializationQueryset,\
    ApiForeignKey
from superuser.models import Settings
from core.helpers import get_translit_filename
from core.models import ItemsLogApiObject

from catalog.exceptions import InvalidWeightSizeSpecification


class TradeMark(ItemsLogApiObject, SerializationMixin, models.Model):
    show_name = models.CharField(
        u"Название Торговой Марки", max_length=255,
        db_index=True)
    full_name = models.CharField(u"Полное Название", max_length=255)
    short_name = models.CharField(max_length=255, null=True)
    oe = models.BooleanField(default=False)
    tec_doc_id = models.IntegerField(null=True)
    created_by_id = models.IntegerField(blank=True, null=True)

    SERIALIZATION_FIELDS = ('id', 'full_name')

    SHARED_DATABASE = 'api'

    def __unicode__(self):
        return u"%s" % (self.show_name)

    class Meta:
        verbose_name = u"торговая марка"
        verbose_name_plural = u"Торговые марки"


class EngineTextType(models.Model):
    enginetexttype = models.CharField(u"Описание типа свойства для двигателя",
                                      max_length=255)

    def __unicode__(self):
        return u"%s" % (self.enginetexttype)

    class Meta:
        verbose_name = u"тип свойства для двигателя"
        verbose_name_plural = u"Типы свойства для двигателей"


class EngineText(models.Model):
    enginetext = models.CharField(u"Описание", max_length=255)
    enginetexttype = models.ForeignKey(
        EngineTextType,
        verbose_name=u"Тип Свойства для двигателя")

    def __unicode__(self):
        return u"%s" % (self.enginetext)

    class Meta:
        verbose_name = u"описание свойства для двигателя"
        verbose_name_plural = u"Описания свойств для двигателя"


class EngineModification(models.Model):
    car_brand = models.ForeignKey('catalog.CarBrand',
                                  verbose_name=u"Торговая марка")
    engcode = models.CharField(u"Код двигателя", max_length=255)
    setup_startdate = models.DateField(
        verbose_name=u"Дата Начала Производства",
        blank=True, null=True)
    setup_enddate = models.DateField(
        verbose_name=u"Дата Окончания Производства",
        blank=True, null=True)
    kw_power_from = models.PositiveIntegerField(u"Мощность от (kW)",
                                                blank=True, null=True)
    kw_power_till = models.PositiveIntegerField(u"Мощность до (kW)",
                                                blank=True, null=True)
    hp_power_from = models.PositiveIntegerField(u"Мощность от (HP)",
                                                blank=True, null=True)
    hp_power_till = models.PositiveIntegerField(u"Мощность до (HP)",
                                                blank=True, null=True)
    valves_num = models.PositiveIntegerField(u"Кол-во Клапанов", blank=True,
                                             null=True)
    cylinders_num = models.PositiveIntegerField(u"Кол-во Цилиндров",
                                                blank=True, null=True)
    engine_volume = models.PositiveIntegerField(u"Объем Двигателя (см3)",
                                                blank=True, null=True)
    piston_diameter = models.PositiveIntegerField(u"Диаметр Поршня",
                                                  blank=True, null=True)
    stroke = models.PositiveIntegerField(u"Ход Поршня", blank=True, null=True)
    crankshaft_number = models.PositiveIntegerField(
        u"Кол-во Подшипников на Коленвале",
        blank=True, null=True)
    cylpos = models.ForeignKey('catalog.CarText',
                               verbose_name=u"Расположение Цилиндров",
                               related_name="+", blank=True, null=True)
    fuel = models.ForeignKey('catalog.CarText', verbose_name=u"Тип Топлива",
                             related_name="+", blank=True, null=True)
    injtype = models.ForeignKey('catalog.CarText',
                                verbose_name=u"Тип Подачи Топлива",
                                related_name="+", blank=True, null=True)
    fueltype = models.ForeignKey('catalog.CarText',
                                 verbose_name=u"Тип Топлива",
                                 related_name="+", blank=True, null=True)
    charge = models.ForeignKey('catalog.CarText', verbose_name=u"Тип Наддува",
                               related_name="+", blank=True, null=True)
    econorm = models.ForeignKey('catalog.CarText',
                                verbose_name=u"Экологическая Норма",
                                related_name="+", blank=True, null=True)
    camtype = models.ForeignKey('catalog.CarText',
                                verbose_name=u"Тип Системы Распредвала",
                                related_name="+", blank=True, null=True)
    camdrive = models.ForeignKey('catalog.CarText',
                                 verbose_name=u"Тип Привода Распредвала",
                                 related_name="+", blank=True, null=True)
    valvedrive = models.ForeignKey('catalog.CarText',
                                   verbose_name=u"Тип Управления Клапанами",
                                   related_name="+", blank=True, null=True)
    cooltype = models.ForeignKey('catalog.CarText',
                                 verbose_name=u"Тип Системы Охлаждения",
                                 related_name="+", blank=True, null=True)
    engremarks = models.CharField(u"Описание", max_length=255, blank=True,
                                  null=True)

    def __unicode__(self):
        return u"%s (%s)" % (self.engcode, self.id)

    class Meta:
        verbose_name = u"модификация мотора"
        verbose_name_plural = u"Модификации моторов"


class CarTextType(models.Model):
    cartexttype = models.CharField(u"Описание типа свойства", max_length=255)

    def __unicode__(self):
        return u"%s" % (self.cartexttype)

    class Meta:
        verbose_name = u"тип свойства автомобиля"
        verbose_name_plural = u"Типы свойства автомобиля"


class CarText(models.Model):
    cartext = models.CharField(u"Описание", max_length=255)
    cartexttype = models.ForeignKey(CarTextType,
                                    verbose_name=u"Тип Свойства Авто")

    def __unicode__(self):
        return u"%s" % (self.cartext)

    class Meta:
        verbose_name = u"описание свойства для автомобиля"
        verbose_name_plural = u"Описания свойств для автомобиля"


class CarBrand(models.Model):
    title = models.CharField(u"Название", max_length=255)
    title_rus = models.CharField(u"Название на русском", max_length=255,
                                 blank=True, null=True)
    slug = models.SlugField(u"URL", unique=True,
                            blank=True, null=True,
                            max_length=280)
    start_date = models.DateField(verbose_name=u"Год начала бренда",
                                  blank=True, null=True)
    end_date = models.DateField(verbose_name=u"Год конца бренда",
                                blank=True, null=True)
    is_passenger = models.BooleanField(u"Пассажирский", default=False)
    is_commercial = models.BooleanField(u"Коммерческий", default=False)
    is_axis_manufacturer = models.BooleanField(u"Ось", default=False)
    logo = models.ImageField(
        verbose_name=u"Иконка",
        upload_to="car_brand_logos",
        blank=True, null=True
    )
    is_shown = models.BooleanField(u"Показывать", default=True)
    to_main = models.BooleanField(u"Показывать на главной", default=True)

    def __unicode__(self):
        return u"%s" % self.title

    def get_general_model_title_qs(self):
        queryset = self.carmodel_set.all()

        country = Settings.objects.filter(code_name='default_tec_doc_country').first()
        if country:
            queryset = queryset.filter(model_country__country_id=country.object_id)

        queryset = queryset.extra(
            select={'title': 'catalog_carmodelcountry.title',
                    'car_model_tex_id': 'catalog_carmodelcountry.id'}
        )

        ids_qs = queryset.order_by('model_country__general_title')\
            .distinct('model_country__general_title')\
            .values_list('model_country__general_title', flat=True)

        return CarText.objects.filter(id__in=ids_qs).order_by('cartext')

    class Meta:
        verbose_name = u"марка и сегмент автомобиля"
        verbose_name_plural = u"Марки и сегменты автомобиля"


class CarModel(models.Model):
    car_brand = models.ForeignKey(CarBrand, verbose_name=u"Марка автомобиля")
    car_type_shot_key = models.CharField(u"Тип Авто", max_length=255)
    slug = models.SlugField(u"URL", unique=True,
                            blank=True, null=True,
                            max_length=280)
    tcartype = models.CharField(u"Описание",
                                max_length=255,
                                blank=True,
                                null=True)
    startdate_production = models.DateField(
        verbose_name=u"Дата Начала Производства",
        blank=True, null=True)
    enddate_production = models.DateField(
        verbose_name=u"Дата Окончания Производства",
        blank=True, null=True)
    is_passenger = models.BooleanField(u"Пассажирский", default=False)
    is_commercial = models.BooleanField(u"Коммерческий", default=False)
    is_axis_manufacturer = models.BooleanField(u"Ось", default=False)
    img = models.ImageField(u'Фото модели', upload_to='car_model_photo',
                            null=True, blank=True)

    def _get_cmc(self):
        country = Settings.objects.filter(
            code_name='default_tec_doc_country').first()
        item = CarModelCountry.objects.filter(car_model_id=self.id,
                                              country_id=country.object_id).first()
        return item

    def __unicode__(self):
        item = self._get_cmc()
        if item:
            return u"%s" % item.title
        else:
            return u"%s" % self.car_type_shot_key

    @property
    def general_title(self):
        item = self._get_cmc()
        if item.general_title:
            return item.general_title.cartext
        else:
            return item.title

    class Meta:
        verbose_name = u"модель автомобиля"
        verbose_name_plural = u"Модели автомобилей"


class CarModelCountry(models.Model):
    car_model = models.ForeignKey(CarModel, related_name="model_country")
    country = models.ForeignKey('catalog.SalesCountry')
    title = models.CharField(u"Тип Авто", max_length=255)
    general_title = models.ForeignKey(CarText, verbose_name=u"Обобщенное название",
                                      related_name="+", blank=True, null=True)

    def __unicode__(self):
        return u"%s" % self.title


class CarModification(models.Model):
    car_model = models.ForeignKey(CarModel, verbose_name=u"Модель автомобиля")
    show_name = models.CharField(u"Описание Модификации", max_length=255)
    full_name = models.CharField(u"Полное Описание", max_length=255)
    slug = models.SlugField(u"URL", unique=True,
                            blank=True, null=True,
                            max_length=280)
    engine_modifications = models.ManyToManyField(
        EngineModification,
        verbose_name=u"Модификация мотора",
        related_name="car_modifications")
    startdate_production = models.DateField(
        verbose_name=u"Дата Начала Производства",
        blank=True, null=True)
    enddate_production = models.DateField(
        verbose_name=u"Дата Окончания Производства",
        blank=True, null=True)
    kw_engine_power = models.PositiveIntegerField(u"Мощность Двигателя (kW)",
                                                  blank=True, null=True)
    hp_engine_power = models.PositiveIntegerField(u"Мощность Двигателя в (HP)",
                                                  blank=True, null=True)
    engine_volume = models.PositiveIntegerField(u"Объем Двигателя (см3)",
                                                blank=True, null=True)
    cylinders_num = models.PositiveIntegerField(u"Кол-во Цилиндров",
                                                blank=True, null=True)
    doors_num = models.PositiveIntegerField(u"Кол-во Дверей",
                                            blank=True, null=True)
    tank_volume = models.PositiveIntegerField(u"Объем Бака (л)",
                                              blank=True, null=True)
    valves_per_cylinder = models.PositiveIntegerField(u"Клапанов на Цилиндр",
                                                      blank=True, null=True)

    voltage = models.ForeignKey(CarText, verbose_name=u"Код Вольтажа",
                                related_name="+", blank=True, null=True)
    abs = models.ForeignKey(CarText, verbose_name=u"Тип ABS",
                            related_name="+", blank=True, null=True)
    asr = models.ForeignKey(CarText, verbose_name=u"Тип ASR",
                            related_name="+", blank=True, null=True)
    engine = models.ForeignKey(CarText, verbose_name=u"Тип Двигателя",
                               related_name="+", blank=True, null=True)
    braketype = models.ForeignKey(CarText,
                                  verbose_name=u"Тип Тормозной Системы",
                                  related_name="+", blank=True, null=True)
    brakesys = models.ForeignKey(CarText,
                                 verbose_name=u"Код Тормозной Системы",
                                 related_name="+", blank=True, null=True)
    fuel = models.ForeignKey(CarText, verbose_name=u"Код Топлива",
                             related_name="+", blank=True, null=True)
    catalyst = models.ForeignKey(CarText, verbose_name=u"Тип Катализатора",
                                 related_name="+", blank=True, null=True)
    body = models.ForeignKey(CarText, verbose_name=u"Тип Кузова",
                             related_name="+", blank=True, null=True)
    chassis = models.ForeignKey(CarText, verbose_name=u"Тип Шасси",
                                related_name="+", blank=True, null=True)
    axle = models.ForeignKey(CarText, verbose_name=u"Тип Оси",
                             related_name="+", blank=True, null=True)
    drive = models.ForeignKey(CarText, verbose_name=u"Код Привода",
                              related_name="+", blank=True, null=True)
    transmission = models.ForeignKey(CarText, verbose_name=u"Код Трансмиссии",
                                     related_name="+", blank=True, null=True)
    fuelin = models.ForeignKey(CarText, verbose_name=u"Код Системы Впрыска",
                               related_name="+", blank=True, null=True)

    @property
    def search_popup_title(self):
        return u'%s, %s л.с., %s' % (self.show_name, self.hp_engine_power,
                                     self.car_model.car_type_shot_key)

    def __unicode__(self):
        return u'%s, %s, %s л.с., %s, %s' % (self.show_name, self.engine_volume,
                                             self.hp_engine_power, self.engine,
                                             self.body)

    class Meta:
        verbose_name = u"модификация автомобиля"
        verbose_name_plural = u"Модификации автомобилей"


class CarModificationCountry(models.Model):
    car_modification = models.ForeignKey(CarModification, related_name="modification_country")
    country = models.ForeignKey('catalog.SalesCountry')
    title = models.CharField(u"Модификации Авто", max_length=255)

    def __unicode__(self):
        return u"%s" % self.title


class Unit(SerializationMixin, models.Model):
    created_by_content_type = models.ForeignKey(ContentType, null=True,
                                                related_name='+')
    created_by_object_id = models.PositiveIntegerField(null=True)
    created_by = generic.GenericForeignKey('created_by_content_type',
                                           'created_by_object_id')

    code = models.CharField(u"Код", max_length=255, null=True)
    title = models.CharField(u"Описание", max_length=255)
    full_name = models.CharField(u"Полное описание", max_length=255, null=True)
    translation = models.ForeignKey('core.Translation')
    tec_doc_id = models.IntegerField(null=True)

    SERIALIZATION_FIELDS = ('id', '__unicode__')

    class Meta:
        verbose_name = u"единица измерения"
        verbose_name_plural = u"Единицы измерения"

    def __unicode__(self):
        return u"%s (%s)" % (self.title, self.id)


class PartPropertyType(models.Model):
    title = models.CharField(u"Описание", max_length=255)
    unit = models.ForeignKey(Unit, verbose_name=u"Единица измерения",
                             blank=True, null=True)

    def __unicode__(self):
        return u"%s (%s)" % (self.title, self.unit)

    class Meta:
        verbose_name = u"тип характеристик запчастей"
        verbose_name_plural = u"Типы характеристик запчастей"


class PartProperty(models.Model):
    value = models.CharField(u"Значение Характеристики", max_length=255)
    part_property = models.ForeignKey(
        PartPropertyType,
        verbose_name=u"Тип характеристики запчасти",
        blank=True, null=True)

    def __unicode__(self):
        return u"%s, %s" % (self.part_property, self.value)

    class Meta:
        verbose_name = u"характеристика товара"
        verbose_name_plural = u"Характеристики товаров"


class CarTree(MPTTModel):
    title = models.CharField(u"Название", max_length=255)
    slug = models.SlugField(u"URL", unique=True,
                            blank=True, null=True,
                            max_length=280)
    parent = TreeForeignKey('self', null=True, blank=True,
                            related_name='children')
    node_id = models.PositiveIntegerField(db_index=True)
    sort = models.IntegerField(null=True)
    type = models.IntegerField(null=True)
    show_on_main = models.BooleanField(u"Показывать на главной дерева", default=False)
    show_on_menu = models.BooleanField(u"Показывать в меню", default=False)
    image = models.ImageField(
        verbose_name=u"Изображение",
        upload_to="car_tree",
        blank=True, null=True
    )

    def __unicode__(self):
        # if self.parent:
        #     return u"%s->%s" % (self.parent, self.title)
        # else:
        #     return u"%s" % (self.title)
        return u"%s" % (self.title)

    class MPTTMeta:
        order_insertion_by = ['title']

    class Meta:
        verbose_name = u"устройство автомобиля (дерево)"
        verbose_name_plural = u"Устройство Автомобиля (дерево)"


class PartSet(models.Model):
    title = models.CharField(u"Название", max_length=255)
    quantity = models.PositiveIntegerField(u"Количество",
                                           blank=True, null=True)
    # car_tree = models.ForeignKey(CarTree,
    #                              verbose_name=u"Узел дерева",
    #                              blank=True, null=True)

    def __unicode__(self):
        return u"%s (%s)" % (self.title, self.id)

    class Meta:
        verbose_name = u"комплект"
        verbose_name_plural = u"Комплекты"


class GoodsCategoryGroup(models.Model):

    """
    Defines goods categories group
    """
    name = models.CharField(u"Название группы категорий", max_length=255)
    icon = models.ImageField(
        verbose_name=u"Иконка группы",
        upload_to="goods_category_group",
        blank=True, null=True
    )

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u"группа категорий товаров"
        verbose_name_plural = u"Группы категорий товаров"


class GoodsCategory(models.Model):

    """
    Defines goods categories
    """
    name = models.CharField(u"Название категории", max_length=255)
    categoty_group = models.ForeignKey(GoodsCategoryGroup,
                                       verbose_name=u"Группа",
                                       blank=True, null=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u"Категория товара"
        verbose_name_plural = u"Категории товаров"


class CatalogGood(models.Model):

    """
    Represents standard catalog good (non part related product),
    e.g. Oil
    """
    name = models.CharField(u"Название товара", max_length=255)
    category = models.ForeignKey(GoodsCategory, verbose_name=u"Категория")
    tm = models.ForeignKey(TradeMark, verbose_name=u"Торговая марка")
    description = models.CharField(u"Описание", max_length=255,
                                   blank=True, null=True)

    def __unicode__(self):
        return u"%s" % (self.name)

    class Meta:
        verbose_name = u"Товар"
        verbose_name_plural = u"Товары"


class Part(SerializationMixin, models.Model):
    search_number = models.CharField(
        u"Номер Поиска", max_length=255, db_index=True, null=True)
    tm = ApiForeignKey(TradeMark, verbose_name=u"Торговая марка")
    slug = models.SlugField(u"URL", unique=True,
                            blank=True, null=True,
                            max_length=280)
    catalog_number = models.CharField(u"Каталожный Номер", max_length=255,
                                      db_index=True, null=True)
    description = models.TextField(u"Описание",
                                   blank=True, null=True)
    marks = models.CharField(u"Примечание", max_length=255,
                             blank=True, null=True)
    tecdoc = models.PositiveIntegerField(u"TecDoc",
                                         blank=True, null=True,
                                         db_index=True)
    part_set = models.ManyToManyField(PartSet,
                                      verbose_name=u"Комплект",
                                      blank=True, null=True)
    # car_tree = models.ManyToManyField(CarTree,
    #                                   verbose_name=u"Узлы дерева",
    #                                   blank=True, null=True)
    car_modification = models.ManyToManyField(
        CarModification,
        verbose_name=u"Модификация автомобиля",
        blank=True, null=True)
    properties = models.ManyToManyField(PartProperty,
                                        verbose_name=u"Характеристики",
                                        blank=True, null=True)

    default_description = models.ForeignKey('PartDescription', null=True)
    weight = models.DecimalField(max_digits=13, null=True,
                                 decimal_places=3, blank=True,
                                 max_length=255,
                                 verbose_name=u"Вес, кг.")
    weight_volume = models.DecimalField(max_digits=13, null=True,
                                 decimal_places=3, blank=True,
                                 max_length=255,
                                 verbose_name=u"Объемный вес, кг.")
    img_quantity = models.PositiveIntegerField(verbose_name="Image quantity",
                                               blank=True,
                                               null=True,
                                               default=0)

    SERIALIZATION_FIELDS = ('id', 'catalog_number', 'description', 'tm',
                            'barcode_set', 'default_description')

    SHARED_DATABASE = 'api'

    class Meta:
        verbose_name = u"запчасть"
        verbose_name_plural = u"Запчасти"

    def __unicode__(self):
        return u"%s %s (%s)" % (self.tm, self.catalog_number, self.id)

    def get_description(self, language_id=16):
        default_description = PartDescriptionText.objects.filter(
            part_description_id=self.default_description_id,
            language_id=language_id).first()

        if default_description:
            return default_description.value

        return self.description

    def get_info(self):
        info = ""
        info += self.tm.show_name + " " + self.catalog_number
        if self.description:
            info += ", " + self.description
        if self.marks:
            info += ", " + self.marks
        return info

    def get_specs(self, attr):
        return WeightSizeSpecification.get_by_attr(attr)

    def serialize(self):
        data = super(Part, self).serialize()
        wss = WeightSizeSpecification.objects.filter(
            tm=self.tm, catalog_number=self.catalog_number).first()
        data['wss'] = wss.serialize() if wss else None
        return data


def partimage_upload_to(instance, filename):
    return 'parts_images/part_%s/' % instance.part_id + filename


class PartImage(models.Model):
    part = ApiForeignKey(Part, verbose_name=u'Запчасть')
    image = models.ImageField(u'Файл', upload_to=partimage_upload_to)
    type = models.CharField(u"Тип", max_length=16,
                            blank=True, null=True)
    tecdoc_image = models.BooleanField(u"Из ТекДока", default=False)
    created = models.DateTimeField(
        auto_now_add=True, verbose_name=u'Время создания')

    def __unicode__(self):
        return u'%s' % self.id

    class Meta:
        verbose_name = u'Фотография'
        verbose_name_plural = u'Фотографии'

    @staticmethod
    def post_save(sender, instance, created, **kwargs):
        if created:
            part = instance.part
            img_quantity = part.img_quantity if part.img_quantity else 0
            img_quantity += 1
            part.img_quantity = img_quantity
            part.save(update_fields=['img_quantity'])

    @staticmethod
    def post_delete(sender, instance, **kwargs):
        part = instance.part
        img_quantity = part.img_quantity if part.img_quantity else 0
        if img_quantity > 0:
            img_quantity -= 1
        part.img_quantity = img_quantity
        part.save(update_fields=['img_quantity'])

signals.post_delete.connect(sender=PartImage,
                            receiver=PartImage.post_delete)

signals.post_save.connect(sender=PartImage,
                          receiver=PartImage.post_save)


class PartEngine(models.Model):
    part = models.ForeignKey(Part,
                             verbose_name=u"Запчасть",
                             blank=True, null=True)
    part_set = models.ForeignKey(PartSet,
                                 verbose_name=u"Комплект",
                                 blank=True, null=True)
    engine = models.ForeignKey(EngineModification,
                               verbose_name=u"Двигатель",
                               blank=True, null=True)

    def __unicode__(self):
        return u"%s, %s" % (self.part, self.engine)

    class Meta:
        verbose_name = u"применение запчасти в двигателе"
        verbose_name_plural = u"Применение запчасти в двигателе"


class Cross(models.Model):
    part = models.ForeignKey(Part,
                             verbose_name=u"Запчасть",
                             related_name="crosses")
    cross = models.ForeignKey(Part,
                              verbose_name=u"Заменитель",
                              related_name="+")
    is_checked = models.BooleanField(u"Проверен", default=False)
    created = models.DateTimeField(
        auto_now_add=True, verbose_name=u'Время создания',
        null=True, blank=True)

    SHARED_DATABASE = 'api'

    def __unicode__(self):
        return u"%s -> %s" % (self.part, self.cross)

    class Meta:
        verbose_name = u"кросс (замена)"
        verbose_name_plural = u"Кроссы (замены)"

    def save(self, *args, **kwargs):
        if not kwargs.pop('skip_last_upload_update', False):
            self.created = datetime.datetime.now()

        super(Cross, self).save(*args, **kwargs)


class LaximoCross(models.Model):
    original_part = ApiForeignKey(
        Part, verbose_name=u"Запчасть автомоби", null=True, blank=True)

    part_detailid = models.PositiveIntegerField()
    part_oem = models.CharField(
        u"Оригинальный номер", max_length=255, db_index=True, null=True)
    part_formattedoem = models.CharField(
        u"Оригинальный поисковый номер", max_length=255,
        db_index=True, null=True)
    part_manufacturer = models.CharField(
        u"Бренд оригинального номера", max_length=255, null=True)
    part_manufacturerid = models.PositiveIntegerField()
    part_name = models.CharField(u"Наименование", max_length=255, null=True)
    part_weight = models.DecimalField(
        max_digits=13, null=True, decimal_places=3, blank=True,
        max_length=255, verbose_name=u"Вес")

    original_cross = ApiForeignKey(
        Part, verbose_name=u"Кросс автомоби", null=True, blank=True,
        related_name='original_cross')

    cross_detailid = models.PositiveIntegerField()
    cross_oem = models.CharField(
        u"Номер замены", max_length=255, db_index=True, null=True)
    cross_formattedoem = models.CharField(
        u"Поисковый номер замены", max_length=255,
        db_index=True, null=True)
    cross_manufacturer = models.CharField(
        u"Бренд номера замены", max_length=255, null=True)
    cross_manufacturerid = models.PositiveIntegerField()
    cross_name = models.CharField(u"Наименование замены", max_length=255, null=True)
    cross_weight = models.DecimalField(
        max_digits=13, null=True, decimal_places=3, blank=True,
        max_length=255, verbose_name=u"Вес замены")
    cross_rate = models.CharField(
        u"номера замены - rate", max_length=255, null=True)
    cross_type = models.CharField(
        u"номера замены - type", max_length=255, null=True)
    cross_way = models.CharField(
        u"номера замены - way", max_length=255, null=True)

    is_checked = models.BooleanField(u"Проверен", default=False)
    is_copied = models.BooleanField(u"Проверен", default=False)
    created = models.DateTimeField(
        auto_now_add=True, verbose_name=u'Время создания',
        null=True, blank=True)

    def __unicode__(self):
        return u"%s -> %s" % (self.part_oem, self.cross_oem)

    class Meta:
        verbose_name = u"лаксима кросс (замена)"
        verbose_name_plural = u"лаксима кроссы (замены)"


class WeightSizeSpecification(SerializationMixin, models.Model):
    catalog_number = models.CharField(max_length=255,
                                      blank=True, verbose_name=u'Каталожный номер',
                                      db_index=True)
    tm = models.ForeignKey(TradeMark, verbose_name=u'Торговая марка')
    unit = models.ForeignKey(Unit, blank=True,
                             verbose_name=u'Единица измерения Автомоби')
    weight = models.DecimalField(max_digits=10,
                                 decimal_places=2,
                                 verbose_name=u'Вес, кг.',
                                 null=True, blank=True)
    m3 = models.DecimalField(max_digits=10,
                             decimal_places=2,
                             verbose_name=u'Габариты, м3',
                             null=True, blank=True)
    length = models.DecimalField(max_digits=10,
                                 decimal_places=2,
                                 verbose_name=u'Длина, см',
                                 null=True, blank=True)
    width = models.DecimalField(max_digits=10,
                                decimal_places=2,
                                verbose_name=u'Ширина, см',
                                null=True, blank=True)
    height = models.DecimalField(max_digits=10,
                                 decimal_places=2,
                                 verbose_name=u'Высота, см',
                                 null=True, blank=True)

    SERIALIZATION_FIELDS = ('unit', 'weight', 'm3', 'length', 'width', 'height')

    class Meta:
        index_together = [['catalog_number', 'tm']]

    @classmethod
    def get_by_attr(cls, attr_name, *args, **kwargs):
        kwargs.update({'%s__isnull' % attr_name: False})
        obj = cls.objects.filter(*args, **kwargs).order_by('-catalog_number').first()
        if obj is None:
            return Decimal(
                Settings.get_value_by_name(
                    'default_weight_size_specification'))

        return getattr(obj, attr_name)

    @staticmethod
    def pre_save(instance, **kwargs):
        attrs = ['weight', 'm3', 'length', 'width', 'height']
        truth = [True for attr in attrs if getattr(instance, attr)]

        if len(truth) != 1:
            raise InvalidWeightSizeSpecification(
                u'Выберите только один атрибут')

        if not instance.unit:
            instance.unit = Settings.get_value_by_name('default_unit')

signals.pre_save.connect(sender=WeightSizeSpecification,
                         receiver=WeightSizeSpecification.pre_save)


class Barcode(SerializationMixin, models.Model):
    part = models.ForeignKey('Part', verbose_name=u'Деталь')
    value = models.CharField(u'Штрих код', max_length=255)
    wws = models.ForeignKey('WeightSizeSpecification',
                            verbose_name=u'Единица измерения')

    objects = SerializationQueryset().as_manager()

    SERIALIZATION_FIELDS = ('value', 'wws', 'part_id')


class AC(MPTTModel):
    title = models.CharField(u'Название', max_length=255)
    slug = models.SlugField(u"URL", unique=True,
                            blank=True, null=True,
                            max_length=280)
    parts = models.ManyToManyField('Part', verbose_name=u'Запчасти',
                                   blank=True)

    parent = TreeForeignKey('self', null=True,
                            blank=True, related_name='children',
                            verbose_name=u"Родитель")

    def __unicode__(self):
        return u'(%s) %s' % (self.level, self.title)

    @staticmethod
    def post_save(sender, instance, created, **kwargs):
        if not instance.slug:
            instance.slug = '%s-%s' % (
                pytils.translit.slugify(instance.title),
                instance.id)
            instance.save()

signals.post_save.connect(sender=AC,
                          receiver=AC.post_save)


class ACProperty(models.Model):
    title = models.CharField(u'Название', max_length=255, unique=True)
    show_by_count = models.IntegerField(u'Количество выводимых значений',
                                        null=True, default=8
                                        )
    show_by_parts = models.IntegerField(u'Количество выводимых значений по партам',
                                        null=True, default=20
                                        )
    order = models.IntegerField(u'Порядок сортировки',
                                null=True, default=10
                                )
    acs = models.ManyToManyField('AC', verbose_name=u'Меню каталога')

    def __unicode__(self):
        return u'%s' % self.title


class ACPropertyValue(models.Model):
    title = models.CharField(u'Название', max_length=255)
    ac_property = models.ForeignKey('ACProperty',
                                    verbose_name=u'Свойство каталога')
    parts = models.ManyToManyField('Part', verbose_name=u'Запчасти',
                                   blank=True)

    class Meta:
        unique_together = (('title', 'ac_property'),)

    def __unicode__(self):
        return u'%s' % self.title


class PartDescription(SerializationMixin, models.Model):
    # description = models.TextField(u'Описание')
    # part = models.ForeignKey('Part')
    # language = models.ForeignKey('core.Language')
    # tec_doc_id = models.IntegerField()

    SERIALIZATION_FIELDS = ('id', 'texts')

    # def __unicode__(self):
    #     return u'%s' % self.description


class PartDescriptionText(SerializationMixin, models.Model):
    part_description = models.ForeignKey('PartDescription', related_name='texts')
    value = models.TextField()
    language = models.ForeignKey('core.Language')

    SERIALIZATION_FIELDS = ('language_id', 'value')

    objects = SerializationQueryset().as_manager()

    class Meta:
        unique_together = ('part_description', 'language')


class GeneratedPart(models.Model):
    part = models.ForeignKey('Part')
    tm = models.ForeignKey('TradeMark')
    search_number = models.CharField(max_length=255)
    catalog_number = models.CharField(max_length=255)
    description = models.TextField(null=True)
    timestamp = models.DateTimeField(auto_now_add=True)


class GeneratedTradeMark(models.Model):
    tm = models.ForeignKey('TradeMark')
    show_name = models.CharField(max_length=255)
    timestamp = models.DateTimeField(auto_now_add=True)


class SearchTreeGenericArticle(models.Model):
    tree_id = models.IntegerField(null=True,
                                  db_index=True)
    generic_article_id = models.IntegerField(null=True,
                                             db_index=True)


class LinkGenericArticlePart(models.Model):
    part_id = models.IntegerField(null=True,
                                  db_index=True)
    tecdoc_id = models.IntegerField(null=True,
                                  db_index=True)
    generic_article_id = models.IntegerField(null=True,
                                             db_index=True)


class LinkCarModificationGenericArticle(models.Model):
    car_modification_id = models.IntegerField(null=True,
                                              db_index=True)
    link_generic_article_part_id = models.IntegerField(null=True,
                                                       db_index=True)
    generic_article_id = models.IntegerField(null=True,
                                             db_index=True)


class VinHistory(models.Model):
    user = models.ForeignKey('users.User',
                             null=True,
                             blank=True)
    vin = models.CharField(max_length=512)
    timestamp = models.DateTimeField(auto_now_add=True)
    user_ip = models.GenericIPAddressField(null=True, blank=True)


class Criteria(models.Model):
    description = models.CharField(max_length=255, null=True, blank=True,
                                   verbose_name=u'Описание')
    short_description = models.CharField(max_length=255, null=True, blank=True,
                                         verbose_name=u'Короткое описание')
    unit_text = models.CharField(max_length=255, null=True, blank=True,
                                 verbose_name=u'Единица измерения')
    type = models.CharField(max_length=6, null=True, blank=True,
                            verbose_name=u'Тип')
    is_interval = models.BooleanField(u"Интервал", default=False)
    successor = models.ForeignKey('Criteria', null=True, blank=True,
                                  verbose_name=u'Второй критерий')

    def __unicode__(self):
        return u"%s" % (self.short_description)

    class Meta:
        verbose_name = u"критерий"
        verbose_name_plural = u"Критерии"


class PartCriteria(models.Model):
    catalog_part = models.ForeignKey(Part, verbose_name=u'Запчасть', null=True)
    criteria = models.ForeignKey('Criteria', null=True, blank=True,
                                 verbose_name=u'Критерий')
    value = models.CharField(max_length=255, null=True, blank=True,
                             verbose_name=u'Значение')
    is_shown = models.BooleanField(u"Показывать в списке", default=False)

    def __unicode__(self):
        return u"%s: %s (%s)" % (
            self.criteria,
            (self.value if self.value else self.text_value),
            self.catalog_part)

    class Meta:
        verbose_name = u"значение критерия"
        verbose_name_plural = u"Значения критериев"


class PartCriteriaLA(models.Model):
    link_generic_article_part_id = models.IntegerField(null=True,
                                                       db_index=True)
    criteria = models.ForeignKey('Criteria', null=True, blank=True,
                                 verbose_name=u'Критерий')
    value = models.CharField(max_length=255, null=True, blank=True,
                             verbose_name=u'Значение')
    is_shown = models.BooleanField(u"Показывать в списке", default=False)

    def __unicode__(self):
        return u"%s: %s (%s)" % (
            self.criteria,
            (self.value if self.value else self.text_value),
            self.link_generic_article_part_id)

    class Meta:
        verbose_name = u"значение критерия для групп"
        verbose_name_plural = u"Значения критериев для групп"


class SalesCountry(models.Model):
    code = models.CharField(max_length=255, null=True, blank=True,
                            verbose_name=u'Код страны (3-х буквенный)')
    name = models.CharField(max_length=255, null=True, blank=True,
                            verbose_name=u'Название страны')
    currency_code = models.CharField(max_length=255, null=True, blank=True,
                                     verbose_name=u'Код валюты')
    iso2 = models.CharField(max_length=255, null=True, blank=True,
                            verbose_name=u'Код страны (2-х буквенный)')

    is_group = models.BooleanField(u"Группа стран", default=False)

    def __unicode__(self):
        return u"%s" % (self.name)

    class Meta:
        verbose_name = u"страна"
        verbose_name_plural = u"Страны"


class ModificationSalesCountry(models.Model):
    modification_id = models.IntegerField(null=True,
                                          db_index=True)
    sales_country = models.ForeignKey('SalesCountry', null=True, blank=True,
                                      verbose_name=u'страна')


def get_upload_path_catalog(instance, filename):
    path = os.path.join('catalog_files/', get_translit_filename(filename))
    return path


class CatalogUploadObject(models.Model):
    STATUS_CHOICES = (
        ('wait', u"Ожидает обработки"),
        ('proccessing', u"Обрабатывается"),
        ('error', u"Ошибка"),
        ('done', u"Обработан"))

    status = models.CharField(u"Статус загрузки", max_length=255,
                              choices=STATUS_CHOICES,
                              default='wait')
    upload_file = models.FileField(upload_to=get_upload_path_catalog,
                                   verbose_name=u"Файл для загрузки")
    error_msg = models.TextField(blank=True, null=True,
                                 verbose_name=u'Сообщение об ошибке')
    timestamp = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return u"Загрузка: №%s (%s)" % (self.id, self.timestamp)

    class Meta:
        verbose_name = u"Загрузка новых данных в каталог"
        verbose_name_plural = u"Загрузки новых данных в каталог"

    def readlines_from_csv(self):
        filepath = self.upload_file.path
        with open(filepath, 'rb') as csvfile:
            reader = csv.reader(csvfile, delimiter='\t',
                                encoding='utf-8', errors='ignore')
            for x, row in enumerate(reader):
                # Skipping empty rows
                if len(row) == 1:
                    continue

                fields = {
                    'row': x,
                    'data_row': row,
                }
                yield fields
