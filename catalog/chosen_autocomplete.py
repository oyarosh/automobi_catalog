# coding: utf-8
import json
import logging
import chosen_complete
from datetime import datetime
from django.db.models import Q

from chosen_complete import Autocomplete
from superuser.models import Settings
from core.utils import get_default_currency
from search.utils import get_roles_ids, get_currency_rates, get_sales_price
from auto_api.methods import api_search

from catalog.models import (Part, PartDescription, CarModel, CarBrand,
                            CarModification, TradeMark, PartDescriptionText,
                            CarTree, CarModelCountry, CarText, SalesCountry)
from catalog.utils import part_full_text_qs

logger = logging.getLogger('django.request')


class ACSearchAutocomplete(Autocomplete):
    model = Part
    autocomplete_name = 'ac_search'
    search_fields = ['description__icontains']
    query_limit = '10'
    ac_category = None  # Use only with AC-Full-Text-Search
    sales_price_list = None  # Use only with AC-Full-Text-Search
    dc_id, so_id, client_id, supplier_id, shortcut = \
        None, None, None, None, None  # Use only with AC-Full-Text-Search

    def __init__(self, *args, **kwargs):
        super(ACSearchAutocomplete, self).__init__()

    def post(self, request, *args, **kwargs):
        # look at comment above
        self.ac_category = request.POST.get('categories', None)
        self.dc_id, self.so_id, self.client_id, self.supplier_id, self.shortcut = \
            get_roles_ids(request.role)
        if isinstance(self.dc_id, int):
            self.dc_id = (self.dc_id, )

        self.sales_price_list = get_sales_price(self.dc_id, self.so_id,
                                                self.client_id,
                                                self.supplier_id,
                                                self.shortcut)

        return super(ACSearchAutocomplete, self).post(request, *args, **kwargs)

    def filter(self, queryset, q=None, excluded=None,
               always=None, return_query=False):
        if not q:
            q = u''

        if return_query:
            return q

        spl_ids = [item['id'] for item in self.sales_price_list]
        return part_full_text_qs(query=q, ac_part_filter_enable=True,
                                 empty_query_result=True,
                                 search_field='description',
                                 is_catalog_process=True,
                                 ac_category=self.ac_category,
                                 sales_price_list_ids=spl_ids)

    def get_total_count(self, queryset=None):
        if self.filter(queryset, return_query=True):
            q = self.filter(queryset, return_query=True)
            spl_ids = [item['id'] for item in self.sales_price_list]
            return part_full_text_qs(query=q, ac_part_filter_enable=True,
                                     empty_query_result=True,
                                     search_field='description',
                                     is_catalog_process=True,
                                     ac_category=self.ac_category,
                                     sales_price_list_ids=spl_ids).count()
        return 100

    def get_items(self, queryset):
        return_list = []

        part_ids = list(queryset.values_list('id', flat=True))

        basic_currency_id = get_default_currency().id

        currency_rates = get_currency_rates(self.dc_id, self.so_id,
                                            self.client_id, self.supplier_id,
                                            basic_currency_id)

        user_currency_id = getattr(self.request.currency, 'id', basic_currency_id)
        sorting = ('price', 'asc')

        api_response = api_search(
            'hui',
            basic_currency_id,
            user_currency_id,
            json.dumps(currency_rates),
            json.dumps(self.sales_price_list),
            False,
            json.dumps(sorting),
            self.shortcut,
            '{}',
            self.so_id,
            self.client_id,
            False,
            part_ids
        )

        items = api_response['parts']
        part_map = {}
        for item in items:
            part_map[item[0][19]] = item[0]

        get_image = lambda x: getattr(getattr(x, 'image', ''), 'name', '')

        for item in queryset:
            try:
                tm_name = unicode(item.tm)
            except TradeMark.DoesNotExist:
                tm_name = ''

            price = str(part_map[item.id][16]) + ' грн' \
                if part_map[item.id][16] else u'<span style="color: #7F7F7F;">Нет в наличии</span>'

            return_list.append({
                'id': item.slug,
                'catalog_number': item.catalog_number,
                'tm': tm_name,
                'price': price,
                'image': get_image(item.partimage_set.first()),
                'description': item.description[:150]})
        return return_list

chosen_complete.register(view=ACSearchAutocomplete)


class PartAutocomplete(Autocomplete):
    model = Part
    search_fields = ['tm__show_name__icontains', ]
    order_by = ['search_number']


chosen_complete.register(view=PartAutocomplete)


class PartDescriptionAutocomplete(Autocomplete):
    model = PartDescription
    search_fields = ['description__icontains']
    order_by = ['description']


chosen_complete.register(view=PartDescriptionAutocomplete)


class PartDescriptionTextAutocomplete(Autocomplete):
    model = PartDescriptionText
    search_fields = ['value']
    order_by = ['value']

    def get_queryset(self):
        return PartDescriptionText.objects.filter(language_id=16)


chosen_complete.register(view=PartDescriptionTextAutocomplete)


class CarTreeAutocomplete(Autocomplete):
    model = CarTree
    search_fields = ['title__icontains']
    order_by = ['title']

    def get_queryset(self):
        return CarTree.objects.filter(children__isnull=True)\
            .order_by('title')\
            .distinct('title')

chosen_complete.register(view=CarTreeAutocomplete)


class CarModelAutocomplete(Autocomplete):
    model = CarModel
    search_fields = ['model_country__title__istartswith']
    order_by = ['model_country__title']
    max_items = None

    def filter(self, queryset, q=None, excluded=None, always=None):
        queryset = super(CarModelAutocomplete, self) \
            .filter(queryset, q, excluded, always)
        car_brand = self.request.POST.get('car_brand')
        year = self.request.POST.get('date_production')
        if car_brand:
            queryset = queryset.filter(car_brand=car_brand)
        if year and len(year) > 3:
            queryset = queryset.filter(Q(startdate_production__year=int(year))
                                       | Q(enddate_production__year=int(year))
                                       | (Q(startdate_production__lte=datetime.strptime(year, '%Y'))
                                          & Q(Q(enddate_production__gte=datetime.strptime(year, '%Y'))
                                            | Q(enddate_production=None))))
        country = Settings.objects.filter(code_name='default_tec_doc_country').first()
        if country:
            queryset = queryset.filter(model_country__country_id=country.object_id)

        queryset = queryset.extra(
            select={'title': 'catalog_carmodelcountry.title',
                    'car_model_tex_id': 'catalog_carmodelcountry.id'}
        )
        queryset = queryset.distinct('model_country__title')
        return queryset

    def get_items(self, queryset):
        return [{'id': item.car_model_tex_id, 'text': "%s (%s - %s)" % (
            item.title,
            item.startdate_production.strftime('%m.%Y') if item.startdate_production else '...',
            item.enddate_production.strftime('%m.%Y') if item.enddate_production else '...'
        )} for item in queryset]


chosen_complete.register(view=CarModelAutocomplete)


class CarBrandAutocomplete(Autocomplete):
    model = CarBrand
    search_fields = ['title__istartswith']
    order_by = ['title']
    max_items = None

    def get_items(self, queryset):
        return [{'id': item.id, 'text': item.title} for item in queryset]


chosen_complete.register(view=CarBrandAutocomplete)


class CarModificationAutocomplete(Autocomplete):
    model = CarModification
    search_fields = ['show_name__icontains']
    order_by = ['show_name']
    max_items = None

    def filter(self, queryset, q=None, excluded=None, always=None):
        queryset = super(CarModificationAutocomplete, self) \
            .filter(queryset, q, excluded, always)

        car_model = None
        if self.request.POST.get('car_model', None):
            car_model_text_id = self.request.POST.get('car_model')
            car_model_country = CarModelCountry.objects.get(id=car_model_text_id)
            car_model_general_title_id = car_model_country.general_title_id
            car_model = car_model_country.car_model_id
        year = self.request.POST.get('date_production')
        if car_model:
            queryset = queryset.filter(car_model=car_model)
            try:
                self.request.session['catalog_car_model'] = \
                    CarText.objects.get(id=car_model_general_title_id).cartext
            except CarText.DoesNotExist:
                logger.warning('CarText DoesNotExist: CarModelCountry id %s' \
                               % str(car_model_country.id))

        if year and len(year) > 3:
            queryset = queryset.filter(Q(startdate_production__year=int(year))
                                       | Q(enddate_production__year=int(year))
                                       | (Q(startdate_production__lte=datetime.strptime(year, '%Y'))
                                          & Q(Q(enddate_production__gte=datetime.strptime(year, '%Y'))
                                            | Q(enddate_production=None))))
        country = Settings.objects.filter(code_name='default_tec_doc_country').first()
        if country:
            queryset = queryset.filter(
                modification_country__country_id=country.object_id)

        queryset = queryset.extra(
            select={'title': 'catalog_carmodificationcountry.title'}
        )
        queryset = queryset.distinct()\
            .order_by('modification_country__title')
        return queryset


chosen_complete.register(view=CarModificationAutocomplete)


class TradeMarkAutocomplete(Autocomplete):
    model = TradeMark
    search_fields = ['show_name__istartswith', ]
    order_by = ['show_name']


chosen_complete.register(view=TradeMarkAutocomplete)


class SalesCountryAutocomplete(Autocomplete):
    model = SalesCountry
    search_fields = ['name__istartswith', ]
    order_by = ['-id']


chosen_complete.register(view=SalesCountryAutocomplete)