# coding: utf-8
import json
import urllib
import cStringIO
import suds
import math
import datetime
import random
from operator import *
from collections import OrderedDict
from PIL import Image

from django.conf import settings
from django.http import HttpResponse, JsonResponse, Http404, \
    HttpResponsePermanentRedirect
from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import TemplateView
from django.db.models import Q
from django.template.loader import render_to_string
from django.core.urlresolvers import reverse

from chosen_complete import registry
from db_share.utils import get_connection

from superuser.models import Settings
from users.models import DC, ClientCar
from seo.utils import generate_text_from_template, save_seo_data
from core.utils import get_default_currency
from search.utils import get_roles_ids, get_currency_rates, get_sales_price
from auto_api.methods import api_search, api_catalog
from laximo import OEM

from cabinet.mixins import View, AjaxFormMixin, FormView
from cabinet.utils import send_templated_email

from price_generator.tasks import str2bool
from price_generator.models import SalesPriceList

from catalog.queries import prepare_query
from catalog.chosen_autocomplete import SalesCountryAutocomplete
from catalog.models import CarBrand, CarModel, CarModification, CarTree, \
    Part, GoodsCategory, AC, ACPropertyValue, ACProperty, \
    LinkCarModificationGenericArticle, VinHistory, SalesCountry, \
    PartImage, CarModelCountry, TradeMark, CarText
from catalog.forms import SearchCarForm, FastCatalogForm, CallBackForm, \
    ACSearchForm, ACSearchCategoryForm
from catalog.utils import GetPrice, CarSearchPopupMixin, is_pk, \
    get_multiply_four


def brands(request, select_letter=None):
    alphabet = []
    grouped_brands = {}
    query = Q(is_passenger=True) | Q(is_commercial=True)
    query &= Q(is_shown=True)
    brand_qs = CarBrand.objects.filter(query).order_by('title').all()

    brands_passenger_list = list(brand_qs)

    for brand in brands_passenger_list:
        first_letter = brand.title[:1]
        if first_letter not in alphabet:
            alphabet.append(first_letter)
        if first_letter not in grouped_brands:
            grouped_brands[first_letter] = []
        grouped_brands[first_letter].append(brand)

    if select_letter:
        grouped_brands_tmp = grouped_brands.copy()
        grouped_brands = {}
        grouped_brands[select_letter] = grouped_brands_tmp[select_letter]

    data = {
        "alphabet": alphabet,
        "grouped_brands": grouped_brands,
        "select_letter": select_letter,
        "fast_form": FastCatalogForm()
    }

    return render(request, "catalog/brands.html", data)


class ACView(TemplateView):
    template_name = 'catalog/ac_short_list.html'

    def get_context_data(self, **kwargs):
        kwargs['acs'] = AC.objects.filter(level=0)
        kwargs['brands'] = CarBrand.objects.all()
        kwargs['fast_form'] = FastCatalogForm()
        return kwargs


class VinSearch(TemplateView):
    template_name = 'catalog/ac_short_list-orig.html'


def general_models(request, car_brand_slug):
    brand = get_object_or_404(CarBrand, slug=car_brand_slug)
    query = Q(is_passenger=True) | Q(is_commercial=True)
    query &= Q(car_brand=brand)
    queryset = CarModel.objects.filter(query)

    country = Settings.objects.filter(code_name='default_tec_doc_country').first()
    if country:
        queryset = queryset.filter(model_country__country_id=country.object_id)

    queryset = queryset.extra(
        select={'title': 'catalog_carmodelcountry.title',
                'car_model_tex_id': 'catalog_carmodelcountry.id'}
    )

    ids_qs = queryset.order_by('model_country__general_title')\
        .distinct('model_country__general_title')\
        .values_list('model_country__general_title', flat=True)

    models_list = CarText.objects.filter(id__in=ids_qs).order_by('cartext')

    per_column = models_list.count() / 3 + 1

    spm_brands_qs = CarBrand.objects.filter(id=brand.id).order_by('title')
    spm_per_column = spm_brands_qs.first()\
        .get_general_model_title_qs().count() / 4 + 1

    data = {
        "models_list": models_list,
        "brand": brand,
        "car_brand_slug": car_brand_slug,
        "per_column": per_column,
        "spm_brands_qs": spm_brands_qs,
        "spm_per_column": spm_per_column
    }

    if not request.seo_data or not request.seo_data.seo_text:
        h1 = 'Запчасти %s: %s' % (brand.title_rus, brand.title)
        additional = '<br />'
        for i, item in enumerate(models_list):
            if i >= 4:
                break
            additional += ' - Запчасти для %s модель: %s <br />' % (brand.title, item.cartext)
        text = generate_text_from_template(
            template_id=11,
            prebreadcrumbs='Запчасти для %s' % brand.title,
            h1=h1, additional=additional)
        seo = save_seo_data(request=request, text=text, title=h1)
        data['seo_data'] = seo

    return render(request, "catalog/general_models.html", data)


def models(request, car_brand_slug, general_model_id=None):
    brand = get_object_or_404(CarBrand, slug=car_brand_slug)
    request.session['catalog_car_model'] = ''
    query = Q(is_passenger=True) | Q(is_commercial=True)
    query &= Q(car_brand=brand)
    models_list = CarModel.objects.filter(query)

    country = Settings.objects.filter(code_name='default_tec_doc_country').first()
    if country:
        models_list = models_list.filter(model_country__country_id=country.object_id)
    general_model = None
    if general_model_id:
        models_list = models_list.filter(model_country__general_title_id=general_model_id)
        general_model = CarText.objects.get(id=general_model_id)
    models_list = models_list.extra(
            select={'title': 'catalog_carmodelcountry.title',
                    'car_model_tex_id': 'catalog_carmodelcountry.id'}
    )
    models_list = models_list.distinct('model_country__title')\
        .order_by('model_country__title').all()
    data = {
        "models_list": models_list,
        "brand": brand,
        "car_brand_slug": car_brand_slug,
        "general_model": general_model,
    }

    if not request.seo_data or not request.seo_data.seo_text:
        h1 = 'Запчасти %s: %s' % (brand.title_rus, brand.title)
        additional = '<br />'
        for i, item in enumerate(models_list):
            if i >= 4:
                break
            additional += ' - Запчасти для %s модель: %s <br />' % (brand.title, item.title)
        text = generate_text_from_template(
            template_id=11,
            prebreadcrumbs='Запчасти для %s' % brand.title,
            h1=h1, additional=additional)
        seo = save_seo_data(request=request, text=text, title=h1)
        data['seo_data'] = seo

    return render(request, "catalog/models.html", data)


def models_selective(request):
    # need remove with related views
    form = SearchCarForm(request.GET)
    brand_id = request.GET.get('val_param', None)

    if not brand_id:
        brand_id = 0

    brand = CarBrand.objects.filter(id=brand_id).first()

    form.is_valid()
    cl_data = form.cleaned_data

    models_list = CarModel.objects.all()

    if brand:
        models_list = models_list.filter(car_brand=brand)

    if cl_data.get('body', None):
        models_list = models_list.filter(carmodification__body=cl_data['body'])

    if cl_data.get('fuel', None):
        models_list = models_list.filter(carmodification__fuel=cl_data['fuel'])

    data = {
        "models_list": models_list,
        "brand": brand,
    }

    return render(request, "catalog/models.html", data)


def modifications(request, car_brand_slug, car_model_slug):
    # 301 redirect for get parametrs
    if request.GET.get('country_id', None):
        return HttpResponsePermanentRedirect(request.path)

    car_model = get_object_or_404(CarModel, slug=car_model_slug)
    modifications_list = CarModification.objects.filter(car_model=car_model)
    carmodel_country_id = request.session.get('model_country', None)
    carmodel = carmodel_title = ''

    if carmodel_country_id and CarModelCountry.objects.filter(id=carmodel_country_id).exists():
        carmodel = CarModelCountry.objects.get(id=carmodel_country_id)
    if carmodel:
        carmodel_title = carmodel.title

    sales_country_id = request.POST.get('country_id', None)
    if not sales_country_id:
        country = Settings.objects.filter(code_name='default_tec_doc_country').first()
        sales_country_id = country.object_id
    if sales_country_id:
        modifications_list = modifications_list.filter(
            modification_country__country_id=sales_country_id)

    modifications_list = modifications_list.extra(
            select={'title': 'catalog_carmodificationcountry.title',
                    'car_mod_tex_id': 'catalog_carmodificationcountry.id'}
    )
    modifications_list = modifications_list.distinct('modification_country__title')\
        .order_by('modification_country__title').prefetch_related(
            'modification_country',
            'modification_country__country'
    ).all()

    countries_url = registry.get_url_for_autocomplete(SalesCountryAutocomplete)
    sales_country = SalesCountry.objects.get(id=sales_country_id)
    country_json = '{"id": "%s", "text": "%s"}' % \
        (str(sales_country.id), sales_country.name)

    req_data = {'country_id': sales_country_id}
    data = {
        "req_data": req_data,
        "modifications_list": modifications_list,
        "car_model": car_model,
        "car_brand_slug": car_brand_slug,
        "car_model_slug": car_model_slug,
        "countries_url": countries_url,
        "carmodel_title": carmodel_title,
        "carmodel_country_id": carmodel_country_id,
        "country_json": country_json,
    }

    if request.session.get('catalog_car_model', None):
        data['car_model_save'] = request.session['catalog_car_model']
    elif data['carmodel_title']:
        data['car_model_save'] = data['carmodel_title']
    else:
        data['car_model_save'] = data['car_model'].car_type_shot_key
    if request.seo_data and request.seo_data.page_h1:
        data['h1_save'] = request.seo_data.page_h1
    else:
        data['h1_save'] = 'Запчасти %s: %s %s' % (
                            data['car_model'].car_brand.title_rus,
                            data['car_model'].car_brand.title,
                            data['car_model_save'])

    if not request.seo_data or not request.seo_data.seo_text:
        h1 = data['h1_save']
        additional = '<br />'
        for i, item in enumerate(modifications_list):
            if i >= 4:
                break
            additional += ' - Запчасти для %s модель: %s модификация: %s <br />' % \
                          (car_model.car_brand.title, car_model.car_type_shot_key, item.title)
        text = generate_text_from_template(
            template_id=12,
            prebreadcrumbs='Запчасти для %s' % car_model.car_brand.title,
            h1=h1, additional=additional)
        seo = save_seo_data(request=request, text=text, title=h1)
        data['seo_data'] = seo

    return render(request, "catalog/modifications.html", data)


def get_tree_mobile_data(car_brand_slug, car_model_slug, car_modification_slug):
    modification = get_object_or_404(CarModification,
                                     slug=car_modification_slug)

    tree_exists = LinkCarModificationGenericArticle.objects.filter(
        car_modification_id=modification.id).exists()

    query = """
        WITH RECURSIVE
            items AS (
                SELECT
                    query.id
                FROM (
                    SELECT
                        catalog_cartree.id,
                        row_number() OVER (PARTITION BY catalog_cartree.parent_id ORDER BY catalog_cartree.title) as n,
                        catalog_cartree.show_on_main
                    FROM catalog_linkcarmodificationgenericarticle
                    JOIN catalog_searchtreegenericarticle ON catalog_searchtreegenericarticle.generic_article_id = catalog_linkcarmodificationgenericarticle.generic_article_id
                    JOIN catalog_cartree ON catalog_cartree.id = catalog_searchtreegenericarticle.tree_id
                    WHERE catalog_linkcarmodificationgenericarticle.car_modification_id = %(modification_id)s
                    AND catalog_cartree.type = 1
                    AND catalog_cartree.parent_id IS NOT NULL
                    GROUP BY catalog_cartree.id
                    ORDER by catalog_cartree.title
                ) AS query

            ),
            res (id, parent_id) AS (
                SELECT CCT.id, CCT.parent_id
                FROM catalog_cartree AS CCT
                WHERE
                    CCT.parent_id IN (
                        SELECT * FROM items
                    )
                                    OR CCT.id IN (
                        SELECT * FROM items
                    )
            )
        SELECT res.id
        FROM res
        WHERE res.id IN (
            SELECT catalog_cartree.id
            FROM catalog_linkcarmodificationgenericarticle
            JOIN catalog_searchtreegenericarticle ON catalog_searchtreegenericarticle.generic_article_id = catalog_linkcarmodificationgenericarticle.generic_article_id
            JOIN catalog_cartree ON catalog_cartree.id = catalog_searchtreegenericarticle.tree_id
            WHERE catalog_linkcarmodificationgenericarticle.car_modification_id = %(modification_id)s
        )
        ORDER BY res.id
    """

    params = {'modification_id': modification.id}
    tree_items = []
    with get_connection().cursor() as c:
        c.execute(query, params)
        tree_items = [item[0] for item in c.fetchall()]

    data = {
        "modification": modification,
        "tree_exists": tree_exists,
        "car_brand_slug": car_brand_slug,
        "car_model_slug": car_model_slug,
        "car_modification_slug": car_modification_slug,
        "nodes": CarTree.objects.filter(id__in=tree_items, level__in=[1,2,3,4]).all()
    }

    return data


def tree(request, car_brand_slug, car_model_slug, car_modification_slug,
         tree_first_id=None, tree_second_id=None, tree_third_id=None):
    carmodel_country_id = request.session.get('model_country', None)
    carmodelcountry = None
    if carmodel_country_id and is_pk(carmodel_country_id):
        if CarModelCountry.objects.filter(id=carmodel_country_id).exists():
            carmodelcountry = CarModelCountry.objects.get(id=carmodel_country_id)

    if request.is_mobile:
        # this fast stupid solution but anyway
        data = get_tree_mobile_data(
            car_brand_slug, car_model_slug, car_modification_slug)
        return render(request, "catalog/tree.html", data)

    modification = get_object_or_404(CarModification,
                                     slug=car_modification_slug)

    tree_exists = LinkCarModificationGenericArticle.objects.filter(
        car_modification_id=modification.id).exists()

    query = """
        WITH
            items AS (
                    SELECT
                        catalog_cartree.id
                    FROM catalog_linkcarmodificationgenericarticle
                    JOIN catalog_searchtreegenericarticle ON catalog_searchtreegenericarticle.generic_article_id = catalog_linkcarmodificationgenericarticle.generic_article_id
                    JOIN catalog_cartree ON catalog_cartree.id = catalog_searchtreegenericarticle.tree_id
                    WHERE catalog_linkcarmodificationgenericarticle.car_modification_id = %(modification_id)s
                    AND catalog_cartree.type = 1
                    AND
                    CASE
                        WHEN %(tree_third_id)s > 0 THEN
                            ((catalog_cartree.parent_id = %(tree_third_id)s AND catalog_cartree.level=%(level_root)s)
                            OR (catalog_cartree.parent_id IN (SELECT id FROM catalog_cartree WHERE parent_id=%(tree_third_id)s) AND catalog_cartree.level=%(level_node)s))
                        WHEN %(tree_second_id)s > 0 THEN
                            ((catalog_cartree.parent_id = %(tree_second_id)s AND catalog_cartree.level=%(level_root)s)
                            OR (catalog_cartree.parent_id IN (SELECT id FROM catalog_cartree WHERE parent_id=%(tree_second_id)s) AND catalog_cartree.level=%(level_node)s))
                        WHEN %(tree_first_id)s > 0 THEN
                            ((catalog_cartree.parent_id = %(tree_first_id)s AND catalog_cartree.level=%(level_root)s)
                            OR (catalog_cartree.parent_id IN (SELECT id FROM catalog_cartree WHERE parent_id=%(tree_first_id)s) AND catalog_cartree.level=%(level_node)s))
                        ELSE
                            catalog_cartree.parent_id IS NOT NULL
                    END
                    GROUP BY catalog_cartree.id
                    ORDER by catalog_cartree.lft
            ),
            res (id, parent_id, level, show_on_main, title, lft) AS (
                SELECT CCT.id, CCT.parent_id, CCT.level, CCT.show_on_main, CCT.title, CCT.lft
                FROM catalog_cartree AS CCT
                WHERE
                    CCT.parent_id IN (
                        SELECT * FROM items
                    )
                                    OR CCT.id IN (
                        SELECT * FROM items
                    )
            )
        SELECT id, part_count, level, lft
        FROM (
            SELECT
                res.id,
                row_number() OVER (PARTITION BY res.parent_id ORDER BY res.lft) as n,
                res.level,
                res.parent_id,
                query.part_count,
                res.lft
            FROM res
            INNER JOIN (
                    SELECT catalog_cartree.id, COUNT(catalog_searchtreegenericarticle.generic_article_id) AS part_count
                    FROM catalog_linkcarmodificationgenericarticle
                    JOIN catalog_searchtreegenericarticle ON catalog_searchtreegenericarticle.generic_article_id = catalog_linkcarmodificationgenericarticle.generic_article_id
                    JOIN catalog_cartree ON catalog_cartree.id = catalog_searchtreegenericarticle.tree_id
                    WHERE catalog_linkcarmodificationgenericarticle.car_modification_id = %(modification_id)s
                    AND catalog_cartree.type = 1
                    AND
                    CASE
                        WHEN %(search_query)s IS NOT NULL THEN
                            catalog_cartree.title ILIKE %(search_query)s
                        ELSE
                            1=1
                        END
                    GROUP BY catalog_cartree.id
            ) AS query
                ON res.id = query.id

            ORDER BY res.lft
        ) AS query
        WHERE
            CASE
               WHEN %(tree_third_id)s > 0 THEN
                level IN (%(level_root)s, %(level_node)s)
               WHEN %(tree_second_id)s > 0 THEN
                level IN (%(level_root)s, %(level_node)s)
               WHEN %(tree_first_id)s > 0 THEN
                level IN (%(level_root)s, %(level_node)s)
               ELSE
                level = %(level_root)s OR (level=%(level_node)s AND n < 7)
            END
        ORDER BY lft
    """

    tree_first_node = None
    level_root = 1
    level_node = 2
    skip_tree = False
    if tree_third_id:
        tree_first_node = get_object_or_404(CarTree, id=tree_third_id)
        level_root = 4
        level_node = 4
        skip_tree = True
    elif tree_second_id:
        tree_first_node = get_object_or_404(CarTree, id=tree_second_id)
        level_root = 3
        level_node = 4
    elif tree_first_id:
        tree_first_node = get_object_or_404(CarTree, id=tree_first_id)
        level_root = 2
        level_node = 3

    search_query = None
    if request.GET.get('search', None):
        search_query = '%{}%'.format(request.GET['search'])
    params = {
        'modification_id': modification.id,
        'tree_first_id': tree_first_id,
        'tree_second_id': tree_second_id,
        'tree_third_id': tree_third_id,
        'level_root': level_root,
        'level_node': level_node,
        'search_query': search_query
    }
    tree_items = []
    tree_count = {}
    tree_sum = 0

    with get_connection().cursor() as c:
        c.execute(query, params)
        for item in c.fetchall():
            tree_items.append(item[0])
            tree_count[item[0]] = item[1]
            if item[2] == level_root or search_query:
                tree_sum += item[1]

    nodes = CarTree.objects.filter(id__in=tree_items,
                                   level__in=[level_root, level_node]).all()

    if search_query and len(nodes) == 1:
        return parts_redirect(request, car_brand_slug, car_model_slug,
                              car_modification_slug, nodes.first().slug,
                              permanent=False)
    data = {
        "modification": modification,
        "tree_exists": tree_exists,
        "car_brand_slug": car_brand_slug,
        "car_model_slug": car_model_slug,
        "car_modification_slug": car_modification_slug,
        "nodes": nodes,
        "tree_first_id": tree_first_id,
        "tree_first_node": tree_first_node,
        "tree_second_id": tree_second_id,
        "tree_third_id": tree_third_id,
        "level_root": level_root,
        "level_node": level_node,
        "skip_tree": skip_tree,
        "search_query": search_query,
        "tree_count": tree_count,
        "tree_sum": tree_sum,
        "carmodelcountry": carmodelcountry,
    }

    if not request.seo_data or not request.seo_data.seo_text:
        if tree_first_id or search_query:
            if tree_first_node:
                h1 = tree_first_node.title
            else:
                h1 = request.GET['search']
        else:
            h1 = 'Запчасти %s: %s %s %s' % (modification.car_model.car_brand.title_rus,
                                  modification.car_model.car_brand.title,
                                  modification.car_model.car_type_shot_key,
                                  modification.show_name)
        text = generate_text_from_template(template_id=13,
                               prebreadcrumbs='Запчасти для %s' % modification.car_model.car_brand.title,
                               h1=h1)
        seo = save_seo_data(request=request, text=text, title=h1)
        data['seo_data'] = seo

    if tree_first_id or search_query:
        return render(request, "catalog/tree_sub.html", data)
    else:
        if data['carmodelcountry'] and data['carmodelcountry'].title:
            data['title_save'] = data['carmodelcountry'].title
        else:
            data['title_save'] = data['modification'].car_model.car_type_shot_key
        if request.seo_data and request.seo_data.page_h1:
            data['h1_save'] = request.seo_data.page_h1
        else:
            data['h1_save'] = 'Запчасти %s %s %s %s' %(
                data['modification'].car_model.car_brand.title_rus,
                data['modification'].car_model.car_brand.title,
                data['title_save'],
                data['modification'].show_name)
        return render(request, "catalog/tree.html", data)


def tree_wo_modification(request, tree_first_id=None, tree_second_id=None,
                         tree_third_id=None, tree_forth_id=None):
    query = """
        WITH
                items AS (
                        SELECT
                            catalog_cartree.id
                        FROM catalog_cartree
                        WHERE catalog_cartree.type = 1
                        AND
                        CASE
                            WHEN %(tree_third_id)s > 0 THEN
                                ((catalog_cartree.parent_id = %(tree_third_id)s AND catalog_cartree.level=%(level_root)s)
                                OR (catalog_cartree.parent_id IN (SELECT id FROM catalog_cartree WHERE parent_id=%(tree_third_id)s) AND catalog_cartree.level=%(level_node)s))
                            WHEN %(tree_second_id)s > 0 THEN
                                ((catalog_cartree.parent_id = %(tree_second_id)s AND catalog_cartree.level=%(level_root)s)
                                OR (catalog_cartree.parent_id IN (SELECT id FROM catalog_cartree WHERE parent_id=%(tree_second_id)s) AND catalog_cartree.level=%(level_node)s))
                            WHEN %(tree_first_id)s > 0 THEN
                                ((catalog_cartree.parent_id = %(tree_first_id)s AND catalog_cartree.level=%(level_root)s)
                                OR (catalog_cartree.parent_id IN (SELECT id FROM catalog_cartree WHERE parent_id=%(tree_first_id)s) AND catalog_cartree.level=%(level_node)s))
                            ELSE
                                catalog_cartree.parent_id IS NOT NULL
                        END
                        GROUP BY catalog_cartree.id
                        ORDER by catalog_cartree.lft
                ),
                res (id, parent_id, level, show_on_main, title, lft) AS (
                    SELECT CCT.id, CCT.parent_id, CCT.level, CCT.show_on_main, CCT.title, CCT.lft
                    FROM catalog_cartree AS CCT
                    WHERE
                        CCT.parent_id IN (
                            SELECT * FROM items
                        )
                                        OR CCT.id IN (
                            SELECT * FROM items
                        )
                )
            SELECT id, level, lft
            FROM (
                SELECT
                    res.id,
                    row_number() OVER (PARTITION BY res.parent_id ORDER BY res.lft) as n,
                    res.level,
                    res.parent_id,
                    res.lft
                FROM res
                INNER JOIN (
                        SELECT catalog_cartree.id
                        FROM catalog_cartree
                        WHERE catalog_cartree.type = 1
                        AND
                        CASE
                            WHEN %(search_query)s IS NOT NULL THEN
                                catalog_cartree.title ILIKE %(search_query)s
                            ELSE
                                1=1
                            END
                        GROUP BY catalog_cartree.id
                ) AS query
                    ON res.id = query.id

                ORDER BY res.lft
            ) AS query
            WHERE
                CASE
                   WHEN %(tree_third_id)s > 0 THEN
                    level IN (%(level_root)s, %(level_node)s)
                   WHEN %(tree_second_id)s > 0 THEN
                    level IN (%(level_root)s, %(level_node)s)
                   WHEN %(tree_first_id)s > 0 THEN
                    level IN (%(level_root)s, %(level_node)s)
                   ELSE
                    level = %(level_root)s OR (level=%(level_node)s AND n < 7)
                END
            ORDER BY lft
    """

    tree_first_node = None
    level_root = 1
    level_node = 2
    skip_tree = False
    if tree_third_id:
        tree_first_node = get_object_or_404(CarTree, id=tree_third_id)
        level_root = 4
        level_node = 4
        skip_tree = True
    elif tree_second_id:
        tree_first_node = get_object_or_404(CarTree, id=tree_second_id)
        level_root = 3
        level_node = 4
    elif tree_first_id:
        tree_first_node = get_object_or_404(CarTree, id=tree_first_id)
        level_root = 2
        level_node = 3

    search_query = None
    if request.GET.get('search', None):
        search_query = '%{}%'.format(request.GET['search'])
    params = {
        'tree_first_id': tree_first_id,
        'tree_second_id': tree_second_id,
        'tree_third_id': tree_third_id,
        'level_root': level_root,
        'level_node': level_node,
        'search_query': search_query
    }
    tree_items = []
    tree_count = {}
    tree_sum = 0

    with get_connection().cursor() as c:
        c.execute(query, params)
        for item in c.fetchall():
            tree_items.append(item[0])
            tree_count[item[0]] = item[1]
            if item[2] == level_root or search_query:
                tree_sum += item[1]

    nodes = CarTree.objects.filter(id__in=tree_items,
                                   level__in=[level_root, level_node]).all()
    data = {
        "nodes": nodes,
        "tree_first_id": tree_first_id,
        "tree_first_node": tree_first_node,
        "tree_second_id": tree_second_id,
        "tree_third_id": tree_third_id,
        "level_root": level_root,
        "level_node": level_node,
        "skip_tree": skip_tree,
        "search_query": search_query,
        "tree_count": tree_count,
        "tree_sum": tree_sum,

    }

    h1 = '''%s - автомобильные запчасти по лучшей цене,
                интернет магазине автозапчастей Automobi.ua''' % tree_first_node.title
    desc = '''Качественные автозапчасти, %s и другие
              комплектующие к авто - доставка в любой город Украины,
              цены и условия на сайте Automobi''' % tree_first_node.title
    data['title'] = h1
    data['desc'] = desc
    if not request.seo_data or not request.seo_data.seo_text:
        seo = save_seo_data(request=request, title=h1)
        data['seo_data'] = seo

    if tree_first_id or search_query:
        return render(request, "catalog/tree_sub.html", data)
    else:
        return render(request, "catalog/tree.html", data)


def tree_wo_modification_part(request, tree_first_id=None, tree_second_id=None,
                              tree_third_id=None, tree_forth_id=None):

    alphabet = []
    grouped_brands = {}
    spm_brands_qs = CarBrand.objects.filter(is_passenger=True,
                                            is_shown=True,
                                            to_main=True).order_by('title')
    brands_passenger_list = list(spm_brands_qs)
    for brand in brands_passenger_list:
        first_letter = brand.title[:1]
        if first_letter not in alphabet:
            alphabet.append(first_letter)
        if first_letter not in grouped_brands:
            grouped_brands[first_letter] = []
        grouped_brands[first_letter].append(brand)

    if tree_third_id:
        tree_first_node = get_object_or_404(CarTree, id=tree_third_id)
    elif tree_second_id:
        tree_first_node = get_object_or_404(CarTree, id=tree_second_id)
    elif tree_first_id:
        tree_first_node = get_object_or_404(CarTree, id=tree_first_id)

    redirect_tree = ''
    if tree_first_id:
        redirect_tree += '%s/' % tree_first_id
    if tree_second_id:
        redirect_tree += '%s/' % tree_second_id
    if tree_third_id:
        redirect_tree += '%s/' % tree_third_id
    if tree_forth_id:
        redirect_tree += '%s/' % tree_forth_id

    h1 = '''%s - автомобильные запчасти по лучшей цене,
                интернет магазине автозапчастей Automobi.ua''' % tree_first_node.title
    desc = '''Качественные автозапчасти, %s и другие
              комплектующие к авто - доставка в любой город Украины,
              цены и условия на сайте Automobi''' % tree_first_node.title

    data = {
        "alphabet": alphabet,
        "grouped_brands": grouped_brands,
        "title": h1,
        "desc": desc,
        "tree_first_node": tree_first_node,
        "spm_brands_qs": spm_brands_qs,
        "redirect_tree": redirect_tree

    }
    return render(request, "catalog/tree_wo.html", data)


def get_tree(request, car_brand_slug, car_model_slug, car_modification_slug):
    modification = get_object_or_404(CarModification,
                                     slug=car_modification_slug)
    modification_id = modification.id

    tree_id = request.GET.get('node', None)

    query = """
        SELECT catalog_cartree.id, catalog_cartree.title, catalog_cartree.slug,
            EXISTS(SELECT 1
                   FROM catalog_linkcarmodificationgenericarticle lcmga
                   JOIN catalog_searchtreegenericarticle stga ON stga.generic_article_id = lcmga.generic_article_id
                   JOIN catalog_cartree ct ON ct.id = stga.tree_id
                   WHERE lcmga.car_modification_id = %(modification_id)s
                   AND parent_id = catalog_cartree.id
                   AND ct.type = 1
                   GROUP BY ct.id
                   ORDER by ct.title
            )
        FROM catalog_linkcarmodificationgenericarticle
        JOIN catalog_searchtreegenericarticle ON catalog_searchtreegenericarticle.generic_article_id = catalog_linkcarmodificationgenericarticle.generic_article_id
        JOIN catalog_cartree ON catalog_cartree.id = catalog_searchtreegenericarticle.tree_id
        WHERE catalog_linkcarmodificationgenericarticle.car_modification_id = %(modification_id)s
        AND parent_id {parent_id}
        AND catalog_cartree.type = 1
        GROUP BY catalog_cartree.id
        ORDER by catalog_cartree.title
        """

    params = {'modification_id': modification_id}
    if tree_id:
        parent_id = "= %s" % tree_id
    else:
        parent_id = """
            IN (
                SELECT catalog_cartree.id
                FROM catalog_linkcarmodificationgenericarticle
                JOIN catalog_searchtreegenericarticle ON catalog_searchtreegenericarticle.generic_article_id = catalog_linkcarmodificationgenericarticle.generic_article_id
                JOIN catalog_cartree ON catalog_cartree.id = catalog_searchtreegenericarticle.tree_id
                WHERE catalog_linkcarmodificationgenericarticle.car_modification_id = %(modification_id)s
                AND parent_id IS NULL
                AND catalog_cartree.type = 1
            )
            """
    query = query.format(
        parent_id=parent_id)

    with get_connection().cursor() as c:
        c.execute(query, params)
        tree_items = c.fetchall()

    tree = []
    if tree_items:
        for tree_item in tree_items:
            tree.append({
                'id': tree_item[0],
                'label': tree_item[1],
                'load_on_demand': tree_item[3],
                'url': reverse('catalog:parts',
                               args=(car_brand_slug, car_model_slug,
                                     car_modification_slug, tree_item[2])),
            })

    js = json.dumps(tree)
    return HttpResponse(js, content_type="application/json")


def get_modification_url(request):
    modification_id = request.GET.get('modification_id', None)

    modification = get_object_or_404(CarModification,
                                     id=modification_id)

    js = json.dumps({
        'redirect': reverse(
            'catalog:tree',
            kwargs={
                'car_brand_slug': modification.car_model.car_brand.slug,
                'car_model_slug': modification.car_model.slug,
                'car_modification_slug': modification.slug
            }),
    })
    return HttpResponse(js, content_type="application/json")


def parts_redirect(request, car_brand_slug, car_model_slug,
                   car_modification_slug, car_tree_slug, permanent=True):
    tree_item = get_object_or_404(CarTree, slug=car_tree_slug)

    url = ''
    if tree_item.level == 4:
        url = reverse('catalog:tree:tree:tree:tree:parts', kwargs={
            'car_brand_slug': car_brand_slug,
            'car_model_slug': car_model_slug,
            'car_modification_slug': car_modification_slug,
            'tree_first_id': tree_item.parent.parent.parent.id,
            'tree_second_id': tree_item.parent.parent.id,
            'tree_third_id': tree_item.parent.id,
            'tree_forth_id': tree_item.id,
        })
    elif tree_item.level == 3:
        url = reverse('catalog:tree:tree:tree:parts', kwargs={
            'car_brand_slug': car_brand_slug,
            'car_model_slug': car_model_slug,
            'car_modification_slug': car_modification_slug,
            'tree_first_id': tree_item.parent.parent.id,
            'tree_second_id': tree_item.parent.id,
            'tree_third_id': tree_item.id,
        })
    elif tree_item.level == 2:
        url = reverse('catalog:tree:tree:parts', kwargs={
            'car_brand_slug': car_brand_slug,
            'car_model_slug': car_model_slug,
            'car_modification_slug': car_modification_slug,
            'tree_first_id': tree_item.parent.id,
            'tree_second_id': tree_item.id,
        })
    elif tree_item.level == 1:
        url = reverse('catalog:tree:parts', kwargs={
            'car_brand_slug': car_brand_slug,
            'car_model_slug': car_model_slug,
            'car_modification_slug': car_modification_slug,
            'tree_first_id': tree_item.id,
        })
    if permanent:
        return HttpResponsePermanentRedirect(url)
    else:
        return redirect(url)


def parts(request, car_brand_slug, car_model_slug, car_modification_slug,
          tree_first_id=None, tree_second_id=None, tree_third_id=None,
          tree_forth_id=None, tm_id=None):
    # 301 redirect for get parametrs
    if request.GET.get('tm_id', None):
        tm_id = request.GET.get('tm_id', None)
        return HttpResponsePermanentRedirect(request.path + tm_id + '/')

    carmodel_country_id = request.session.get('model_country', None)
    carmodelcountry = None
    if carmodel_country_id and is_pk(carmodel_country_id):
        if CarModelCountry.objects.filter(id=carmodel_country_id).exists():
            carmodelcountry = CarModelCountry.objects.get(id=carmodel_country_id)

    car_tree_id = 0
    if tree_forth_id:
        car_tree_id = tree_forth_id
    elif tree_third_id:
        car_tree_id = tree_third_id
    elif tree_second_id:
        car_tree_id = tree_second_id
    elif tree_first_id:
        car_tree_id = tree_first_id
    tree_item = get_object_or_404(CarTree, id=car_tree_id)
    modification = get_object_or_404(CarModification, slug=car_modification_slug)

    part_ids = []
    trademarks = []
    parts = {}
    result = {}
    trademarks_dict = OrderedDict()

    with get_connection().cursor() as cur:
        query, params = prepare_query('catalog_criteria',
                                      modification_filter=modification.id,
                                      tree_filter=tree_item.id)
        cur.execute(query, params)
        parts_criteria = cur.fetchall()

    for item in parts_criteria:
        criteria_part = {}
        criteria_mod = {}
        for i in item[1]:
            if criteria_part.get(i['f1'], None):
                criteria_part[i['f1']].append(i['f2'])
            else:
                criteria_part[i['f1']] = [i['f2'], ]
        for ii in item[2]:
            if criteria_mod.get(ii['f1'], None):
                criteria_mod[ii['f1']].append(ii['f2'])
            else:
                criteria_mod[ii['f1']] = [ii['f2'], ]
        criteria_part_string = ''
        for k, i in criteria_part.items():
            if i:
                try:
                    criteria_part_string += '%s: <span style="font-weight: bold;">%s</span><br />' % (k, ', '.join(i))
                except TypeError:
                    pass
            else:
                criteria_part_string += '<span style="font-weight: bold;">%s</span><br />' % k
        criteria_mod_string = ''
        for k, i in criteria_mod.items():
            if i:
                try:
                    criteria_mod_string += '%s: <span style="font-weight: bold;">%s</span><br />' % (k, ', '.join(i))
                except TypeError:
                    pass
            else:
                criteria_mod_string += '<span style="font-weight: bold;">%s</span><br />' % k

        parts[item[0]] = {
            'part_id': item[0],
            'criteria_part': criteria_part_string,
            'criteria_mod': criteria_mod_string
        }
        part_ids.append(item[0])

    if part_ids:
        query = """
            SELECT DISTINCT ON (parts.id)
                parts.id,
                trademarks.id,
                trademarks.show_name,
                CASE
                    WHEN pdt.id IS NOT NULL
                        THEN pdt.value
                    ELSE parts.description
                END, --description,
                parts.marks,
                parts.search_number,
                parts.slug,
                parts.img_quantity,
                parts.catalog_number,
                cpi.image
            FROM catalog_part parts
            LEFT JOIN catalog_trademark trademarks ON trademarks.id = parts.tm_id
            LEFT JOIN catalog_partdescriptiontext pdt ON pdt.part_description_id = parts.default_description_id AND language_id = 16
            LEFT JOIN catalog_partimage cpi ON cpi.part_id = parts.id
            WHERE parts.id = ANY(%(part_ids)s)
        """

        if tm_id:
            query += ' AND parts.tm_id = %s' % tm_id
        query += 'ORDER BY parts.id, trademarks.show_name ASC'
        params = {'part_ids': part_ids}
        with get_connection('api').cursor() as c:
            c.execute(query, params)
            trademarks = c.fetchall()

        search_numbers = []
        for item in trademarks:
            trademarks_dict[item[1]] = item[2]
            search_numbers.append(item[5])
            result[item[0]] = {
                'part_id': parts[item[0]]['part_id'],
                'criteria_part': parts[item[0]]['criteria_part'],
                'criteria_mod': parts[item[0]]['criteria_mod'],
                'trademarks_id': item[1],
                'trademarks_show_name': item[2],
                'description': item[3],
                'marks': item[4],
                'search_number': item[5],
                'slug': item[6],
                'img_quantity': item[7],
                'catalog_number': item[8],
                'image': item[9],
            }

        basic_currency_id = get_default_currency().id

        role = request.role
        dc_id, so_id, client_id, supplier_id, shortcut = get_roles_ids(role)
        if isinstance(dc_id, int):
            dc_id = (dc_id, )
        currency_rates = get_currency_rates(dc_id, so_id, client_id,
                                            supplier_id, basic_currency_id)

        sales_price_list = get_sales_price(dc_id, so_id, client_id,
                                           supplier_id, shortcut)

        user_currency_id = getattr(request.currency, 'id', basic_currency_id)
        sorting = ('price', 'asc')

        api_response = api_search(
            search_numbers,
            basic_currency_id,
            user_currency_id,
            json.dumps(currency_rates),
            json.dumps(sales_price_list),
            False,
            json.dumps(sorting),
            shortcut,
            '{}',
            so_id,
            client_id,
            False,
            part_ids,
            True,
            None,
            False
        )

        part_mapping = api_response['parts']
        stock = {}
        delivery = {}
        prices = {}
        for items in part_mapping:
            for item in items:
                if stock.get(item[19], None):
                        stock[item[19]].append(item[8])
                else:
                    stock[item[19]] = [item[8]]

                if delivery.get(item[19], None):
                    delivery[item[19]].append(item[13])
                else:
                    delivery[item[19]] = [item[13]]

                if item[16]:
                    if prices.get(item[19], None):
                        prices[item[19]].append(item[16])
                    else:
                        prices[item[19]] = [item[16]]

        for item in part_mapping:
            if result.get(item[0][19], None):
                result[item[0][19]]['price'] = item[0][16]
                if item[0][16]:
                    result[item[0][19]]['sort'] = 1
                else:
                    result[item[0][19]]['sort'] = 0
                result[item[0][19]]['delivery'] = item[0][12]
                result[item[0][19]]['count'] = item[0][18]
                result[item[0][19]]['pli_id'] = item[0][0]
                result[item[0][19]]['spl_id'] = item[0][1]
                result[item[0][19]]['max_stock'] = max(stock[item[0][19]])
                result[item[0][19]]['min_stock'] = min(stock[item[0][19]])
                result[item[0][19]]['max_delivery'] = max(delivery[item[0][19]])
                result[item[0][19]]['min_delivery'] = min(delivery[item[0][19]])
                try:
                    result[item[0][19]]['more_prices'] = True \
                        if len(set(prices[item[0][19]])) > 1 else False
                except KeyError:
                    result[item[0][19]]['more_prices'] = False
    empty_parts = []
    analogs = []
    sales_price_list_ids = []
    for item in part_ids:
        if result.get(item, None) and not result[item]['pli_id']:
            empty_parts.append(item)
    if empty_parts:
        query = """
            SELECT PART.id FROM ppl_pricelistitem PPL
            JOIN catalog_cross CR
                ON CR.cross_id = PPL.part_id
            JOIN catalog_part PART
                ON CR.part_id = PART.id
            JOIN spl_salespricelistitem SPLI
              ON SPLI.primary_ppl_id = PPL.price_list_id
              AND SPLI.sales_price_list_id = ANY(%(sales_price_list_ids)s)
            WHERE PART.id = ANY(%(empty_parts)s)
            AND PPL.active = True
            GROUP BY PART.id
        """
        for item in sales_price_list:
            sales_price_list_ids.append(item['id'])

        params = {'empty_parts': empty_parts,
                  'sales_price_list_ids': sales_price_list_ids}
        with get_connection('api').cursor() as c:
            c.execute(query, params)
            for item in c.fetchall():
                analogs.append(item[0])
    req_data = dict(request.GET)
    sort_item = request.session.get('sort_item', 'price')
    sort_type = request.session.get('sort_type', 'asc')
    result = sorted(result.items(), key=lambda x: getitem(x[1], 'sort'), reverse=True)
    if sort_item:
        try:
            result = sorted(result, key=lambda x: getitem(x[1], sort_item),
                            reverse=True if sort_type=='desc' else False)
        except KeyError:
            result = sorted(result, key=lambda x: getitem(x[1], 'price'))
    result = sorted(result, key=lambda x: getitem(x[1], 'sort'), reverse=True)

    data = {
        "req_data": req_data,
        "tm_id": tm_id,
        "modification": modification,
        "tree_item": tree_item,
        "parts": result,
        "trademarks": trademarks_dict,
        "display_type": request.session.get('display_type', 'list'),
        "sort_item": sort_item,
        "sort_type": sort_type,
        "carmodelcountry": carmodelcountry,
        "analogs": analogs,
    }

    if data['carmodelcountry']:
        title = data['carmodelcountry'].title
    else:
        title = data['modification'].car_model.car_type_shot_key
    tm = None
    if tm_id:
        tm = TradeMark.objects.filter(id=tm_id).first()
    if request.seo_data and request.seo_data.page_h1:
        data['h1_save'] = request.seo_data.page_h1
    else:
        data['h1_save'] = '%s %s: %s %s %s %s %sл.с. %s' % (
            data['tree_item'].title,
            data['modification'].car_model.car_brand.title_rus,
            data['modification'].car_model.car_brand.title,
            title,
            data['modification'].show_name,
            data['modification'].engine,
            data['modification'].hp_engine_power,
            tm.show_name if tm else '')

    if not request.seo_data or not request.seo_data.seo_text:
        i = 0
        products = '<br />'
        for item in result:
            item = item[1]
            if i >= 4:
                break
            i += 1
            products += ' - <a href="/catalog/product/%s">%s %s %s</a><br />' % (
                item['slug'],
                item['trademarks_show_name'],
                item['search_number'],
                item['description'])
        text = generate_text_from_template(template_id=10,
                               prebreadcrumbs=modification.show_name,
                               h1=data['h1_save'],
                               additional=products)
        seo = save_seo_data(request=request, text=text, title=data['h1_save'])
        data['seo_data'] = seo

    return render(request, "catalog/parts.html", data)


def category(request, category_id):
    category = get_object_or_404(GoodsCategory, id=category_id)

    data = {
        "category": category,
    }

    return render(request, "catalog/category.html", data)


class AutomobiCatalog(TemplateView):
    template_name = 'catalog/automobi_catalog.html'
    paginate_by = 10
    order_by = 'price_asc'
    cookie_dict = {}

    def dispatch(self, request, *args, **kwargs):
        # 301 redirect for order get parameter
        if self.kwargs.get('order_by', None):
            path = self.request.path
            path = path.replace('/order-price_asc', '')
            path = path.replace('/order-price_desc', '')
            return HttpResponsePermanentRedirect(path)

        # 301 redirect for paginate get parameter
        if self.kwargs.get('paginate_by', None):
            path = self.request.path
            path = path.replace('/paginate_by-10', '')
            path = path.replace('/paginate_by-25', '')
            return HttpResponsePermanentRedirect(path)

        # 301 redirect for filer get parameter
        if self.kwargs.get('filter', None):
            path = self.request.path
            path = path.replace('filter-', 'filters-')
            return HttpResponsePermanentRedirect(path)

        # 301 redirect for page-1
        if self.kwargs.get('page', None) == '1':
            path = self.request.path
            path = path.replace('/page-1', '')
            return HttpResponsePermanentRedirect(path)

        return super(AutomobiCatalog, self).dispatch(request, *args, **kwargs)

    def render_to_response(self, context, **response_kwargs):
        response = super(AutomobiCatalog, self).render_to_response(context, **response_kwargs)
        if self.cookie_dict:
            max_age = 3600 * 24 * 365  # one year
            for key, value in self.cookie_dict.iteritems():
                response.set_cookie(key, value, max_age=max_age)
        return response

    def get_paginate_by(self, queryset=None):
        if self.request.COOKIES.get('paginate_by', None):
            paginate_by = int(self.request.COOKIES.get('paginate_by'))
        else:
            paginate_by = self.paginate_by

        display_type = self.request.session.get('display_type', 'list')
        if display_type == 'grid':
            return get_multiply_four(paginate_by)

        return paginate_by

    def get_order_by(self, queryset=None):
        if self.request.COOKIES.get('order_by', None):
            order_by = self.request.COOKIES.get('order_by')
        else:
            order_by = self.order_by
        return order_by

    def order_queryset(self, queryset):
        return queryset.order_by()

    def get_filters(self):
        filters = self.kwargs.get('filters', 'filters-')
        try:
            filters = list(set(filters.split('filters-')))
        except AttributeError:
            filters = []
        try:
            filter_items = filters[0]
        except IndexError:
            filter_items = None

        if filter_items:
            filters = filter_items.split('-')

        if filters == ['']:
            filters = []

        return filters

    def get_items(self, ac_list):
        page = 1
        if self.kwargs.get('page', None):
            page = int(self.kwargs['page'])

        basic_currency_id = get_default_currency().id

        role = self.request.role
        dc_id, so_id, client_id, supplier_id, shortcut = get_roles_ids(role)
        if isinstance(dc_id, int):
            dc_id = (dc_id, )
        currency_rates = get_currency_rates(dc_id, so_id, client_id,
                                            supplier_id, basic_currency_id)

        sales_price_list = get_sales_price(dc_id, so_id, client_id,
                                           supplier_id, shortcut)

        user_currency_id = getattr(self.request.currency, 'id', basic_currency_id)
        offset = (page-1) * self.get_paginate_by()

        filters_dict = {}
        filters = self.get_filters()

        if filters:
            filters_data = ACPropertyValue.objects.filter(id__in=filters).all()
            for item in filters_data:
                if not filters_dict.get(item.ac_property_id, None):
                    filters_dict.setdefault(item.ac_property_id, [item.id])
                else:
                    filters_dict[item.ac_property_id].append(item.id)

        api_response = api_catalog(
            json.dumps(map(unicode, ac_list)),
            json.dumps(sales_price_list),
            basic_currency_id,
            user_currency_id,
            json.dumps(currency_rates),
            shortcut,
            self.get_order_by(),
            json.dumps(filters_dict),
            so_id,
            client_id,
            self.get_paginate_by(),
            offset
        )

        return api_response

    def get_extra_h1(self, filters_val):
        special_props = ("Производитель", "Состав", "Сезонность")
        result_str = ''
        if not filters_val:
            return ''
        prop_value_qs = ACPropertyValue.objects.filter(id__in=filters_val)
        order_props_qs = prop_value_qs.extra(
            select={'seo_order': '''SELECT true FROM catalog_acpropertyvalue val
                                    INNER JOIN catalog_acproperty property
                                    ON val.ac_property_id = property.id
                                    WHERE property.title IN %s
                                      AND val.id = catalog_acpropertyvalue.id'''},
            select_params=(special_props, ),
            order_by=['seo_order'])
        nofollow = False if order_props_qs.count() < 2 else True
        result_str += ' ' if order_props_qs.first() and order_props_qs.first().seo_order else ' -'
        first_entry = True if order_props_qs.first() and order_props_qs.first().seo_order else False
        for index, item in enumerate(order_props_qs):
            result_str += ', ' if index != 0 else ''
            if item.seo_order:
                result_str += item.title
            else:
                result_str += ' - ' if first_entry else ' '
                result_str += item.ac_property.title.lower()
                result_str += ': ' + item.title
                first_entry = False
        return result_str, nofollow

    def get_context_data(self, **kwargs):
        context = super(AutomobiCatalog, self).get_context_data()

        context['ac_search_form'] = ACSearchForm()
        context['ac_search_category'] = ACSearchCategoryForm()

        page = 1
        if self.kwargs.get('page', None):
            page = int(self.kwargs['page'])
        ac_slug = None
        if self.kwargs.get('fifth_slug', None):
            ac_slug = self.kwargs.get('fifth_slug')
        elif self.kwargs.get('forth_slug', None):
            ac_slug = self.kwargs.get('forth_slug')
        elif self.kwargs.get('third_slug', None):
            ac_slug = self.kwargs.get('third_slug')
        elif self.kwargs.get('second_slug', None):
            ac_slug = self.kwargs.get('second_slug')
        elif self.kwargs.get('first_slug', None):
            ac_slug = self.kwargs.get('first_slug')

        context['first_slug'] = self.kwargs.get('first_slug')
        context['second_slug'] = self.kwargs.get('second_slug')
        context['third_slug'] = self.kwargs.get('third_slug')
        context['forth_slug'] = self.kwargs.get('forth_slug')
        context['fifth_slug'] = self.kwargs.get('fifth_slug')
        ac_list = []

        if ac_slug:
            ac_list = AC.objects.filter(slug=ac_slug)\
                .exclude(parent_id__exact=None)\
                .values_list('id', flat=True)
            if not ac_list:
                ac_obj = get_object_or_404(AC, slug=ac_slug)
                children = ac_obj.get_children().values_list('id', flat=True)
                if children:
                    ac_list = children
                else:
                    raise Http404
        catalog = self.get_items(ac_list)
        try:
            context['items'] = catalog['items']
            context['count'] = catalog['count']
            context['filters'] = self.make_filters(catalog['filters'])
        except KeyError:
            context['items'] = []
            context['count'] = 0
            context['filters'] = []
        context['paginate_by'] = self.get_paginate_by()
        context['order_by'] = self.get_order_by()
        context['page'] = page
        context['pages'] = int(math.ceil(context['count'] / context['paginate_by']))+1
        context['pages_list'] = list(range(1, context['pages']+1))
        context['next_page'] = context['page'] + 1
        context['prev_page'] = context['page'] - 1
        context['filters_val'] = self.get_filters()
        filter_h1 = None
        if context['filters_val']:
            filter_h1, nofollow = self.get_extra_h1(context['filters_val'])
            context['filter_h1'] = filter_h1
            context['nofollow'] = nofollow
        context['filters_string'] = '-'.join(self.get_filters())
        context['req_data'] = dict(filter(lambda x: filter(bool, x[1]),
                                          self.request.GET.iterlists()))

        ac_properties = ACProperty.objects.all()

        context['acs_sel_hierarchy'] = AC.objects.all()
        context['ac_sel'] = AC.objects.filter(slug=ac_slug).first()
        context['ac_properties'] = ac_properties
        context['basic_currency'] = get_default_currency()

        item_props = {}
        for item in catalog['items']:
            render_props = []
            for prop in item[6]:
                render_props.append((prop['f2'], prop['f4']))
            if context['ac_sel']:
                render_props.append(
                    (u'Тип товара', context['ac_sel'].title))
            item_props[item[0]] = render_props
        context['item_props'] = json.dumps(item_props)

        if context['ac_sel']:
            context['acs_sel_branch'] = \
                context['ac_sel'].get_ancestors(include_self=True)

        display_type = self.request.GET.get('display_type', None)
        if display_type:
            self.request.session['display_type'] = display_type

        context['display_type'] = self.request.session.get('display_type', 'list')

        if self.request.seo_data and self.request.seo_data.page_h1:
            context['h1_save'] = self.request.seo_data.page_h1
        else:
            if filter_h1:
                context['h1_save'] = '%s %s' % (context['ac_sel'].title, filter_h1)
            else:
                context['h1_save'] = context['ac_sel'].title

            if page != 1:
                context['h1_save'] = '%s - страница №%s' % (context['h1_save'], page)

        if (not self.request.seo_data or not self.request.seo_data.seo_text) \
                and len(context['filters_val']) <= 2:
            i = 0
            products = '<br />'
            for item in catalog['items']:
                if i >= 4:
                    break
                i += 1
                products += ' - <a href="/catalog/product/%s">%s</a><br />' % (item[7], item[2])
            prebreadcrumbs = context['ac_sel'].parent.title \
                if context['ac_sel'].parent else context['ac_sel'].title
            text = generate_text_from_template(
                template_id=10,
                prebreadcrumbs=prebreadcrumbs,
                h1=context['h1_save'],
                additional=products)
            seo = save_seo_data(request=self.request, text=text, title=context['ac_sel'].title)
            context['seo_data'] = seo

        return context

    def make_filters(self, filters):
        result = OrderedDict()
        for item in filters:
            if not result.get(item[1], None):
                result.setdefault(item[1], [])
            result[item[1]].append(item)

        return result

    def get_queryset(self):
        return []


class AutomobiCatalogPart(TemplateView):
    template_name = 'catalog/automobi_catalog_part.html'


class AutomobiCatalogPartUnit(TemplateView):
    template_name = 'catalog/automobi_catalog_part_unit.html'


class OEMView(TemplateView):

    def get(self, request, *args, **kwargs):
        result = OEM().request(self.service_name, **self.get_api_kwargs())
        if self.get_api_kwargs().get('VIN', False) and isinstance(result, dict):
            result['VIN'] = self.get_api_kwargs().get('VIN')
        context = self.get_context_data(result=result, **kwargs)
        return self.render_to_response(context)


class OEMVehicle(OEMView):
    service_name = 'FindVehicleByVin'
    template_name = 'catalog/oem_vehicle.html'

    def get_api_kwargs(self):
        vin = self.kwargs.get('vin')
        user = None if self.request.user.is_anonymous() else self.request.user
        x_forwarded_for = self.request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            user_ip = x_forwarded_for.split(',')[-1].strip()
        else:
            user_ip = self.request.META.get('REMOTE_ADDR')

        if not VinHistory.objects.filter(
                user=user, vin=vin, user_ip=user_ip).exists():
            vin_history = VinHistory.objects.create(
                user=user,
                vin=vin,
                user_ip=user_ip)

            if user is None:
                if 'vin_history' not in self.request.session:
                    self.request.session['vin_history'] = []
                self.request.session['vin_history'].append(vin_history.id)
                self.request.session.modified = True

        return {'VIN': vin}


class OEMGroups(OEMView):
    service_name = 'ListQuickGroup'
    template_name = 'catalog/oem_groups.html'

    # TODO rewrite after get ALL info about laximo
    def dispatch(self, request, *args, **kwargs):
        try:
            OEM().request(self.service_name, **self.get_api_kwargs())
        except suds.WebFault:
            params = urllib.urlencode(self.request.GET)
            return redirect(reverse(
                'catalog:oem_groups_only_lc') + "?%s" % params)
        return super(OEMGroups, self).dispatch(request, *args, **kwargs)

    def get_api_kwargs(self):
        data = self.request.GET
        return {
            'Catalog': data.get('catalog'),
            'VehicleId': data.get('vehicle_id'),
            'ssd': data.get('ssd'),
        }

    def get_context_data(self, **kwargs):
        context = super(OEMGroups, self).get_context_data(**kwargs)
        api_params = self.get_api_kwargs().copy()
        api_params['CategoryId'] = u'-1'
        categories_list = OEM().request('ListCategories', **api_params)
        get_vehicle_info = OEM().request('GetVehicleInfo',
                                         **self.get_api_kwargs())
        context['categories_list'] = categories_list
        context['get_vehicle_info'] = get_vehicle_info
        return context


class OEMGroupsOnlyLC(OEMView):
    service_name = 'ListCategories'
    template_name = 'catalog/oem_groups.html'

    def get_api_kwargs(self):
        data = self.request.GET
        return {
            'Catalog': data.get('catalog'),
            'VehicleId': data.get('vehicle_id'),
            'ssd': data.get('ssd'),
            'CategoryId': u'-1',
        }

    def get_context_data(self, **kwargs):
        context = super(OEMGroupsOnlyLC, self).get_context_data(**kwargs)
        get_vehicle_info = OEM().request('GetVehicleInfo',
                                         **self.get_api_kwargs())
        context['categories_list'] = context.pop('result')
        context['get_vehicle_info'] = get_vehicle_info
        return context


class OEMDetail(OEMView):
    service_name = 'ListQuickDetail'
    template_name = 'catalog/oem_detail.html'

    def get_api_kwargs(self):
        data = self.request.GET
        return {
            'Catalog': data.get('catalog'),
            'VehicleId': data.get('vehicle_id'),
            'QuickGroupId': data.get('quick_group_id', ''),
            'ssd': data.get('ssd'),
        }


class OEMDetailByUnit(TemplateView):
    template_name = 'catalog/oem_detail_by_unit.html'

    def get(self, request, *args, **kwargs):
        data = self.request.GET
        query_data = {
            'Catalog': data.get('catalog'),
            'UnitId': data.get('unit_id'),
            'ssd': data.get('ssd'),
        }
        queries = (('ListDetailByUnit', query_data),
                   ('ListImageMapByUnit', query_data))
        result = OEM().multiple_request(*queries)
        if result:
            img_map = result['ListImageMapByUnit']
            detail_unit = result['ListDetailsByUnit']
            if img_map and detail_unit:
                if isinstance(img_map.get('row'), dict):
                    item = img_map.get('row')
                    if isinstance(result['ListDetailsByUnit'].get('row'), dict):
                        one_detail = result.get('ListDetailsByUnit').get('row')
                        if one_detail['@codeonimage'] == item['@code']:
                            if '@note' in one_detail:
                                item['@note'] = one_detail['@note']
                            if '@name' in one_detail:
                                item['@name'] = one_detail['@name']
                    else:
                        for one_detail in result['ListDetailsByUnit'].get('row'):
                            if one_detail['@codeonimage'] == item['@code']:
                                if '@note' in one_detail:
                                    item['@note'] = one_detail['@note']
                                if '@name' in one_detail:
                                    item['@name'] = one_detail['@name']
                else:
                    for item in img_map.get('row'):
                        if isinstance(result['ListDetailsByUnit'].get('row'), dict):
                            one_detail = result.get('ListDetailsByUnit').get('row')
                            if isinstance(item, basestring):
                                raise TypeError('Check views.py:741 item = %' % item)
                            if one_detail['@codeonimage'] == item['@code']:
                                if '@note' in one_detail:
                                    item['@note'] = one_detail['@note']
                                if '@name' in one_detail:
                                    item['@name'] = one_detail['@name']
                        else:
                            for one_detail in result['ListDetailsByUnit'].get('row'):
                                if isinstance(item, basestring):
                                    raise TypeError('Check views.py:741 item = %' % item)
                                if one_detail['@codeonimage'] == item['@code']:
                                    if '@note' in one_detail:
                                        item['@note'] = one_detail['@note']
                                    if '@name' in one_detail:
                                        item['@name'] = one_detail['@name']

        api_params = {
            'Catalog': data.get('catalog'),
            'VehicleId': data.get('vehicle_id'),
            'ssd': data.get('ssd'),
        }
        get_vehicle_info = OEM().request('GetVehicleInfo',
                                         **api_params)
        context = self.get_context_data(result=result,
                                        get_vehicle_info=get_vehicle_info)
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(OEMDetailByUnit, self).get_context_data(**kwargs)
        data = self.request.GET
        magic_k = data.get('k', '')
        context['image_url'] = data.get('image_url') + '&k=' + magic_k
        context['unit_name'] = data.get('unit_name')
        if context['image_url']:
            file = cStringIO.StringIO(urllib.urlopen(
                context['image_url']).read())
            im = Image.open(file)
            width, height = im.size
            context['img_size'] = [width, height]
        return context


class OEMChildMenu(TemplateView):
    template_name = 'catalog/child_menu.html'

    def post(self, request, *args, **kwargs):
        menu_items = self.request.POST.getlist('menu_items', [])
        parent = self.request.POST.get('parent', None)
        context = self.get_context_data(menu_items=menu_items, parent=parent)
        return self.render_to_response(context)


class OEMListUnit(TemplateView):
    service_name = 'ListUnits'
    template_name = 'catalog/list_unit.html'

    def get_api_kwargs(self):
        data = self.request.GET
        return {
            'Catalog': data.get('catalog'),
            'VehicleId': data.get('vehicle_id'),
            'CategoryId': data.get('category_id', ''),
            'ssd': data.get('ssd'),
        }

    def get(self, request, *args, **kwargs):
        api_params = self.get_api_kwargs()
        result = []
        if api_params.get('CategoryId', '-1') == '-1':
            categories_list = OEM().request('ListCategories', **api_params)
            for row in categories_list:
                category_id = row.get('@categoryid', None)
                if not category_id:
                    continue
                api_params['CategoryId'] = category_id
                row_result = OEM().request(self.service_name, **api_params)
                if row_result:
                    result += row_result
        else:
            result = OEM().request(self.service_name, **api_params)
        context = self.get_context_data(result=result, **kwargs)
        return self.render_to_response(context)


class OEMDetailedSearch(TemplateView):
    template_name = 'catalog/car_search_form_details_part.html'

    def get(self, request, *args, **kwargs):
        data = self.request.GET
        service_name = 'GetCatalogInfo'
        service_name_gw = 'GetWizard'
        service_name_gw2 = 'GetWizard2'
        param = {
            'Catalog': data.get('val_param'),
            'ssd': '',
        }
        gw_param = param.copy()
        gw_param['WizardId'] = ''
        gw_param['ValueId'] = ''
        allowed_gw = str2bool(data.get('wizard'))
        allowed_gw2 = str2bool(data.get('wizard2'))
        if allowed_gw2:
            if data.get('catalog'):
                param['Catalog'] = data.get('catalog')
                param['ssd'] = data.get('val_param')
            result = OEM().request(service_name_gw2, **param)
        elif allowed_gw:
            if data.get('catalog'):
                gw_param['Catalog'] = data.get('catalog')
                gw_param['ValueId'] = data.get('val_param')
                param['Catalog'] = data.get('catalog')
            if data.get('wizardid') != 'None':
                gw_param['WizardId'] = data.get('wizardid')
            result = OEM().request(service_name_gw, **gw_param)
        else:
            result = OEM().request(service_name, **param)
        context = self.get_context_data(result=result,
                                        catalog=param['Catalog'],
                                        wizard=data.get('wizard'),
                                        wizard2=data.get('wizard2'),
                                        ssd=data.get('val_param'))
        return self.render_to_response(context)


class OEMExtendsSelective(TemplateView):
    template_name = 'catalog/oem_models.html'

    def get(self, request, *args, **kwargs):
        data = self.request.GET
        vin = ''
        if 'car_VIN' in data:
            service_name = 'FindVehicleByVin'
            self.template_name = 'catalog/oem_vehicle.html'
            param = {'VIN': data.get('car_VIN').strip()}
            vin = param['VIN']
        else:
            service_name = 'FindVehicleByWizard2'
            param = {
                'Catalog': data.get('car_brand'),
                'ssd': data.get('current_ssd'),
            }

            if data.get('wizardid') not in ['None', '']:
                service_name = 'FindVehicleByWizard'
                param['WizardId'] = data.get('wizardid')

        result = OEM().request(service_name, **param)
        context = self.get_context_data(result=result, **kwargs)
        context['request_vin'] = vin
        return self.render_to_response(context)


class PartInfo(TemplateView):
    template_name = 'catalog/catalog_part_info_modal.html'

    def get_context_data(self, **kwargs):
        part_id = kwargs.get('part_id')
        if part_id:
            part = get_object_or_404(Part, id=part_id)
            kwargs['part'] = part

        query = """
            SELECT DISTINCT
                lcmga.car_modification_id
            FROM catalog_linkgenericarticlepart lgap
            JOIN catalog_linkcarmodificationgenericarticle lcmga ON lgap.id = lcmga.link_generic_article_part_id
            WHERE lgap.part_id = %(part_id)s
            ORDER BY lcmga.car_modification_id
        """

        params = {'part_id': part.id}

        modifications = []
        with get_connection().cursor() as c:
            c.execute(query, params)
            modifications_list = c.fetchall()
            modifications = [i[0] for i in modifications_list]

        kwargs['modification_ids'] = modifications

        return kwargs


class FpsCatalog(TemplateView):
    template_name = 'catalog/fps_catalog.html'


class PartDetails(TemplateView):
    template_name = 'catalog/part_details.html'

    def get_context_data(self, **kwargs):
        context = super(PartDetails, self).get_context_data()
        part_slug = self.kwargs.get('part_slug')
        modification_id = self.request.session.get('catalog_modification', None)
        tree_id = self.request.session.get('catalog_tree', None)
        carmodel_country_id = self.request.session.get('model_country', None)
        carmodelcountry = None
        if carmodel_country_id and is_pk(carmodel_country_id):
            if CarModelCountry.objects.filter(id=carmodel_country_id).exists():
                carmodelcountry = CarModelCountry.objects.get(id=carmodel_country_id)

        if modification_id:
            context['modification'] = get_object_or_404(CarModification,
                                                        id=modification_id)
        if tree_id:
            context['tree'] = get_object_or_404(CarTree, id=tree_id)

        part = get_object_or_404(Part, slug=part_slug)
        matched, cross_other_tm, cross_same_tm = self.get_prices(part)

        context['part'] = part

        context['ac_part'] = part.ac_set.first()

        for key, item in matched.iteritems():
            if item:
                context['price'] = item[0]
        context['matched'] = matched
        context['cross_same_tm'] = cross_same_tm
        context['cross_other_tm'] = cross_other_tm

        try:
            if cross_other_tm.items()[0][1][0]:
                context['is_cross'] = cross_other_tm.items()[0][1][0]
        except IndexError:
            context['is_cross'] = False
        query = """
            SELECT DISTINCT
                lcmga.car_modification_id
            FROM catalog_linkgenericarticlepart lgap
            JOIN catalog_linkcarmodificationgenericarticle lcmga ON lgap.id = lcmga.link_generic_article_part_id
            WHERE lgap.part_id = %(part_id)s
            ORDER BY lcmga.car_modification_id
        """

        params = {'part_id': part.id}

        modifications = []
        with get_connection().cursor() as c:
            c.execute(query, params)
            modifications_list = c.fetchall()
            modifications = [i[0] for i in modifications_list]

        query = """
            SELECT DISTINCT
                parts.id, parts.slug, parts.catalog_number, tm.show_name
            FROM catalog_cross crosses
            JOIN catalog_part parts ON parts.id = crosses.cross_id
            JOIN catalog_trademark tm ON tm.id = parts.tm_id
            WHERE crosses.part_id = %(part_id)s
            ORDER BY tm.show_name
        """

        params = {'part_id': part.id}

        crosses = []
        with get_connection('api').cursor() as c:
            c.execute(query, params)
            crosses = c.fetchall()

        part_criteria = []
        with get_connection().cursor() as cur:
            query, params = prepare_query('catalog_criteria',
                                          modification_filter=modification_id,
                                          tree_filter=tree_id,
                                          part_filter=[part.id])
            cur.execute(query, params)
            part_criteria = cur.fetchone()

        criteria_part = {}
        criteria_mod = {}
        try:
            for i in part_criteria[1]:
                if criteria_part.get(i['f1'], None):
                    criteria_part[i['f1']].append(i['f2'])
                else:
                    criteria_part[i['f1']] = [i['f2'], ]
        except (TypeError, IndexError):
            pass

        try:
            for ii in part_criteria[2]:
                if criteria_mod.get(ii['f1'], None):
                    criteria_mod[ii['f1']].append(ii['f2'])
                else:
                    criteria_mod[ii['f1']] = [ii['f2'], ]
        except (TypeError, IndexError):
            pass

        criteria_part_string = ''
        for k, i in criteria_part.items():
            if i:
                try:
                    criteria_part_string += '%s: <span style="font-weight: bold;">%s</span><br />' % (k, ', '.join(i))
                except TypeError:
                    pass
            else:
                criteria_part_string += '<span style="font-weight: bold;">%s</span><br />' % k
        criteria_mod_string = ''
        for k, i in criteria_mod.items():
            if i:
                try:
                    criteria_mod_string += '%s: <span style="font-weight: bold;">%s</span><br />' % (k, ', '.join(i))
                except TypeError:
                    pass
            else:
                criteria_mod_string += '<span style="font-weight: bold;">%s</span><br />' % k

        context['crosses'] = crosses
        context['criteria_part_string'] = criteria_part_string
        context['criteria_mod_string'] = criteria_mod_string

        car_brands = []
        car_models = []
        car_modifications = []
        if modifications:
            car_modifications = CarModification.objects.select_related('car_model', 'body', 'engine').filter(
                id__in=modifications).distinct()
            car_models = CarModel.objects.select_related('car_brand').filter(
                carmodification__in=car_modifications).distinct()
            country = Settings.objects.filter(code_name='default_tec_doc_country').first()
            if country.object_id == 251:
                # cause we live in Europe and this shit can take much time in otherway
                car_models = car_models.extra(
                    select={'title': '''
                            SELECT title
                            FROM catalog_carmodelcountry cmc
                            WHERE cmc.car_model_id=catalog_carmodel.id
                                AND country_id=250
                        '''})
            car_brands = CarBrand.objects.filter(
                carmodel__in=car_models).distinct()

        context['car_brands'] = car_brands
        context['car_models'] = car_models
        context['car_modifications'] = car_modifications
        context['carmodelcountry'] = carmodelcountry

        context['h1_save'] = '%s %s %s' % (part.get_description(), part.tm.show_name, part.catalog_number)
        if not self.request.seo_data or not self.request.seo_data.seo_text:
            h1 = context['h1_save']
            text = generate_text_from_template(template_id=random.randint(16, 17),
                                               h1=h1)
            seo = save_seo_data(request=self.request, text=text, title=h1)
            context['seo_data'] = seo

        return context

    def get_prices(self, part):
        data = []
        basic_currency_id = get_default_currency().id

        role = self.request.role
        dc_id, so_id, client_id, supplier_id, shortcut = get_roles_ids(role)
        if isinstance(dc_id, int):
            dc_id = (dc_id, )
        currency_rates = get_currency_rates(dc_id, so_id, client_id,
                           supplier_id, basic_currency_id)

        sales_price_list = get_sales_price(dc_id, so_id, client_id,
                                           supplier_id, shortcut)

        user_currency_id = getattr(self.request.currency, 'id', basic_currency_id)
        sorting = ('price', 'asc')

        api_response = api_search(
            part.search_number,
            basic_currency_id,
            user_currency_id,
            json.dumps(currency_rates),
            json.dumps(sales_price_list),
            False,
            json.dumps(sorting),
            shortcut,
            '{}',
            so_id,
            client_id,
            False,
            part.id
        )
        try:
            part_mapping = api_response['parts']
        except KeyError:
            part_mapping = []

        matched = OrderedDict()
        cross_same_tm = OrderedDict()
        cross_other_tm = OrderedDict()
        for data_mapping in part_mapping:

            matched = OrderedDict()
            cross_same_tm = OrderedDict()
            cross_other_tm = OrderedDict()

            offers = 0
            for row in data_mapping:
                if not isinstance(row, int):
                    if row[12] == 0:
                        target = matched
                    elif row[12] == 1:
                        target = cross_same_tm
                    else:
                        target = cross_other_tm

                    key = (row[2], row[3], row[4], row[19])
                    target_row = target.setdefault(key, [])

                    if row[5] is None:
                        continue

                    target_row.append(row)
                    offers += 1

        return matched, cross_other_tm, cross_same_tm


# TODO: this method not using and should be deleted
class OneClickOrder(View):
    def post(self, request, *args, **kwargs):
        part_id = request.POST.get('part_id', None)
        phone = request.POST.get('phone', None)
        price = request.POST.get('price', None)
        delivery = request.POST.get('delivery', None)
        spl_id = request.POST.get('spl_id', None)

        part = None
        if part_id:
            part = Part.objects.filter(id=part_id).first()

        spl = None
        if spl_id:
            spl = SalesPriceList.objects.filter(id=spl_id).first()

        if not phone or not part:
            return JsonResponse({'status': "error",
                                 'message': u"Некорректные данные"})

        manager = None
        role = None
        if request.user is not None and request.role is not None:
            role = request.role
            if request.role.shortcut == 'client':
                manager = request.role.current_manager
            else:
                assigned_staff = request.role.assigned_staff.filter().last()
                if assigned_staff:
                    manager = assigned_staff.staff
        else:
            sfa = Settings.objects.filter(code_name='so_for_anonymous').first()
            if sfa and sfa.get_value():
                so = sfa.get_value()
            else:
                so = DC.get_default().created_so.first()

            manager = so.my_staff.filter(can_be_assigned=True).first()

        if manager:
            send_templated_email(
                "Automobi.ua: Заказ в один клик",
                'catalog/mail/one_click_order.html',
                {'phone': phone, 'part': part, 'spl': spl,
                 'price': price, 'delivery': delivery,
                 'role': role},
                manager.user.email)
            return JsonResponse({'status': 'success',
                                 'message': "Ваш запрос отправлен.<br>" +
                                 "Наш менеджер свяжется с Вами в ближайшее время"})

        else:
            return JsonResponse({'status': "error",
                                 'message': u"Не возможно выбрать менеджера"})


class CallBackRequest(AjaxFormMixin, FormView):
    form_class = CallBackForm
    template_name = 'catalog/call_back_modal_form.html'

    def get_initial(self):
        initial = super(CallBackRequest, self).get_initial()
        initial['price'] = self.request.GET.get('price', None)
        initial['part_id'] = self.request.GET.get('part_id', None)
        initial['spl_id'] = self.request.GET.get('spl_id', None)
        initial['delivery'] = self.request.GET.get('delivery', None)
        initial['mod_id'] = self.request.GET.get('mod_id', None)
        if not self.request.user.is_anonymous():
            initial['name'] = self.request.role.full_name
            initial['phone'] = self.request.role.telephone_number

        return initial

    def form_valid(self, form):
        name = form.cleaned_data.get('name')
        phone = form.cleaned_data.get('phone')

        part_id = form.cleaned_data.get('part_id', None)
        price = form.cleaned_data.get('price', None)
        delivery = form.cleaned_data.get('delivery', None)
        spl_id = form.cleaned_data.get('spl_id', None)
        mod_id = form.cleaned_data.get('mod_id', None)

        part = None
        if part_id:
            part = Part.objects.filter(id=part_id).first()

        spl = None
        if spl_id:
            spl = SalesPriceList.objects.filter(id=spl_id).first()

        modification = None
        if mod_id:
            modification = CarModification.objects.filter(id=mod_id).first()

        request = self.request
        manager = None
        if request.user is not None and request.role is not None:
            if request.role.shortcut == 'client':
                manager = request.role.current_manager
            else:
                assigned_staff = request.role.assigned_staff.filter().last()
                if assigned_staff:
                    manager = assigned_staff.staff
        else:
            sfa = Settings.objects.filter(code_name='so_for_anonymous').first()
            if sfa and sfa.get_value():
                so = sfa.get_value()
            else:
                so = DC.get_default().created_so.first()

            manager = so.my_staff.filter(can_be_assigned=True).first()

        if manager:
            send_templated_email(
                "Automobi.ua: Заказ обратного звонка",
                'catalog/mail/call_back_request.html',
                {'phone': phone, 'name': name, 'part': part, 'spl': spl,
                 'price': price, 'delivery': delivery,
                 'modification': modification},
                manager.user.email)
            return JsonResponse({'status': 'success',
                                 'message': "Ваш запрос отправлен.<br> " +
                                 "Наш менеджер свяжется с Вами в ближайшее время"})
        else:
            return JsonResponse({'status': "error",
                                 'message': u"Не возможно выбрать менеджера"})


class GetBestOffer(View):
    def post(self, request, *args, **kwargs):
        per_each_first = 4
        per_each_other = 2

        items = []
        last_pli_id = request.POST.get('last_pli_id', None)
        prev = request.POST.get('prev', None)

        settings_object = Settings.objects.get(code_name='best_offer_spl')
        spl = SalesPriceList.objects.get(id=settings_object.object_id)
        if not spl:
            return JsonResponse({'status': "error"})
        pli_qs = spl.get_items()

        index = per_each_first
        if last_pli_id is None:
            pli_qs_sliced = pli_qs[:per_each_first] if pli_qs else []
        else:
            if last_pli_id == u'':
                last_pli_id = 0
            else:
                last_pli_id = int(last_pli_id)

            if last_pli_id >= (len(pli_qs)-per_each_other):
                return JsonResponse({'status': "error"})

            if prev:
                index = (len(pli_qs)-per_each_other) if last_pli_id == 0 else (last_pli_id-per_each_other)

            else:
                index = 0 if last_pli_id >= (len(pli_qs)-per_each_other) else (last_pli_id+per_each_other)

            index_from = index
            index_to = index + per_each_other
            pli_qs_sliced = pli_qs[index_from:index_to] if pli_qs else []
            print pli_qs_sliced[0].id

        for pli in pli_qs_sliced:
            part = pli.part
            part_img = PartImage.objects.filter(part_id=part.id).first()
            price = GetPrice(pli, spl, request.role).process()

            sfa = Settings.objects.filter(code_name='so_for_anonymous').first()
            if sfa and sfa.get_value():
                so = sfa.get_value()
            else:
                dc = DC.get_default()
                so = dc.created_so.first()
            price_old = GetPrice(pli, spl, so, get_purchase=False).process()

            obj = {
                'spl': spl,
                'pli': pli,
                'part': part,
                'part_img': part_img,
                'price': price,
                'price_old': price_old,
                'index': index,
            }
            items.append(obj)

        if items:
            content = render_to_string('catalog/best_offers.html',
                                       {'items': items})
            return JsonResponse({'status': "success", 'content': content})
        else:
            return JsonResponse({'status': "error"})


class VinCodeHelp(TemplateView):
    template_name = 'catalog/vin_code_help.html'


class CarSearchResponseMixin(object):
    ajax_template = ''

    def get_response(self, data):

        if not self.ajax_template:
            return JsonResponse({'error': 'Template not choosen'})

        modal_content = render_to_string(self.ajax_template, data)
        return JsonResponse({'modal_content': modal_content})


class ProductionYearMixin(object):
    start = datetime.date(1980, 1, 1)
    end = datetime.date(2016, 12, 31)

    def get_production_years(self, car_brand=None):
        production_years = OrderedDict()
        brand_url = ''
        if car_brand and not isinstance(car_brand, CarBrand) and car_brand.isdigit():
            car_brand = CarBrand.objects.get(id=car_brand)

        start = car_brand.start_date if getattr(car_brand, 'start_date', None)\
            else self.start
        if start < self.start:
            start = self.start
            brand_url = reverse('catalog:models',
                                kwargs={'car_brand_slug': car_brand.slug})

        end_year = car_brand.end_date if getattr(car_brand, 'end_date', None)\
            else self.end

        for year in range(start.year, end_year.year + 1):
            key = str(year)[:3]
            production_years.setdefault(key, []).append(year)

        items = production_years.items()
        items.reverse()

        return OrderedDict(items), brand_url


class CountryInfoPopup(View):
    ajax_template = 'catalog/country_info_popup.html'

    def get_response(self, data):

        if not self.ajax_template:
            return JsonResponse({'error': 'Template not choosen'})

        modal_content = render_to_string(self.ajax_template, data)
        return JsonResponse({'modal_content': modal_content})

    def post(self, request, *args, **kwargs):
        modification = None
        mod_id = request.POST.get('mod_id', None)
        if mod_id:
            modification = CarModification.objects.get(id=mod_id)

        return self.get_response({'modification': modification})


class SaveParamSession(View):
    def post(self, request, *args, **kwargs):
        key = request.POST.get('param_name', None)
        if key:
            request.session[key] = request.POST.get(key, None)
        elif request.POST.get('params', None):
            for param in json.loads(request.POST.get('params', None)):
                request.session[param['param_name']] = param[param['param_name']]
        return JsonResponse({'success': 'Save param in session!'})


class GetCarSearchPopup(AjaxFormMixin, CarSearchResponseMixin,
                        ProductionYearMixin, View):
    ajax_template = 'catalog/car_search_modal.html'

    def get_car_brands(self):
        alphabet = []
        grouped_brands = {}
        query = Q(is_passenger=True) | Q(is_commercial=True)
        query &= Q(is_shown=True) & Q(to_main=True)
        brands_passenger_list = list(CarBrand.objects.filter(query)
                                     .order_by('title').all())
        for brand in brands_passenger_list:
            first_letter = brand.title[:1]
            if first_letter not in alphabet:
                alphabet.append(first_letter)
            if first_letter not in grouped_brands:
                grouped_brands[first_letter] = []
            grouped_brands[first_letter].append(brand)

        return alphabet, grouped_brands

    def get(self, request, *args, **kwargs):
        alphabet, grouped_brands = self.get_car_brands()
        car_brand = self.kwargs.get('car_brand', None)
        if car_brand:
            car_brand = CarBrand.objects.get(id=int(car_brand))

        production_years, brand_url = self.get_production_years(
            car_brand=car_brand)
        data = {
            'grouped_brands': grouped_brands,
            'car_brand': car_brand,
            'production_years': production_years,
            'brand_url': brand_url,
        }
        return self.get_response(data)


class CarSearchPopup(AjaxFormMixin, CarSearchPopupMixin,
                     ProductionYearMixin, View):
    def initial_data(self, *args, **kwargs):
        post_data = self.request.POST
        self.body = post_data.get('car-body', '')
        self.engine = post_data.get('car-engine', '')
        self.years = post_data.get('car-years', '')
        self.brands = post_data.get('car-brands', '')
        self.models = post_data.get('car-models', '')
        self.step_name = post_data.get('step-name', '')

    def post(self, request, *args, **kwargs):
        self.initial_data()
        options = ''
        if self.step_name == 'years' and self.brands:
            self.step_name = 'brands'
        queryset = getattr(self, self.STEP_MAP[self.step_name])()
        property_dict = self.STEP_PROPERTY_MAP[self.step_name]
        order_field = 'show_name' if self.step_name == 'engine'\
            else property_dict['data_disp']
        queryset = queryset.order_by(order_field)
        index = 0
        response_data = {}

        total_qs_rows = queryset.count()
        if self.step_name == 'engine':
            columns = total_qs_rows / 11 if total_qs_rows > 30 else 2
        else:
            columns = 4

        per_columns = (total_qs_rows / columns) + 1
        if per_columns < 2:
            per_columns = 4
        for index, item in enumerate(queryset):
            params = {
                'data_val': getattr(item, property_dict['data_val']),
                'data_disp': getattr(item, property_dict['data_disp']),
                'data_title': getattr(item, property_dict['data_title']),
                'step': self.STEPS_LIST[self.STEPS_LIST.index(self.step_name) + 1]
            }
            if self.step_name == 'models':
                data_title = '<img src="' + settings.STATIC_URL + 'images/'
                data_title += str(params['data_val']) + '.png">'
                params['data_title'] = data_title + \
                    '<span>' + params['data_title'] + '</span>'
            if index % per_columns == 0:
                options += self.divider_options
            options += self.option % params
            response_data = {'options': options, 'index': index}

            if self.step_name == 'brands' and self.brands:
                production_years, brand_url = self.get_production_years(
                    car_brand=self.brands)
                response_data['years'] = render_to_string(
                    'catalog/car_search_modal_years.html',
                    {'production_years': production_years,
                     'brand_url': brand_url})

        return JsonResponse(response_data)


class CarSearchModification(AjaxFormMixin, CarSearchResponseMixin, View):
    ajax_template = 'catalog/selected_car_modification.html'

    def post(self, request, *args, **kwargs):
        max_age = 3600 * 24 * 365  # one year
        modificaton = request.POST.get('car-modificaton', '')
        actual_car_model = self.request.session.get('catalog_car_model', '')
        if modificaton:
            modificaton = CarModification.objects.get(id=modificaton)
        data = {
            'mod': modificaton,
            'actual_car_model': actual_car_model,
            'MEDIA_URL': settings.MEDIA_URL
        }
        response = self.get_response(data)
        response.set_cookie('car_mod', str(modificaton.id), max_age=max_age)
        return response


class CarSearchRemove(GetCarSearchPopup):
    ajax_template = 'catalog/unoriginal_main_catalog.html'

    def get(self, request, *args, **kwargs):
        alphabet, grouped_brands = self.get_car_brands()
        data = {
            'grouped_brands': grouped_brands,
            'alphabet': alphabet,
        }
        response = self.get_response(data)
        response.delete_cookie('car_mod')
        return response


class CarSearchSave(AjaxFormMixin, View):
    def get(self, request, *args, **kwargs):
        if request.user.is_anonymous():
            return JsonResponse({'error': "Зарегистрируйтесь или авторизируйтесь!"})
        car_mod_id = request.COOKIES.get('car_mod', None)
        if not car_mod_id:
            return JsonResponse({'error': "Выберите вначале авто"})
        obj = CarModification.objects.get(id=car_mod_id)
        data = {
            'client': request.role,
            'date_production': obj.startdate_production,
            'car_model': obj.car_model,
            'car_modification': obj,
            'engine_volume': obj.engine_volume,
            'hp_engine_power': obj.hp_engine_power,
            'body': obj.body,
            'engine': obj.engine,
            'drive': obj.drive,
            'transmission': obj.transmission
        }
        client_car_qs = ClientCar.objects.filter(car_modification=obj)

        if client_car_qs:
            return JsonResponse({'error': "У вас уже есть авто с такой модификацией"})

        client_car, created = ClientCar.objects.get_or_create(**data)
        return JsonResponse({'success': u"Авто успешно добавлено в гараж"})
