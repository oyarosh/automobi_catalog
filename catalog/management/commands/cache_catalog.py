from django.core.management.base import BaseCommand, CommandError
from price_generator.tasks import refresh_all_brands_parts

class Command(BaseCommand):
    help = 'Caches parts and trademarks in catalog'

    def handle(self, *args, **options):
        refresh_all_brands_parts()