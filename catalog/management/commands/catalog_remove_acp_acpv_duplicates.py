from django.core.management.base import BaseCommand
from catalog.models import ACProperty, ACPropertyValue


class Command(BaseCommand):
    help = 'Remove ACProperty and ACPropertyValue duplicates'

    def handle(self, *args, **options):
        for unique in ACProperty.objects.distinct('title'):
            for non_unique in ACProperty.objects.filter(title=unique.title).exclude(id=unique.id):
                non_unique.acpropertyvalue_set.all().update(ac_property=unique)

                non_unique_acs = non_unique.acs.all()
                unique.acs.add(*non_unique_acs)

                non_unique.delete()

        for unique_value in ACPropertyValue.objects.distinct('title', 'ac_property'):
            for i, non_unique_value in enumerate(ACPropertyValue.objects \
                    .filter(title=unique_value.title, ac_property=unique_value.ac_property) \
                    .exclude(id=unique_value.id)):
                non_unique_value_parts_all = non_unique_value.parts.all()
                unique_value.parts.add(*non_unique_value_parts_all)

                non_unique_value.delete()