from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Caches parts and trademarks in catalog from logged brands'

    def handle(self, *args, **options):
        from price_generator.tasks import refresh_brands_parts
        refresh_brands_parts()