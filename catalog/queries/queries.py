catalog_criteria = """
    SELECT
        part_id,
        json_agg(DISTINCT ROW(criteria_description, cr_value)) AS criteria_part,
        json_agg(DISTINCT ROW(mod_s_description, mod_s_value)) AS criteria_mod
    FROM (
        SELECT DISTINCT
                part_id,
                criteria.description AS criteria_description,
                cr.value AS cr_value,
                criteria_mod_s.description AS mod_s_description,
                cr_mod.value AS mod_s_value
            FROM catalog_linkcarmodificationgenericarticle lcmga
            JOIN catalog_linkgenericarticlepart lgap
                ON lgap.id = lcmga.link_generic_article_part_id
            {tree_join_query}
            LEFT JOIN catalog_partcriteria cr
                ON cr.catalog_part_id = lgap.part_id
            LEFT JOIN catalog_partcriteriala cr_mod
                ON lgap.id = cr_mod.link_generic_article_part_id
                AND lcmga.link_generic_article_part_id = cr_mod.link_generic_article_part_id
                AND lcmga.generic_article_id = lgap.generic_article_id
            LEFT JOIN catalog_criteria criteria_mod_s
                ON cr_mod.criteria_id = criteria_mod_s.id
            LEFT JOIN catalog_criteria criteria
                ON cr.criteria_id = criteria.id
            WHERE
                1=1
                {modification_filter_query}
                {tree_filter_query}
                {part_filter_query}
    ) as query
    GROUP BY part_id
"""

popular_parts = """
    SELECT DISTINCT ON (query.part_id)
        query.id,
        query.part_id,
        part.catalog_number,
        part.description,
        part.slug,
        tm.show_name,
        pi.image
        FROM (
            SELECT pli.id, pli.part_id
            FROM ppl_pricelistitem pli
            JOIN ppl_pricelist ppl
                            ON ppl.id = pli.price_list_id
            JOIN spl_salespricelistitem spli
                            ON spli.primary_ppl_id = ppl.id
            JOIN spl_salespricelist spl
                            ON spl.id = spli.sales_price_list_id

            WHERE spl.id = ANY(%(spl_ids)s)
                    AND pli.active = TRUE
                    AND random() < 0.01
            LIMIT 10
        ) as query
        JOIN catalog_part part
            ON part.id = query.part_id
        JOIN catalog_trademark tm
            ON part.tm_id = tm.id
        LEFT JOIN catalog_partimage pi
            ON pi.part_id = query.part_id
"""
