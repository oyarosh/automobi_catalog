from .queries import *
from .full_text_queries import *

__all__ = ['prepare_query']

QUERY_MAP = {
    'catalog_criteria': catalog_criteria,
    'popular_parts': popular_parts,
    'part_full_text': part_full_text,
}


def prepare_query(query_name, **kwargs):
    modification_filter_query = ''
    if kwargs.get('modification_filter', None):
        modification_filter_query = ' AND lcmga.car_modification_id = %(modification_id)s'
        kwargs['modification_id'] = kwargs['modification_filter']
    tree_filter_query = ''
    tree_join_query = ''
    if kwargs.get('tree_filter', None):
        tree_filter_query = ' AND stga.tree_id = %(tree_id)s'
        tree_join_query = '''LEFT JOIN catalog_searchtreegenericarticle stga
                             ON stga.generic_article_id = lcmga.generic_article_id'''
        kwargs['tree_id'] = kwargs['tree_filter']
    part_filter_query = ''
    if kwargs.get('part_filter', None):
        part_filter_query = ' AND part_id = ANY(%(part_ids)s)'
        kwargs['part_ids'] = kwargs['part_filter']

    search_field = 'description'
    if kwargs.get('search_field', None):
        search_field = kwargs['search_field']

    tgrm_conds = ''
    inner_tgrm = ''
    if kwargs.get('tgrm_conditions', []):
        cond_tpl_frst = " WHERE %(search_field)s ILIKE '%%%(cond)s%%'"
        cond_tpl = " AND %(search_field)s ILIKE '%%%(cond)s%%'"
        frst = True
        for cond in kwargs['tgrm_conditions']:
            if frst:
                tgrm_conds += cond_tpl_frst % {'cond': cond,
                                               'search_field': search_field}
                frst = False
                continue
            tgrm_conds += cond_tpl % {'cond': cond,
                                      'search_field': search_field}

        inner_tgrm = " INNER JOIN trgm_part t_part ON t_part.id = part.id"

    tsquery_conds = ''
    cond_tpl_tsquery = " WHERE %(search_field)s_tsv @@ to_tsquery('russian', '%(conds)s')"
    if kwargs.get('tsquery_conditions', []):
        conds_list = map(lambda x: x + ':*', kwargs['tsquery_conditions'])
        conds = '&'.join(conds_list)
        tsquery_conds = cond_tpl_tsquery % {'conds': conds,
                                            'search_field': search_field}

    ac_part_filter = ''
    if kwargs.get('ac_part_filter_enable', True):
        ac_part_filter = 'INNER JOIN catalog_ac_parts acparts ON acparts.part_id = part.id'

    order_by_ac_price = ''
    if ac_part_filter:
        order_by_ac_price = order_by_ac_price_query

    union_ts_vector = ''
    if tgrm_conds and ac_part_filter:
        union_ts_vector = union_ts_vector_query
        union_ts_vector += ' '
        conds_list = map(lambda x: x + ':*', kwargs['tgrm_conditions'])
        conds = '&'.join(conds_list)
        union_ts_vector += cond_tpl_tsquery % {'conds': conds,
                                               'search_field': search_field}
        if tsquery_conds:
            union_ts_vector += tsquery_conds.replace('WHERE', 'AND')
        if kwargs.get('ac_categories', []):
            union_ts_vector += ' AND acparts.ac_id IN %(ac_ids)s' \
                % {'ac_ids': tuple(kwargs.get('ac_categories', []))}

    ac_categories_query = ''
    if ac_part_filter and kwargs.get('ac_categories', []):
        if tsquery_conds:
            ac_categories_query_tpl = ' AND'
        else:
            ac_categories_query_tpl = ' WHERE'
        ac_categories_query_tpl += ' acparts.ac_id IN %(ac_ids)s'
        ac_categories_query = ac_categories_query_tpl % {'ac_ids': tuple(kwargs.get('ac_categories', []))}

    sales_price_list_query = ''
    if ac_part_filter and kwargs.get('sales_price_list_ids', []):
        if tsquery_conds or ac_categories_query:
            sales_price_list_tpl = ' AND'
        else:
            sales_price_list_tpl = ' WHERE'
        sales_price_list_tpl += ' ci.sale_price_list IN %(spl_ids)s AND ci.active=True'
        sales_price_list_query = \
            sales_price_list_tpl % {'spl_ids': tuple(kwargs.get('sales_price_list_ids', []))}
        if union_ts_vector:
            union_ts_vector += sales_price_list_query.replace('WHERE', 'AND')

    query = QUERY_MAP[query_name].format(
        part_filter_query=part_filter_query,
        tree_filter_query=tree_filter_query,
        modification_filter_query=modification_filter_query,
        tree_join_query=tree_join_query,
        tgrm_where_conditions=tgrm_conds,
        tsquery_where_conditions=tsquery_conds,
        inner_tgrm=inner_tgrm,
        ac_part_filter=ac_part_filter,
        ac_categories_query=ac_categories_query,
        sales_price_list_query=sales_price_list_query,
        order_by_ac_price=order_by_ac_price,
        union_ts_vector=union_ts_vector
    )
    return query, kwargs
