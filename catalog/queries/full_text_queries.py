part_full_text = """
    SELECT * FROM(
        WITH trgm_part AS (
            SELECT id
            FROM catalog_part
            {tgrm_where_conditions}
        )

        SELECT
            part.id as id,
            ci.id as ci_id
        FROM catalog_part part
        LEFT JOIN ppl_catalogitem ci ON ci.part_id = part.id
        {ac_part_filter}
        {inner_tgrm}
        {tsquery_where_conditions}
        {ac_categories_query}
        {sales_price_list_query}

        {union_ts_vector}

    ) as ft_query
    {order_by_ac_price}
    LIMIT 100;
"""

# helpers start
union_ts_vector_query = """
    UNION

    SELECT
        part.id as id,
        ci.id as ci_id
    FROM catalog_part part
    INNER JOIN catalog_ac_parts acparts ON acparts.part_id = part.id
    LEFT JOIN ppl_catalogitem ci ON ci.part_id = part.id
"""

order_by_ac_price_query = """
    ORDER BY
    CASE
        WHEN ci_id IS NOT NULL THEN
            0
    END NULLS LAST
"""
# helpers end
