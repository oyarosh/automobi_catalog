# -*- coding: utf-8 -*-
import re

from django import template
from django.http import Http404
from django.core.urlresolvers import reverse

from cacheops import cache, CacheMiss

from catalog.models import GoodsCategoryGroup, CarBrand, AC, \
    VinHistory, CarModel, ACPropertyValue, CarModification, \
    CarModelCountry, TradeMark, CarTree
from catalog.utils import get_multiply_four
from superuser.models import Settings
from laximo import OEM
from django.utils.http import urlencode as django_urlencode
from db_share.utils import get_connection


register = template.Library()


@register.inclusion_tag('catalog/templatetags/output_tree.html',
                        takes_context=True)
def output_tree(context, tree, modification_id):
    data = {
        'tree': tree,
        'modification_id': modification_id,
    }
    if tree:
        return data
    else:
        return None


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)


@register.filter
def prefetch_related(objects):
    return objects.prefetch_related('goodscategory_set')


@register.inclusion_tag('catalog/templatetags/goods_categories_groups.html',
                        takes_context=True)
def goods_categories_groups(context):
    groups = GoodsCategoryGroup.objects.order_by('pk').all()

    data = {
        'groups': groups,
    }
    return data


@register.inclusion_tag('catalog/templatetags/ac_menu.html',
                        takes_context=True)
def ac_menu(context):
    return {
            'acs': AC.objects.filter(level=0),
            'catalog': CarTree.objects.filter(
                id__gt=10001, tree_id=3, id__lt=19999,
                show_on_menu=True).order_by('lft').cache(),
            'request': context['request'],
           }


@register.filter
def ac_get_level_1(ac):
    return ac.get_descendants().filter(level=ac.level + 1)


@register.filter
def get_annotated_acpv(ac_property_value, ac):
    if ac:
        ac_property_value = ac_property_value.filter(parts__ac__in=ac.get_descendants(include_self=True)).distinct()
    return ACPropertyValue.objects.filter(id__in=ac_property_value)


@register.filter
def get_extraselect_acpv(qs, req_data):
    id_list = req_data.get('ac_val', [])
    id_str = ', '.join(id_list)
    return qs.extra(
        select={
            'parts_count': """
                SELECT COUNT(ac_prop_parts.part_id)
                FROM catalog_acpropertyvalue_parts ac_prop_parts
                WHERE ac_prop_parts.acpropertyvalue_id = catalog_acpropertyvalue.id%s
            """ % (""" AND ac_prop_parts.part_id IN (
                SELECT ac_prop_parts2.part_id
                FROM catalog_acpropertyvalue_parts ac_prop_parts2
                WHERE ac_prop_parts2.acpropertyvalue_id IN (%s))""" % id_str if id_list else "")
        },
    )


@register.inclusion_tag('catalog/car_search_form.html', takes_context=True)
def car_search_form(context):
    request = context['request']
    cache_key = 'oem_catalog'
    try:
        brands = cache.get(cache_key)
    except CacheMiss:
        brands = OEM().request('ListCatalogs', **{'ssd': u''})
        cache.set(cache_key, brands, timeout=3600*24*7)

    return {'brands': brands, 'request': request}


@register.filter
def is_dict(item):
    return isinstance(item, dict)


@register.simple_tag
def get_row_attr(row, attr_name, param='source', no_spaces=''):
    if isinstance(row, basestring):
        return ''

    if attr_name in row:
        if attr_name == '@imageurl':
            return row[attr_name].replace('%size%', param)
        if not no_spaces:
            try:
                return row[attr_name].replace('\n', ' ')
            except TypeError:
                return attr_name.replace('\n', ' ')
        return "".join(row[attr_name].replace('&', ' ').split())
    return ''


ATTRS_NAMES = {
    'country': u"Страна",
    'framecolor': u"Цвет кузова",
    'manufactured': u"Произведено",
    'market': u"Рынок",
    'model': u"Модель",
    'modification': u"Модификация",
    'modelyearfrom': u"Начало производства",
    'modelyearto': u"Конец производства",
    'transmission': u"Трансмиссия",
    'trimcolor': u"Цвет отделки",
    'destinationregion': u"Регион",
    'train': u"Привод",
}


@register.simple_tag
def get_all_attrs(result):
    rows = []
    print result
    for attr_name in result:
        attr = attr_name.replace('@', '')
        if attr not in ATTRS_NAMES:
            continue
        rows.append(u"%s: <span style='font-weight: bold;'>%s</span>" % (
            ATTRS_NAMES.get(attr, attr),
            result[attr_name].replace('&', ' '))
        )

    return "; ".join(rows)


@register.simple_tag
def get_row_attr_model(row, attr_name, param):
    for attr in row['attribute']:
        if attr['@key'] == attr_name:
            return attr[param]
    return ''


@register.simple_tag
def get_vin_ssd(row, request_vin=''):
    if '@ssd' in row:
        ssd = row['@ssd']
        reg_rezult_tilda = re.findall('vin@(.*?)~', ssd)
        if reg_rezult_tilda:
            return reg_rezult_tilda[0]
        else:
            reg_rezult_endstr = re.findall('vin@(.*?)$', ssd)
            if reg_rezult_endstr:
                return reg_rezult_endstr[0]
    return request_vin


@register.simple_tag
def get_first_row_attr_list(dictionary, attr_name):
    if dictionary:
        row = dictionary[0]
        if attr_name in row:
            return row[attr_name]
    return ''


@register.simple_tag
def get_image_points(row, imagesize):
    width, height = imagesize
    left = float("{0:.2f}".format(float(int(row['@x1']) * 100) / width))
    right = float(
        "{0:.2f}".format(float((width - int(row['@x2'])) * 100) / width))
    top = float("{0:.2f}".format(float(int(row['@y1']) * 100) / height))
    bottom = float(
        "{0:.2f}".format(float((height - int(row['@y2'])) * 100) / height))
    pos = 'left:%s%%; right:%s%%; \
        top:%s%%; bottom:%s%%;' % (left, right, top, bottom)
    return pos


@register.simple_tag
def get_hint_points(row, imagesize):
    width, height = imagesize
    left = float("{0:.2f}".format(float(int(row['@x1']) * 100) / width))
    top = float("{0:.2f}".format(float(int(row['@y1']) * 100) / height))

    pos = ('left:%s%%;' % left) if left <= 50 else ('right:%s%%;' % (96 - left))
    if top <= 80:
        pos += ' top:%s%%;' % (top + 4)
    else:
        pos += ' bottom:%s%%;' % (101 - top)
    return pos


@register.simple_tag
def get_arrow_class(row, imagesize):
    width, height = imagesize
    left = float("{0:.2f}".format(float(int(row['@x1']) * 100) / width))
    top = float("{0:.2f}".format(float(int(row['@y1']) * 100) / height))

    cls = ""
    if left > 50:
        cls = "right_arrow"
    if top > 80:
        cls += " bottom_arrow"
    return cls


@register.filter
def name_boolean_link(item):
    b_name = item.get('@name') or None
    if b_name:
        return True
    return False


@register.filter
def oem_boolean_link(item):
    b_link = item.get('@link') or None
    if b_link and re.match(b_link, u'true', re.IGNORECASE):
        return True
    return False


@register.filter
def is_get_descendant(item):
    desc_row = item.get('row') or None
    if desc_row:
        return True
    return False


@register.filter
def childrens_boolean(item):
    b_link = item.get('@childrens') or None
    if b_link and re.match(b_link, u'true', re.IGNORECASE):
        return True
    return False


@register.filter
def wizard2(item):
    b_link = item.get('@supportparameteridentification2') or None
    if b_link and re.match(b_link, u'true', re.IGNORECASE):
        return True
    return False


@register.filter
def wizard(item):
    b_link = item.get('@supportparameteridentification') or None
    if b_link and re.match(b_link, u'true', re.IGNORECASE):
        return True
    return False


@register.filter
def determined(item):
    b_link = item.get('@determined') or None
    if b_link and re.match(b_link, u'true', re.IGNORECASE):
        return True
    return False


@register.filter
def only_vin(item):
    pattern = 'vin@(\w+)'
    if not item:
        raise Http404
    if 'v_vin@' in item:
        vin = re.findall(pattern, item)
        if vin:
            return vin[0]
    return False


@register.filter
def get_car_brands(modification_ids):
    return CarBrand.objects.filter(
        carmodel__carmodification__id__in=modification_ids).distinct()


@register.filter
def get_car_models(modification_ids):
    return CarModel.objects.filter(
        carmodification__id__in=modification_ids).distinct()


@register.filter
def get_car_modifications(modification_ids):
    return CarModification.objects.filter(
        id__in=modification_ids).distinct()


@register.filter
def filter_by_brand(carmodels, brand):
    return carmodels.filter(car_brand=brand)


@register.filter
def has_descendants(ac):
    return ac.get_descendant_count()


@register.simple_tag(takes_context=True)
def get_query_string_for_ac_tree(context):
    obj = {}
    paginate_by = context['request'].GET.get('paginate_by')
    order_by = context['request'].GET.get('order_by')
    if paginate_by:
        obj['paginate_by'] = paginate_by
    if order_by:
        obj['order_by'] = order_by
    query_str = django_urlencode({'paginate_by': paginate_by,
                                  'order_by': order_by})
    return '?'+query_str if query_str else ''


@register.simple_tag(takes_context=True)
def get_vin_history(context):
    request = context['request']
    user = request.user

    if user.is_anonymous():
        id_list = request.session.get('vin_history', list())
        vin_history = VinHistory.objects.filter(
            id__in=id_list)
    else:
        vin_history = user.vinhistory_set

    vin_history = vin_history.order_by('-id')\
        .values_list('vin', flat=True).distinct()

    vin_history_list = vin_history[:int(10)]

    if vin_history_list:
        result_str = u""
        for item in vin_history_list:
            if item:
                result_str += u"<li>" + item + u"</li>"

    else:
        result_str = u'<li class="no-click">Вы еще ничего не искали...</li>'

    return result_str


@register.filter
def space_before_bracket(str_to_replace):
    return re.sub(r'(\w)\(', '\1 (', str_to_replace)


@register.filter
def for_countries(modification):
    countries = []
    if modification.ukraine:
        countries.append(u"Украина")
    if modification.europe:
        countries.append(u"Европа")
    if modification.all_world:
        countries.append(u"Весь мир")

    return u", ".join(countries)


@register.assignment_tag
def get_criterias(part_id, modification_id=None, tecdoc_id=None):
    criterias = []

    query = """
        SELECT
            COALESCE(criteria.description, criteria.short_description) as description,
            pc.value,
            criteria.unit_text
        FROM catalog_partcriteria pc
        LEFT JOIN catalog_criteria criteria ON criteria.id = pc.criteria_id
        WHERE pc.catalog_part_id = %(part_id)s
        {modification_query}
    """

    params = {'part_id': part_id}

    if modification_id and tecdoc_id:
        modification_query = """
            UNION
            SELECT
                COALESCE(criteria.description, criteria.short_description) as description,
                pc.value,
                criteria.unit_text
            FROM catalog_partcriteriala pc
            LEFT JOIN catalog_criteria criteria ON criteria.id = pc.criteria_id
            LEFT JOIN catalog_linkgenericarticlepart lgap ON lgap.id = pc.link_generic_article_part_id
            LEFT JOIN catalog_linkcarmodificationgenericarticle lcmga
                ON lcmga.link_generic_article_part_id = pc.link_generic_article_part_id
                    AND lcmga.generic_article_id = lgap.generic_article_id
            WHERE lgap.part_id = %(tecdoc_id)s
                AND lcmga.car_modification_id = %(modification_id)s
        """
        params['modification_id'] = modification_id
        params['tecdoc_id'] = tecdoc_id

        query = query.format(
            modification_query=modification_query,
        )

    criterias_result = None
    with get_connection().cursor() as c:
        c.execute(query, params)
        criterias_result = c.fetchall()

    for criteria in criterias_result:
        criteria_str = criteria[0] if criteria[0] else u""
        if criteria[1]:
            if criteria[0]:
                criteria_str += u": "
            criteria_str += "<div class='bold'>" + criteria[1] + "</div>"
        if criteria[2]:
            criteria_str += u" %s" % criteria[2]
        criterias.append(criteria_str)

    return u";<br> ".join(criterias) if criterias else u"-----"


@register.assignment_tag(takes_context=True)
def get_coockie_modification(context):
    request = context['request']
    mod = request.COOKIES.get('car_mod', None)
    if mod:
        mod = CarModification.objects.get(id=mod)
    return mod


@register.assignment_tag(takes_context=True)
def get_tree_is_leaf(context, nodes, level_root):
    for item in nodes:
        if item.is_leaf_node and item.level == level_root:
            return True
    return False


@register.assignment_tag(takes_context=True)
def plus_one(context, number):
    return number + 1


@register.assignment_tag(takes_context=True)
def get_ac_depth(context, ac):
    return ac.level + 2


@register.assignment_tag(takes_context=True)
def get_brand_filter_url(context, ac_url=None, part=None):
    url_pattern = '%sfilters-%s/'
    if not part or not ac_url:
        return False
    tm_name = part.tm.show_name
    prop = ACPropertyValue.objects.filter(title=tm_name).first()
    if not prop:
        return False
    filter_url = url_pattern % (ac_url, str(prop.id))
    filter_name = prop.title
    return (filter_url, filter_name)


@register.assignment_tag(takes_context=True)
def get_ac_bread_crumbs_dict(context, ac):
    ac_dict = {}
    for item in ac.get_ancestors(include_self=True):
        if item.level == 0:
            ac_dict[item.title] = reverse('catalog:automobi_catalog',
                                          kwargs={
                                              'first_slug': item.slug
                                          })
        elif item.level == 1:
            ac_dict[item.title] = reverse('catalog:automobi_catalog',
                                          kwargs={
                                              'first_slug': item.parent.slug,
                                              'second_slug': item.slug
                                          })
        elif item.level == 2:
            ac_dict[item.title] = reverse('catalog:automobi_catalog',
                                          kwargs={
                                              'first_slug': item.parent.parent.slug,
                                              'second_slug': item.parent.slug,
                                              'third_slug': item.slug,
                                          })
        elif item.level == 3:
            ac_dict[item.title] = reverse('catalog:automobi_catalog',
                                          kwargs={
                                              'first_slug': item.parent.parent.parent.slug,
                                              'second_slug': item.parent.parent.slug,
                                              'third_slug': item.parent.slug,
                                              'forth_slug': item.slug,
                                          })
        elif item.level == 4:
            ac_dict[item.title] = reverse('catalog:automobi_catalog',
                                          kwargs={
                                              'first_slug': item.parent.parent.parent.parent.slug,
                                              'second_slug': item.parent.parent.parent.slug,
                                              'third_slug': item.parent.parent.slug,
                                              'forth_slug': item.parent.slug,
                                              'fifth_slug': item.slug,
                                          })
    return ac_dict


@register.simple_tag(takes_context=True)
def get_image_position(context, image, part):
    position = ''
    for index, item in enumerate(part.partimage_set.all(), start=1):
        if item == image:
            position = str(index)
    return position


@register.assignment_tag
def is_four_equal(parts):
    if len(parts) % 4 == 0:
        return True
    return False


@register.assignment_tag
def get_general_title(mod):
    if mod:
        qs = CarModelCountry.objects.filter(car_model_id=mod.car_model.id)
        if qs:
            country = Settings.objects.filter(code_name='default_tec_doc_country').first()
            gtitle_qs = qs.filter(general_title__isnull=False, country_id=country.object_id)
            if gtitle_qs:
                return gtitle_qs.first().general_title.cartext
            else:
                title_qs = qs.filter(country_id=country.object_id)
                return title_qs.first().title
    return ''


@register.simple_tag(takes_context=True)
def get_tm_from_get_request(context):
    request = context['request']
    tm_id = request.GET.get('tm_id', None)
    if tm_id:
        tm = TradeMark.objects.filter(id=tm_id).first()
        return ', %s' % tm.show_name if tm else ''
    return ''


@register.assignment_tag(takes_context=True)
def get_multiply_four_template(context, paginate_by, to_min=False):
    display_type = context.get('display_type', None)
    if display_type and display_type == 'grid':
        return get_multiply_four(paginate_by, to_min=to_min)
    return paginate_by
