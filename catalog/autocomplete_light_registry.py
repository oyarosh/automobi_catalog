# coding: utf-8
import autocomplete_light

from catalog.models import TradeMark, Part, AC, ACProperty, PartDescription, \
    CarTree

autocomplete_light.register(TradeMark,
                            search_fields=['show_name', 'full_name'])

autocomplete_light.register(Part, search_fields=['^search_number', 'tm__show_name'],
                            widget_attrs={'class': 'modern-style'})

autocomplete_light.register(AC, search_fields=('title',),
                            widget_attrs={'class': 'modern-style'})

autocomplete_light.register(ACProperty, search_fields=('title',),
                            widget_attrs={'class': 'modern-style'})

autocomplete_light.register(PartDescription, search_fields=('description',),
                            widget_attrs={'class': 'modern-style'})

autocomplete_light.register(CarTree, search_fields=['title', ],
                            widget_attrs={'class': 'modern-style'})
