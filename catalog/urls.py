from django.conf.urls import patterns, url, include

from catalog.views import AutomobiCatalog, AutomobiCatalogPart, \
    AutomobiCatalogPartUnit, OEMVehicle, OEMGroups, OEMDetail, \
    OEMDetailByUnit, OEMChildMenu, OEMListUnit, PartInfo, ACView, \
    OEMDetailedSearch, OEMExtendsSelective, OEMGroupsOnlyLC, \
    FpsCatalog, PartDetails, OneClickOrder, CallBackRequest, \
    GetBestOffer, VinCodeHelp, VinSearch, GetCarSearchPopup, \
    CarSearchPopup, CarSearchModification, CarSearchRemove, \
    CarSearchSave, CountryInfoPopup, SaveParamSession


urlpatterns = patterns('catalog.views',
    url(r'^$', ACView.as_view(), name='ac_table'),
    url(r'^brands/$', 'brands', name='brands'),
    url(r'^brands/(?:select-(?P<select_letter>[A-Za-z0-9\-]+))/$', 'brands',
        name='select_brands'),

    url(r'^tree/(?P<tree_first_id>\d+)/$', 'tree_wo_modification', name="tree_w_first"),
    url(r'^tree/(?P<tree_first_id>\d+)/(?P<tree_second_id>\d+)/$', 'tree_wo_modification', name="tree_w_second"),
    url(r'^tree/(?P<tree_first_id>\d+)/(?P<tree_second_id>\d+)/parts/$', 'tree_wo_modification_part', name="tree_w_second_part"),
    url(r'^tree/(?P<tree_first_id>\d+)/(?P<tree_second_id>\d+)/(?P<tree_third_id>\d+)/$', 'tree_wo_modification', name="tree_w_third"),
    url(r'^tree/(?P<tree_first_id>\d+)/(?P<tree_second_id>\d+)/(?P<tree_third_id>\d+)/parts/$', 'tree_wo_modification_part', name="tree_w_third_part"),
    url(r'^tree/(?P<tree_first_id>\d+)/(?P<tree_second_id>\d+)/(?P<tree_third_id>\d+)/(?P<tree_forth_id>\d+)/', 'tree_wo_modification', name="tree_w_forth"),
    url(r'^tree/(?P<tree_first_id>\d+)/(?P<tree_second_id>\d+)/(?P<tree_third_id>\d+)/(?P<tree_forth_id>\d+)/parts/', 'tree_wo_modification_part', name="tree_w_forth_part"),

    url(r'^country-info-popup/$', CountryInfoPopup.as_view(),
        name='country_info_popup'),

    url(r'^save-param-session/$', SaveParamSession.as_view(),
        name='save_param_session'),

    url(r'^get_modification_url/$',
        'get_modification_url', name='get_modification_url'),
    url(r'^category/(?P<category_id>\d+)/$',
        'category', name='category'),
    url(r'^get_detailed_car_search_form/$', OEMDetailedSearch.as_view(),
        name='get_detailed_car_search_form'),

    # order- and paginate- 301 redirecting to url w/o itself. Just SEO
    # this pattern NEED for 301 redirect include of redirect from "filter"
    url(r'^tovary/(?P<first_slug>[A-Za-z0-9\-]+)/', include(patterns('',
        url(r'^(?:filter-(?P<filter>[A-Za-z0-9\-_]+)/)?'
            r'(?:order-(?P<order_by>[A-Za-z0-9\-_]+)/)?'
            r'(?:paginate-(?P<paginate_by>[A-Za-z0-9\-_]+)/)?'
            r'(?:filters-(?P<filters>[A-Za-z0-9\-_]+)/)?'
            r'(?:page-(?P<page>[0-9\-]+)/)?$',
            AutomobiCatalog.as_view(), name="automobi_catalog"),
        url(r'^(?P<second_slug>[A-Za-z0-9\-]+)/', include(patterns('',
            url(r'^(?:filter-(?P<filter>[A-Za-z0-9\-_]+)/)?'
                r'(?:order-(?P<order_by>[A-Za-z0-9\-_]+)/)?'
                r'(?:paginate-(?P<paginate_by>[A-Za-z0-9\-_]+)/)?'
                r'(?:filters-(?P<filters>[A-Za-z0-9\-_]+)/)?'
                r'(?:page-(?P<page>[0-9\-]+)/)?$',
                AutomobiCatalog.as_view(), name="automobi_catalog"),
            url(r'^(?P<third_slug>[A-Za-z0-9\-]+)/', include(patterns('',
                url(r'^(?:filter-(?P<filter>[A-Za-z0-9\-_]+)/)?'
                    r'(?:order-(?P<order_by>[A-Za-z0-9\-_]+)/)?'
                    r'(?:paginate-(?P<paginate_by>[A-Za-z0-9\-_]+)/)?'
                    r'(?:filters-(?P<filters>[A-Za-z0-9\-_]+)/)?'
                    r'(?:page-(?P<page>[0-9\-]+)/)?$',
                    AutomobiCatalog.as_view(), name="automobi_catalog"),
                url(r'^(?P<forth_slug>[A-Za-z0-9\-]+)/', include(patterns('',
                    url(r'^(?:filter-(?P<filter>[A-Za-z0-9\-_]+)/)?'
                        r'(?:order-(?P<order_by>[A-Za-z0-9\-_]+)/)?'
                        r'(?:paginate-(?P<paginate_by>[A-Za-z0-9\-_]+)/)?'
                        r'(?:filters-(?P<filters>[A-Za-z0-9\-_]+)/)?'
                        r'(?:page-(?P<page>[0-9\-]+)/)?$',
                        AutomobiCatalog.as_view(), name="automobi_catalog"),
                    url(r'^(?P<fifth_slug>[A-Za-z0-9\-]+)/', include(patterns('',
                        url(r'^(?:filter-(?P<filter>[A-Za-z0-9\-_]+)/)?'
                            r'(?:order-(?P<order_by>[A-Za-z0-9\-_]+)/)?'
                            r'(?:paginate-(?P<paginate_by>[A-Za-z0-9\-_]+)/)?'
                            r'(?:filters-(?P<filters>[A-Za-z0-9\-_]+)/)?'
                            r'(?:page-(?P<page>[0-9\-]+)/)?$',
                            AutomobiCatalog.as_view(), name="automobi_catalog"),
                    ))),
                ))),
            ))),
        ))),
    ))),

    # url(r'^tovary/$', AutomobiCatalog.as_view(),
    #     name='automobi_catalog'),

    url(r'^search_auto/(?:(?P<car_brand>\d+)/)?$',
        GetCarSearchPopup.as_view(), name='get_car_search_popup'),
    url(r'^search_auto_popup/$',
        CarSearchPopup.as_view(), name='car_search_popup'),
    url(r'^search_auto_modification/$',
        CarSearchModification.as_view(), name='car_search_modification'),
    url(r'^search_auto_remove/$',
        CarSearchRemove.as_view(), name='car_search_remove'),
    url(r'^search_save/$',
        CarSearchSave.as_view(), name='car_save_to_garage'),

    url(r'^get_part_info/(?P<part_id>\d+)/$', PartInfo.as_view(),
        name='get_part_info'),

    url(r'^product/(?P<part_slug>[A-Za-z0-9\-]+)/$', PartDetails.as_view(),
        name='part_details'),

    url(r'^product/(?P<part_slug>[A-Za-z0-9\-]+)/\?modification=(?P<modification_id>\d+)$', PartDetails.as_view(),
        name='part_details'),

    url(r'^automobi_catalog_part/$', AutomobiCatalogPart.as_view(),
        name='automobi_catalog_part'),

    url(r'^automobi_catalog_part_unit/$', AutomobiCatalogPartUnit.as_view(),
        name='automobi_catalog_part_unit'),

    url(r'^oem/vehicle/(?P<vin>\w+)/$', OEMVehicle.as_view(),
        name='oem_vehicle'),
    url(r'^oem/groups/$', OEMGroups.as_view(), name='oem_groups'),
    url(r'^oem/detail/$', OEMDetail.as_view(), name='oem_detail'),
    url(r'^oem/datail/unit/$', OEMDetailByUnit.as_view(),
        name='oem_detail_unit'),
    url(r'^oem/groups/child_menu/$', OEMChildMenu.as_view(),
        name='child_menu'),
    url(r'^oem/detail/list_unit/$', OEMListUnit.as_view(),
        name='list_unit'),
    url(r'^oem/models/$', OEMExtendsSelective.as_view(),
        name='models_selective'),
    url(r'^oem/groups_lc/$', OEMGroupsOnlyLC.as_view(),
        name='oem_groups_only_lc'),
    url(r'^fps_catalog/$', FpsCatalog.as_view(),
        name='fps_catalog'),
    # TODO: this method not using and should be deleted (one-click-order)
    url(r'^one-click-order/$', OneClickOrder.as_view(),
        name='one_click_order'),
    url(r'^call-back-request/$', CallBackRequest.as_view(),
        name='call_back_request'),
    url(r'^get-best-offer/$', GetBestOffer.as_view(),
        name='get_best_offer'),
    url(r'^vin-code-help/$', VinCodeHelp.as_view(),
        name='vin_code_help'),
    url(r'^vin-search/$', VinSearch.as_view(), name='m_vin_search'),

    url(r'^general_models/(?P<car_brand_slug>[A-Za-z0-9\-]+)/(?P<general_model_id>\d+)/$',
        'models', name='selected_general_models'),

    url(r'^(?P<car_brand_slug>[A-Za-z0-9\-]+)/$',
        'general_models', name='models'),
    url(r'^(?P<car_brand_slug>[A-Za-z0-9\-]+)/(?P<car_model_slug>[A-Za-z0-9\-]+)/$',
        'modifications', name='modifications'),
    url(r'^(?P<car_brand_slug>[A-Za-z0-9\-]+)/(?P<car_model_slug>[A-Za-z0-9\-]+)/(?P<car_modification_slug>[A-Za-z0-9\-]+)/$',
        'tree', name='tree'),
    url(r'^get-tree/(?P<car_brand_slug>[A-Za-z0-9\-]+)/(?P<car_model_slug>[A-Za-z0-9\-]+)/(?P<car_modification_slug>[A-Za-z0-9\-]+)/$',
        'get_tree', name='get_tree'),

    url(r'^(?P<car_brand_slug>[A-Za-z0-9\-]+)/(?P<car_model_slug>[A-Za-z0-9\-]+)/(?P<car_modification_slug>[A-Za-z0-9\-]+)/(?P<tree_first_id>\d+)/$',
        'tree', name='tree_first'),

    url(r'^(?P<car_brand_slug>[A-Za-z0-9\-]+)/(?P<car_model_slug>[A-Za-z0-9\-]+)/(?P<car_modification_slug>[A-Za-z0-9\-]+)/', include(patterns('catalog.views',
        url(r'^$', 'tree', name="tree"),
        url(r'^parts/(?P<tm_id>\d+)/$', 'parts', name="parts"),
        url(r'^parts/$', 'parts', name="parts"),
        url(r'^(?P<tree_first_id>\d+)/', include(patterns('catalog.views',
            url(r'^$', 'tree', name="tree"),
            url(r'^parts/(?P<tm_id>\d+)/$', 'parts', name="parts"),
            url(r'^parts/$', 'parts', name="parts"),
            url(r'^(?P<tree_second_id>\d+)/', include(patterns('catalog.views',
                url(r'^$', 'tree', name="tree"),
                url(r'^parts/(?P<tm_id>\d+)/$', 'parts', name="parts"),
                url(r'^parts/$', 'parts', name="parts"),
                url(r'^(?P<tree_third_id>\d+)/', include(patterns('catalog.views',
                    url(r'^$', 'tree', name="tree"),
                    url(r'^parts/(?P<tm_id>\d+)/$', 'parts', name="parts"),
                    url(r'^parts/$', 'parts', name="parts"),
                    url(r'^(?P<tree_forth_id>\d+)/', include(patterns('catalog.views',
                        url(r'^$', 'tree', name="tree"),
                        url(r'^parts/(?P<tm_id>\d+)/$', 'parts', name="parts"),
                        url(r'^parts/$', 'parts', name="parts"),
                    ), namespace='tree')),
                ), namespace='tree')),
            ), namespace='tree')),
        ), namespace='tree')),
    ))),

    url(r'^(?P<car_brand_slug>[A-Za-z0-9\-]+)/(?P<car_model_slug>[A-Za-z0-9\-]+)/(?P<car_modification_slug>[A-Za-z0-9\-]+)/(?P<car_tree_slug>[A-Za-z0-9\-]+)/$',
        'parts_redirect', name='parts_redirect'),
)