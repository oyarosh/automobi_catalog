# coding: utf-8
import re
import json
import logging
from decimal import Decimal
from datetime import datetime

from psycopg2 import ProgrammingError

from django.db.models import Q
# from django.db.models import Min
from django.core.exceptions import MultipleObjectsReturned

from users.models import Role, SO, DC

from superuser.models import Settings

from cabinet.models import CurrencySettings, SupplierDCCurrencyRelation

from price_generator.models import DiscountGroup, PriceListItem
# from price_generator.exceptions import NoPriceFound

from auto_api.methods import api_get_price
from db_share.utils import get_connection
from core.utils import get_default_currency, redis_client
from search.utils import get_roles_ids, get_currency_rates, get_sales_price

from catalog.models import CarModel, CarModification, CarModelCountry, \
    CarText, Part, AC
from catalog.queries import prepare_query
from catalog.tasks import update_popular_product_cache


logger = logging.getLogger('django.request')


class Mixin(object):

    def process(self):
        if isinstance(self.user, Role):
            self.role = self.user
        else:
            if self.user is None or self.user.is_anonymous():
                self.role = None
                return self.is_anonymous()

            self.role = self.user.role
        return getattr(self, self.role.shortcut)()


class MixinDelivery(object):

    def delivery(self):
        if isinstance(self.user, Role):
            self.role = self.user
        else:
            if self.user is None or self.user.is_anonymous():
                self.role = None
                return self.is_anonymous()

            self.role = self.user.role
        return getattr(self, self.role.shortcut)()


class GetPriceLists(Mixin):

    def __init__(self, user, *args, **kwargs):
        self.user = user

    def is_anonymous(self):
        sfa = Settings.objects.filter(code_name='so_for_anonymous').first()
        if sfa and sfa.get_value():
            so = sfa.get_value()
        else:
            so = SO.objects.filter(created_by__is_default=True).first()
        return so.salespricelist_set.basic()

    def superuser(self):
        return self.is_anonymous()

    def dc(self):
        return self.role.salespricelist_set.all()

    def so(self):
        return self.role.salespricelist_set.basic()

    def client(self):
        ct = self.role.get_content_type()
        additional_ids = DiscountGroup.objects.filter(
            content_type=ct,
            object_id=self.role.id,
            so=self.role.current_so,
            sales_price_list__isnull=False
        ).values_list('sales_price_list', flat=True)

        return self.role.current_so.salespricelist_set.filter(
            Q(basic=True) | Q(id__in=additional_ids)
        )

    def supplier(self):
        return self.role.purchasepricelist_set.all()


class GetItems(GetPriceLists):

    def __init__(self, part, user, currency=None, *args, **kwargs):
        self.part = part
        self.currency = currency
        super(GetItems, self).__init__(user, *args, **kwargs)

    def process(self):
        return self.get_items(super(GetItems, self).process())

    def get_items(self, price_lists):
        items = set()

        pricelistitems = PriceListItem.objects.filter(part=self.part, active=True,
                                                      price_list__in=price_lists).all()
        for item in pricelistitems:
            spls = item.price_list.get_spls()
            for spl in spls:
                purchase_price = GetPrice(
                    item=item,
                    spl=spl,
                    user=self.user,
                    get_purchase=True,
                    currency=self.currency
                ).process()

                sales_price = None
                if self.role and self.role.shortcut in ('so', 'dc'):
                    sales_price = GetPrice(
                        item=item,
                        spl=spl,
                        user=self.user,
                        get_purchase=False,
                        currency=self.currency
                    ).process()

                if self.role is None or self.role.shortcut not in ('supplier', 'superuser'):
                    delivery_time = 0
                else:
                    delivery_time = 0

                items.add((item, spl, purchase_price,
                           sales_price, delivery_time))

        return items


class GetCurrencySettings(Mixin):

    def __init__(self, user, spl, first, second, oi_update_time=None,
                 default_query_kwargs=None, old_currency_ratio=False,
                 supplier=None, only_supplier_ratio=False):
        self.user = user
        self.spl = spl
        self.first = first
        self.second = second
        self.old_currency_ratio = old_currency_ratio
        self.supplier_obj = supplier
        self.only_supplier_ratio = only_supplier_ratio

        self.default_query = {'first': self.first, 'second': self.second}
        if old_currency_ratio:
            default_query_kwargs = {'date__lte': oi_update_time}
            # Should enable after deploy to prod migrations 9/11/16
            # default_query_kwargs = {
            #     'date__lte': oi_update_time,
            #     'time__lte': oi_update_time.time()
            # }
        if default_query_kwargs is not None:
            self.default_query.update(default_query_kwargs)

    def is_anonymous(self):
        return self.superuser()

    def superuser(self):
        dc = DC.get_default()
        return CurrencySettings.objects.get_or_raise(
            object_id=dc.id,
            **self.default_query)

    def supplier(self):
        role = self.supplier_obj or self.role
        settings, is_reversed = role.currency_settings.get_or_none(
            **self.default_query)
        if settings:
            return (settings, is_reversed)
        elif self.only_supplier_ratio:
            raise role.currency_settings.model.DoesNotExist(
                'only_supplier_ratio is True and no currency')
        else:
            return self.superuser()

    def dc(self, role=None):
        role = role or self.role
        settings, is_reversed = role.currency_settings.get_or_none(
            **self.default_query)
        return (settings, is_reversed) if settings else self.superuser()

    def so(self, role=None):
        role = role or self.role
        settings, is_reversed = role.currency_settings.get_or_none(
            **self.default_query)
        if settings:
            return (settings, is_reversed)
        elif self.spl is None:
            return self.superuser()
        else:
            return self.dc(self.spl.dc)

    def client(self):
        return self.so(self.role.current_so)


def calculate_price_with_currency(user, spl, price, first, second,
                                  oi_update_time=None, return_ratio=False,
                                  old_currency_ratio=False, supplier=None,
                                  return_only_ratio=False):
    if isinstance(user, Role):
        if user.shortcut == 'supplier':
            supplier = user

    settings, is_reversed = GetCurrencySettings(
        user=user,
        spl=spl,
        first=first,
        second=second,
        oi_update_time=oi_update_time,
        old_currency_ratio=old_currency_ratio,
        supplier=supplier
    ).process()

    if is_reversed:
        ratio = Decimal('1') / settings.value
    else:
        ratio = settings.value

    if return_only_ratio:
        return ratio

    price *= ratio

    if not return_ratio:
        return price

    return price, ratio


class GetPrice(Mixin):
    for_smart_ass = 'http://habrahabr.ru/images/eastereggs/harlemshake.ogg'

    def __init__(self, item, spl, user, currency=None, get_purchase=True,
                 agreement_currency=None, agreement_rounding_method=None,
                 return_in_agreement=False, fixed_price=None,
                 fixed_in_agreement=False, return_currency_ratio=False,
                 get_hidden_price=False, round_price=True, get_guaranteed=False,
                 oi_update_time=None, old_currency_ratio=False,
                 basic_currency=None):
        self.item = item
        self.spl = spl
        self.user = user
        self.currency = currency
        self.get_purchase = get_purchase
        self.agreement_currency = agreement_currency
        self.agreement_rounding_method = agreement_rounding_method
        self.return_in_agreement = return_in_agreement
        self.fixed_price = fixed_price
        self.fixed_in_agreement = fixed_in_agreement
        self.return_currency_ratio = return_currency_ratio
        self.get_hidden_price = get_hidden_price
        self.round_price = round_price
        self.basic_currency = basic_currency if basic_currency else get_default_currency()
        self.basic_currency_id = self.basic_currency.id
        self.get_guaranteed = get_guaranteed
        self.currency_ratio = 0.00
        self.oi_update_time = oi_update_time
        self.old_currency_ratio = old_currency_ratio

        if return_currency_ratio:
            assert return_currency_ratio == return_in_agreement  # just in case

    def process(self):
        price = self.api_response()
        if not price:
            #TODO fix this
            price = 0.00
        price = Decimal('{:.4f}'.format(price))
        currency = self.basic_currency
        if self.return_in_agreement:
            currency = self.agreement_currency
        elif self.currency:
            currency = self.currency

        if self.return_currency_ratio:
            return price, self.currency_ratio, currency
        return price, currency

    def api_response(self):
        currency = self.basic_currency
        if self.agreement_currency:
            currency = self.agreement_currency
        elif self.currency:
            currency = self.currency

        price = self.fixed_price
        if not price:
            role = self.user
            dc_id, so_id, client_id, supplier_id, shortcut = get_roles_ids(role)
            if isinstance(dc_id, int):
                dc_id = (dc_id, )
            currency_rates = get_currency_rates(
                dc_id, so_id, client_id, supplier_id, self.basic_currency_id)

            spls_info = get_sales_price(
                dc_id, so_id, client_id, supplier_id, shortcut, spl_id=self.spl.id)
            try:
                spl_info = spls_info[0]
            except IndexError:
                #  TODO fix it
                return 0.00

            #  Not sure about this solution for currency, but ...
            if self.currency and self.agreement_currency != self.currency:
                currency = self.currency

            if self.agreement_rounding_method:
                spl_info['precision'] = float(self.agreement_rounding_method.precision)
                spl_info['type'] = self.agreement_rounding_method.type

            api_response = api_get_price(
                self.item.id,
                json.dumps(spls_info),
                self.basic_currency_id,
                currency.id,
                json.dumps(currency_rates),
                shortcut,
                so_id,
                client_id,
                self.round_price
            )

            if api_response:
                if self.get_purchase:
                    price = api_response[11]
                else:
                    price = api_response[9]

            if self.return_currency_ratio:
                ratio = calculate_price_with_currency(
                            self.user, self.spl,
                            price, self.basic_currency,
                            self.agreement_currency,
                            oi_update_time=self.oi_update_time,
                            old_currency_ratio=self.old_currency_ratio,
                            return_ratio=self.return_currency_ratio,
                            return_only_ratio=True)
                if ratio:
                    self.currency_ratio = ratio
        else:
            ratio = None
            if self.agreement_currency:
                if self.fixed_in_agreement:
                    price = self.fixed_price
                else:
                    price, ratio = calculate_price_with_currency(
                        self.user, self.spl,
                        price, self.basic_currency, self.agreement_currency,
                        oi_update_time=self.oi_update_time,
                        return_ratio=self.return_currency_ratio,
                        old_currency_ratio=self.old_currency_ratio)
                if self.return_in_agreement:
                    if ratio:
                        self.currency_ratio = ratio
                    return self._round_price(price) if self.round_price else price

                if self.agreement_currency != self.basic_currency:
                    price, ratio = calculate_price_with_currency(
                        self.user, self.spl,
                        price, self.agreement_currency, self.basic_currency,
                        return_ratio=True,
                        oi_update_time=self.oi_update_time,
                        old_currency_ratio=self.old_currency_ratio)
            if self.currency:
                price, ratio = calculate_price_with_currency(
                    self.user, self.spl,
                    price, self.basic_currency,
                    self.currency, return_ratio=True,
                    oi_update_time=self.oi_update_time,
                    old_currency_ratio=self.old_currency_ratio)

            if ratio:
                self.currency_ratio = ratio

            if self.round_price:
                price = self._round_price(price)

        return price

    def _get_currency_ratio(self):
        qs = SupplierDCCurrencyRelation.objects.filter(
            dc=self.spl.dc,
            supplier=self.item.price_list.supplier,
            timestamp__lte=self.item.update_time).first()
        if qs:
            return qs.currency_ratio
        return Decimal('1.00')

    def _round_price(self, price):
        method = None
        if self.agreement_rounding_method and self.agreement_currency == self.currency:
            method = self.agreement_rounding_method
        else:
            if self.currency:
                method = self.currency.rounding_methods.ordered().first()
            else:
                method = self.spl.rounding_method

        if method is None:
            return Decimal('{:.2f}'.format(price))

        return method.calculate(price)


class CarSearchPopupMixin(object):
    option = '''
        <li>
            <a href="#" data-%(step)s="%(data_val)s" data-%(step)s-display="%(data_disp)s">%(data_title)s</a>
        </li>
    '''
    divider_options = '</ul><ul>'

    STEPS_LIST = ['years', 'brands', 'models', 'body', 'engine', 'modification']
    STEP_MAP = {
        'years': 'car_brand',
        'brands': 'car_model',
        'models': 'car_body',
        'body': 'car_engine',
        'engine': 'car_modification',
        'modification': None
    }
    STEP_PROPERTY_MAP = {
        'brands': {
            'data_val': 'id',
            'data_disp': 'cartext',
            'data_title': 'cartext'
        },
        'models': {
            'data_val': 'id',
            'data_disp': 'cartext',
            'data_title': 'cartext'
        },
        'body': {
            'data_val': 'id',
            'data_disp': 'cartext',
            'data_title': 'cartext'
        },
        'engine': {
            'data_val': 'id',
            'data_disp': 'search_popup_title',
            'data_title': 'search_popup_title'
        }
    }

    def car_model(self):
        queryset = CarModel.objects.all()
        if self.brands:
            queryset = queryset.filter(car_brand=self.brands)
        if self.years and len(self.years) > 3:
            queryset = queryset.filter(Q(startdate_production__year=int(self.years))
                                       | Q(enddate_production__year=int(self.years))
                                       | (Q(startdate_production__lte=datetime.strptime(self.years, '%Y'))
                                          & Q(Q(enddate_production__gte=datetime.strptime(self.years, '%Y'))
                                            | Q(enddate_production=None))))
        country = Settings.objects.filter(code_name='default_tec_doc_country').first()
        if country:
            queryset = queryset.filter(model_country__country_id=country.object_id)

        queryset = queryset.extra(
            select={'title': 'catalog_carmodelcountry.title',
                    'car_model_tex_id': 'catalog_carmodelcountry.id'}
        )

        ids_qs = queryset.order_by('model_country__general_title')\
            .distinct('model_country__general_title')\
            .values_list('model_country__general_title', flat=True)
        return CarText.objects.filter(id__in=ids_qs)

    def car_modification_filter(self, update_session_model=False):
        queryset = CarModification.objects.all()
        if self.brands:
            queryset = queryset.filter(car_model__car_brand=self.brands)

        car_model_ids = None
        if self.models:
            car_model_general_title_id = self.models
            car_model_countries = CarModelCountry.objects.filter(general_title_id=car_model_general_title_id)
            car_model_ids = car_model_countries.values_list('car_model_id', flat=True)
        if car_model_ids:
            queryset = queryset.filter(car_model__in=car_model_ids)
            if update_session_model:
                self.request.session['catalog_car_model'] = \
                    CarText.objects.get(id=car_model_general_title_id).cartext
        if self.years and len(self.years) > 3:
            queryset = queryset.filter(Q(startdate_production__year=int(self.years))
                                       | Q(enddate_production__year=int(self.years))
                                       | (Q(startdate_production__lte=datetime.strptime(self.years, '%Y'))
                                          & Q(Q(enddate_production__gte=datetime.strptime(self.years, '%Y'))
                                            | Q(enddate_production=None))))
        country = Settings.objects.filter(code_name='default_tec_doc_country').first()
        if country:
            queryset = queryset.filter(
                modification_country__country_id=country.object_id)

        queryset = queryset.extra(
            select={'title': 'catalog_carmodificationcountry.title'}
        )
        queryset = queryset.distinct()\
            .order_by('modification_country__title')

        return queryset

    def car_body(self):
        queryset = self.car_modification_filter()
        ids_qs = queryset.order_by('body').distinct('body').values_list('body', flat=True)
        return CarText.objects.filter(id__in=ids_qs)

    def car_engine(self):
        queryset = self.car_modification_filter()
        if self.body:
            queryset = queryset.filter(body_id=self.body)
        ids_qs = queryset.order_by('engine').distinct('engine').values_list('engine', flat=True)
        return CarText.objects.filter(id__in=ids_qs)

    def car_modification(self):
        queryset = self.car_modification_filter(update_session_model=True)
        if self.body:
            queryset = queryset.filter(body_id=self.body)
        if self.engine:
            queryset = queryset.filter(engine_id=self.engine)

        queryset = queryset\
            .distinct('show_name', 'hp_engine_power', 'car_model__car_type_shot_key')\
            .order_by('hp_engine_power')
        return queryset


def add_general_title_to_carmodelcountry_by_json(path):
    with open(path) as data_file:
        data = json.load(data_file)

    for pk, general_title in data.iteritems():
        pk = pk.strip()
        general_title = general_title.strip()

        obj = CarModelCountry.objects.filter(id=pk)

        if not obj:
            continue

        obj = obj.first()

        try:
            car_text = CarText.objects.get(cartext=general_title)
        except CarText.DoesNotExist:
            car_text = CarText.objects.create(cartexttype_id=1, cartext=general_title)
        except MultipleObjectsReturned:
            car_text = CarText.objects.filter(cartext=general_title).first()

        obj.general_title = car_text
        obj.save(update_fields=['general_title', ])
        print obj.id


def is_pk(str):
    try:
        int(str)
        return True
    except ValueError:
        return False


def get_multiply_four(paginate_by, to_min=False):
    modulo = paginate_by % 4
    return paginate_by - modulo if to_min else paginate_by + (4 - modulo)


def get_popular_parts(spl_ids=[]):
    redis = redis_client(db=3)
    keys = redis.keys('popular_parts:plis_block=*')

    plis = []
    if keys:
        key = keys[0]
        if redis.get(key):
            try:
                plis = json.loads(redis.get(key))
            except TypeError:
                pass
        redis.delete(key)
    else:
        value = update_popular_product_cache(iter_rows=1, to_cache=False)
        try:
            plis = json.loads(value)
        except TypeError:
            pass
        update_popular_product_cache.delay()

    return plis


def catalog_process(query):
    pattern = r'[^0-9]'
    split_pattern = ['W', '/', '\\', 'X']
    iter_query = query
    for item in iter_query.split(' '):
        if len(item) > 2 and True in [len(item.upper().split(x)) == 2 for x in split_pattern]:
            item_new = re.sub(pattern, ' ', item)
            query = query.replace(item, item_new)
    return query


def part_full_text_qs(query='', ac_part_filter_enable=True,
                      empty_query_result=True, search_field='description',
                      ac_category=None, is_catalog_process=False,
                      sales_price_list_ids=[], *args, **kwargs):

    ac_categories = []
    query = re.sub('\s+', ' ', query).strip()

    if is_catalog_process:
        query = catalog_process(query)

    if not empty_query_result and query == '':
        return Part.objects.none()

    if ac_category:
        root_ac = AC.objects.get(id=ac_category)
        full_ac = root_ac.get_descendants(include_self=True)
        ac_categories = full_ac.values_list('id', flat=True)

    tgrm_conditions = [item for item in query.split(' ') if item and len(item) > 2]
    tsquery_conditions = [item for item in query.split(' ') if item and len(item) < 3]
    if tsquery_conditions == ['']:
        tsquery_conditions = []

    data = {
        'tgrm_conditions': tgrm_conditions,
        'tsquery_conditions': tsquery_conditions,
        'ac_part_filter_enable': ac_part_filter_enable,
        'search_field': search_field,
        'ac_categories': ac_categories,
        'sales_price_list_ids': sales_price_list_ids
    }

    with get_connection('api').cursor() as cur:
        query, param = prepare_query('part_full_text', **data)
        try:
            cur.execute(query, **{})
        except ProgrammingError:
            logger.warning('ProgrammingError: catalog.utils:638')
            return Part.objects.none()
        ids_tuples = cur.fetchall()

    if ac_part_filter_enable:
        ids = [tup_item[0] for tup_item in ids_tuples if tup_item[1]]
    else:
        ids = [tup_item[0] for tup_item in ids_tuples]
    return Part.objects.filter(id__in=ids)
