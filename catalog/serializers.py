from rest_framework import serializers

from catalog.models import CatalogUploadObject, AC, ACProperty, ACPropertyValue, \
                           PartDescription, PartDescriptionText, PartImage


class CatalogUploadObjectSerializer(serializers.ModelSerializer):

    class Meta:
        model = CatalogUploadObject


class ACSerializer(serializers.ModelSerializer):
    parts = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = AC


class ACPropertySerializer(serializers.ModelSerializer):
    acs = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = ACProperty


class ACPropertyValueSerializer(serializers.ModelSerializer):
    parts = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = ACPropertyValue


class PartDescriptionSerializer(serializers.ModelSerializer):

    class Meta:
        model = PartDescription


class PartDescriptionTextSerializer(serializers.ModelSerializer):

    class Meta:
        model = PartDescriptionText


class PartImageSerializer(serializers.ModelSerializer):

    class Meta:
        model = PartImage