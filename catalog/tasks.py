# coding=utf-8
import time
import json
from datetime import datetime, timedelta
from hashlib import md5

from django.db.models import Q

from celery.task import periodic_task
from celery.task.schedules import crontab

from db_share.utils import get_connection

from core.utils import redis_client
from laximo.api import AfterMarket
from price_generator.models import PurchasePriceList, PriceListItem
from laximo.utils import laximo_cross_process, get_actual_trademark_id, \
    is_cross_doubles, part_add, part_process, send_laximo_report
from superuser.models import Settings

from catalog.queries import prepare_query
from catalog.models import LaximoCross, Part, Cross


@periodic_task(run_every=crontab(minute=0, hour=23))
def add_laximo_crosses():
    qs = LaximoCross.objects.filter(is_checked=False)
    for item in qs:
        part_tm_id = get_actual_trademark_id(item.part_manufacturer)
        cross_tm_id = get_actual_trademark_id(item.cross_manufacturer)

        if not part_tm_id or not cross_tm_id:
            continue

        part_tm_id = part_tm_id[0]
        cross_tm_id = cross_tm_id[0]

        if (not item.original_part) or \
           (item.original_part and item.original_part.tm.id != part_tm_id):
            part = Part.objects.filter(search_number=item.part_oem, tm_id=part_tm_id)
            if part:
                item.original_part = part[0]
            else:
                item.original_part = part_add(item, part_tm_id)
        if (not item.original_cross) or \
           (item.original_cross and item.original_cross.tm.id != cross_tm_id):
            part = Part.objects.filter(search_number=item.cross_oem, tm_id=cross_tm_id)
            if part:
                item.original_cross = part[0]
            else:
                item.original_cross = part_add(item, cross_tm_id,
                                               prefix='cross')

        part_process(item, part_tm_id)
        part_process(item, cross_tm_id, prefix='cross')

        item.is_checked = True
        item.save(update_fields=[
            'original_cross',
            'original_part',
            'is_checked'])

    qs_to_add = LaximoCross.objects.filter(is_checked=True, is_copied=False)
    for item in qs_to_add:
        item.is_copied = True
        item.save(update_fields=['is_copied', ])
        if is_cross_doubles(item.original_part_id, item.original_cross_id):
            continue
        Cross.objects.create(
            part=item.original_part, cross=item.original_cross)

    send_laximo_report(qs_to_add)


@periodic_task(run_every=crontab(minute='*/1'))
def parse_laximo_crosses():
    period = Settings.get_value_by_name('laximo_period')
    part_per_period = Settings.get_value_by_name('laximo_part_per_period')
    laximo_ppl = Settings.get_value_by_name('laximo_ppl')
    laximo_tm_id = Settings.get_value_by_name('laximo_tm_id')

    if not period:
        return

    if not part_per_period:
        part_per_period = '1'

    redis = redis_client(db=3)

    last_parse = redis.get('laximo_crosses:last_parse')
    dt_period = timedelta(minutes=int(period))
    if not last_parse:
        last_parse = (datetime.now() - dt_period).strftime("%Y-%m-%d_%H:%M:%S.%f")

    dt_last_parse = datetime.strptime(last_parse, "%Y-%m-%d_%H:%M:%S.%f")
    if datetime.now() > dt_period + dt_last_parse:
        return

    redis.set(
        'laximo_crosses:last_parse',
        datetime.now().strftime("%Y-%m-%d_%H:%M:%S.%f"))

    ppl = None
    if laximo_ppl:
        ppl = PurchasePriceList.objects.get(id=laximo_ppl.id)

    query = Q()
    if ppl:
        query &= Q(price_list_id=ppl.id) & Q(server_id=ppl.server_id)

    if laximo_tm_id:
        query &= Q(part__tm_id=int(laximo_tm_id))
    pli_qs = PriceListItem.objects.filter(query).order_by('-id')

    last_ppl = redis.get('laximo_crosses:last_ppl')
    if not last_ppl:
        last_ppl = laximo_ppl

    last_pli = redis.get('laximo_crosses:last_pli')
    if not last_pli or laximo_ppl != last_ppl:
        last_pli = pli_qs[0].id

    pli_qs = pli_qs.filter(id__lte=last_pli)

    part_list = []
    pli = None
    iter_number = 0
    while int(part_per_period) > len(part_list):
        pli = pli_qs[iter_number]
        part = pli.part
        if LaximoCross.objects.filter(original_part=part).exists():
            continue
        if part in part_list:
            continue
        part_list.append(part)
        iter_number += 1

    if pli:
        redis.set('laximo_crosses:last_pli', pli.id)

    # parse
    for part in part_list:

        param = {'OEM': part.search_number}
        result = AfterMarket().request('FindOEM', **param)
        laximo_cross_process(result, part)

        time.sleep(60 / int(part_per_period))
        redis.set(
            'laximo_crosses:last_parse',
            datetime.now().strftime("%Y-%m-%d_%H:%M:%S.%f"))


@periodic_task(run_every=crontab(minute='*/60'))
def update_popular_product_cache(spl_ids=[], iter_rows=300, to_cache=True):
    redis = redis_client(db=3)
    key_template = 'popular_parts:plis_block='

    if not spl_ids:
        spls_for_pp = Settings.get_value_by_name('prices_for_popular_parts')
        spl_ids = map(int, spls_for_pp.split(';'))

    if not spl_ids:
        return []

    keys = redis.keys(key_template + '*')
    range_to = iter_rows - len(keys)

    for number in xrange(0, range_to):
        items = {}
        list_items = []
        with get_connection('api').cursor() as cur:
            query, params = prepare_query('popular_parts', spl_ids=spl_ids)
            cur.execute(query, params)
            for i in cur.fetchall():
                items = {
                    'pli_id': i[0],
                    'part_id': i[1],
                    'catalog_number': i[2],
                    'description': i[3],
                    'slug': i[4],
                    'tm_show_name': i[5],
                    'image': i[6]
                }
                list_items.append(items)
        hash_key = md5(str(items['pli_id'] + number)).hexdigest()
        key = key_template + hash_key
        value = json.dumps(list_items)
        if to_cache:
            redis.set(key, value)
        else:
            return value
