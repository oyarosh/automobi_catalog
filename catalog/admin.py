import autocomplete_light
from django.contrib import admin
from catalog.models import *


class CrossAdmin(admin.ModelAdmin):
    raw_id_fields = ("part", "cross",)


class PartAdmin(admin.ModelAdmin):
    raw_id_fields = ("tm", )

class CarBrandAdmin(admin.ModelAdmin):
    search_fields = ("title", )

# admin.site.register(CarBrand, CarBrandAdmin)
# admin.site.register(CarModel)
# admin.site.register(CarModification)
# admin.site.register(CarTextType)
# admin.site.register(CarText)
# admin.site.register(TradeMark)
# admin.site.register(EngineTextType)
# admin.site.register(EngineText)
# admin.site.register(EngineModification)
# admin.site.register(Unit)
# admin.site.register(PartPropertyType)
# admin.site.register(PartProperty)
# admin.site.register(CarTree)
# admin.site.register(PartSet)
# admin.site.register(Part, PartAdmin)
# admin.site.register(Cross, CrossAdmin)
# admin.site.register(PartEngine)
# admin.site.register(GoodsCategoryGroup)
# admin.site.register(GoodsCategory)
# admin.site.register(CatalogGood)
# admin.site.register(Barcode)


# class PartDescriptionAdmin(admin.ModelAdmin):
#     form = autocomplete_light.modelform_factory(PartDescription)
# admin.site.register(PartDescription, PartDescriptionAdmin)


# class PartImageAdmin(admin.ModelAdmin):
#     form = autocomplete_light.modelform_factory(PartImage)
# admin.site.register(PartImage, PartImageAdmin)


# class AcAdmin(admin.ModelAdmin):
#     form = autocomplete_light.modelform_factory(AC)
# admin.site.register(AC, AcAdmin)


# class ACPropertyAdmin(admin.ModelAdmin):
#     form = autocomplete_light.modelform_factory(ACProperty)
# admin.site.register(ACProperty, ACPropertyAdmin)


# class ACPropertyValueAdmin(admin.ModelAdmin):
#     form = autocomplete_light.modelform_factory(ACPropertyValue)
# admin.site.register(ACPropertyValue, ACPropertyValueAdmin)
