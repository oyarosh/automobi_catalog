import factory
from factory import fuzzy
from factory.django import DjangoModelFactory

from catalog import models


class TradeMarkFactory(DjangoModelFactory):
    show_name = full_name = fuzzy.FuzzyText()
    tec_doc_id = 1

    class Meta:
        model = models.TradeMark


class CarTreeFactory(DjangoModelFactory):
    node_id = 1

    class Meta:
        model = models.CarTree


class CarBrandFactory(DjangoModelFactory):

    class Meta:
        model = models.CarBrand


class CarModelFactory(DjangoModelFactory):
    car_brand = factory.SubFactory(CarBrandFactory)

    class Meta:
        model = models.CarModel


class CarTextTypeFactory(DjangoModelFactory):

    class Meta:
        model = models.CarTextType


class CarTextFactory(DjangoModelFactory):
    cartexttype = factory.SubFactory(CarTextTypeFactory)

    class Meta:
        model = models.CarText


class PartFactory(DjangoModelFactory):
    tm = factory.SubFactory(TradeMarkFactory)
    # car_tree = factory.SubFactory(CarTreeFactory)
    catalog_number = factory.Sequence(lambda n: n)
    description = fuzzy.FuzzyText()

    @factory.lazy_attribute
    def search_number(obj):
        return obj.catalog_number

    class Meta:
        model = models.Part


class CrossFactory(DjangoModelFactory):
    part = factory.SubFactory(PartFactory)
    cross = factory.SubFactory(PartFactory)

    class Meta:
        model = models.Cross

# class ACFactory(DjangoModelFactory):
#     title = fuzzy.FuzzyText()

#     class Meta:
#         model = models.AC


# class ACPropertyFactory(DjangoModelFactory):
#     title = fuzzy.FuzzyText()
#     ac = factory.SubFactory(ACFactory)

#     class Meta:
#         model = models.ACProperty


# class ACPropertyValueFactory(DjangoModelFactory):
#     title = fuzzy.FuzzyText()
#     ac_property = factory.SubFactory(ACPropertyFactory)

#     class Meta:
#         model = models.ACPropertyValue
