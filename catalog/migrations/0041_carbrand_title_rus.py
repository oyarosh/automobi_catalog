# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0040_auto_20150807_1631'),
    ]

    operations = [
        migrations.AddField(
            model_name='carbrand',
            name='title_rus',
            field=models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u043d\u0430 \u0440\u0443\u0441\u0441\u043a\u043e\u043c', blank=True),
            preserve_default=True,
        ),
    ]
