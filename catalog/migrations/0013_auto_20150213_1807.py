# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import cabinet.mixins


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_auto_20150213_1234'),
        ('catalog', '0012_auto_20150213_1234'),
    ]

    operations = [
        migrations.CreateModel(
            name='PartDescriptionText',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.TextField()),
                ('language', models.ForeignKey(to='core.Language')),
                ('part_description', models.ForeignKey(to='catalog.PartDescription')),
            ],
            options={
            },
            bases=(cabinet.mixins.SerializationMixin, models.Model),
        ),
        migrations.RemoveField(
            model_name='partdescription',
            name='description',
        ),
        migrations.RemoveField(
            model_name='partdescription',
            name='language',
        ),
        migrations.RemoveField(
            model_name='partdescription',
            name='part',
        ),
        migrations.AddField(
            model_name='part',
            name='default_description',
            field=models.ForeignKey(to='catalog.PartDescription', null=True),
            preserve_default=True,
        ),
    ]
