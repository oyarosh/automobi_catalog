# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import cabinet.mixins


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0087_laximocross'),
    ]

    operations = [
        migrations.AddField(
            model_name='laximocross',
            name='original_part',
            field=cabinet.mixins.ApiForeignKey(verbose_name='\u0417\u0430\u043f\u0447\u0430\u0441\u0442\u044c \u0430\u0432\u0442\u043e\u043c\u043e\u0431\u0438', blank=True, to='catalog.Part', null=True),
            preserve_default=True,
        ),
    ]
