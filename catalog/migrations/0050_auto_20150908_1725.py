# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0049_remove_partcriteria_part_id'),
    ]

    operations = [
        migrations.RunSQL(
            """
                DELETE FROM catalog_partcriteria WHERE catalog_part_id IS NULL;
            """),
    ]
