# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0014_auto_20150216_1605'),
    ]

    operations = [
        migrations.AlterField(
            model_name='trademark',
            name='tec_doc_id',
            field=models.IntegerField(null=True),
        ),
    ]
