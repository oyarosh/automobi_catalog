# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def update_car_text_one_title(apps, schema_editor):
    CarText = apps.get_model("catalog", "CarText")
    car_text = CarText.objects.get(id=15587)
    car_text.cartext = u'Мотоцикл'
    car_text.save(update_fields=['cartext'])


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0080_auto_20160426_1406'),
    ]

    operations = [
        migrations.RunPython(update_car_text_one_title),
    ]
