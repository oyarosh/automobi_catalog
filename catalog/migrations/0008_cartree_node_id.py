# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0007_auto_20150209_2155'),
    ]

    operations = [
        migrations.AddField(
            model_name='cartree',
            name='node_id',
            field=models.PositiveIntegerField(default=0, db_index=True),
            preserve_default=False,
        ),
    ]
