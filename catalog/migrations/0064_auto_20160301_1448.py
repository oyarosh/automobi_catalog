# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0063_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='unit',
            name='code',
            field=models.CharField(max_length=255, null=True, verbose_name='\u041a\u043e\u0434'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='unit',
            name='full_name',
            field=models.CharField(max_length=255, null=True, verbose_name='\u041f\u043e\u043b\u043d\u043e\u0435 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435'),
            preserve_default=True,
        ),
    ]
