# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0024_partimage_tecdoc_image'),
    ]

    operations = [
        migrations.CreateModel(
            name='LinkCarModificationGenericArticle',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('car_modification_id', models.IntegerField(null=True, db_index=True)),
                ('link_generic_article_part_id', models.IntegerField(null=True, db_index=True)),
                ('generic_article_id', models.IntegerField(null=True, db_index=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='LinkGenericArticlePart',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('part_id', models.IntegerField(null=True, db_index=True)),
                ('generic_article_id', models.IntegerField(null=True, db_index=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SearchTreeGenericArticle',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tree_id', models.IntegerField(null=True, db_index=True)),
                ('generic_article_id', models.IntegerField(null=True, db_index=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='enginemodification',
            name='tm',
        ),
        migrations.AddField(
            model_name='cartree',
            name='sort',
            field=models.IntegerField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='cartree',
            name='type',
            field=models.IntegerField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='enginemodification',
            name='car_brand',
            field=models.ForeignKey(default=1, verbose_name='\u0422\u043e\u0440\u0433\u043e\u0432\u0430\u044f \u043c\u0430\u0440\u043a\u0430', to='catalog.CarBrand'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='enginemodification',
            name='camdrive',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u0422\u0438\u043f \u041f\u0440\u0438\u0432\u043e\u0434\u0430 \u0420\u0430\u0441\u043f\u0440\u0435\u0434\u0432\u0430\u043b\u0430', blank=True, to='catalog.CarText', null=True),
        ),
        migrations.AlterField(
            model_name='enginemodification',
            name='camtype',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u0422\u0438\u043f \u0421\u0438\u0441\u0442\u0435\u043c\u044b \u0420\u0430\u0441\u043f\u0440\u0435\u0434\u0432\u0430\u043b\u0430', blank=True, to='catalog.CarText', null=True),
        ),
        migrations.AlterField(
            model_name='enginemodification',
            name='charge',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u0422\u0438\u043f \u041d\u0430\u0434\u0434\u0443\u0432\u0430', blank=True, to='catalog.CarText', null=True),
        ),
        migrations.AlterField(
            model_name='enginemodification',
            name='cooltype',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u0422\u0438\u043f \u0421\u0438\u0441\u0442\u0435\u043c\u044b \u041e\u0445\u043b\u0430\u0436\u0434\u0435\u043d\u0438\u044f', blank=True, to='catalog.CarText', null=True),
        ),
        migrations.AlterField(
            model_name='enginemodification',
            name='cylpos',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u0420\u0430\u0441\u043f\u043e\u043b\u043e\u0436\u0435\u043d\u0438\u0435 \u0426\u0438\u043b\u0438\u043d\u0434\u0440\u043e\u0432', blank=True, to='catalog.CarText', null=True),
        ),
        migrations.AlterField(
            model_name='enginemodification',
            name='econorm',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u042d\u043a\u043e\u043b\u043e\u0433\u0438\u0447\u0435\u0441\u043a\u0430\u044f \u041d\u043e\u0440\u043c\u0430', blank=True, to='catalog.CarText', null=True),
        ),
        migrations.AlterField(
            model_name='enginemodification',
            name='fuel',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u0422\u0438\u043f \u0422\u043e\u043f\u043b\u0438\u0432\u0430', blank=True, to='catalog.CarText', null=True),
        ),
        migrations.AlterField(
            model_name='enginemodification',
            name='fueltype',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u0422\u0438\u043f \u0422\u043e\u043f\u043b\u0438\u0432\u0430', blank=True, to='catalog.CarText', null=True),
        ),
        migrations.AlterField(
            model_name='enginemodification',
            name='injtype',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u0422\u0438\u043f \u041f\u043e\u0434\u0430\u0447\u0438 \u0422\u043e\u043f\u043b\u0438\u0432\u0430', blank=True, to='catalog.CarText', null=True),
        ),
        migrations.AlterField(
            model_name='enginemodification',
            name='valvedrive',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u0422\u0438\u043f \u0423\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u0438\u044f \u041a\u043b\u0430\u043f\u0430\u043d\u0430\u043c\u0438', blank=True, to='catalog.CarText', null=True),
        ),
    ]
