# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_auto_20150213_1234'),
        ('catalog', '0011_auto_20150212_1304'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='unit',
            name='language',
        ),
        migrations.AddField(
            model_name='unit',
            name='translation',
            field=models.ForeignKey(default=0, to='core.Translation'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='unit',
            name='tec_doc_id',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='weightsizespecification',
            name='unit',
            field=models.ForeignKey(verbose_name='\u0415\u0434\u0438\u043d\u0438\u0446\u0430 \u0438\u0437\u043c\u0435\u0440\u0435\u043d\u0438\u044f \u0410\u0432\u0442\u043e\u043c\u043e\u0431\u0438', blank=True, to='catalog.Unit'),
        ),
    ]
