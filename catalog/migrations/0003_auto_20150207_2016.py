# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0002_auto_20150206_1826'),
    ]

    operations = [
        migrations.AlterField(
            model_name='partdescription',
            name='part',
            field=models.ForeignKey(to='catalog.Part'),
        ),
    ]
