# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0041_carbrand_title_rus'),
    ]

    operations = [
        migrations.RunSQL(
            """
                CREATE OR REPLACE FUNCTION translit_str(p_string character varying)
                  RETURNS character varying AS
                $BODY$
                select 
                initcap(
                translate(
                replace(
                replace(
                replace(
                replace(
                replace(
                replace(lower($1),
                'zh', 'ж'),
                'ch', 'ч'),
                'shh', 'щ'),
                'sh', 'ш'),
                'yu', 'ю'),
                'ya', 'я'), 
                'abvgdeezijklmnoprstufхc`', 'абвгдеёзийклмнопрстуфхць'));
                $BODY$
                  LANGUAGE sql IMMUTABLE
                  COST 100;
                UPDATE
                    catalog_carbrand
                SET title_rus = translit_str(title);
            """),
    ]
