# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import re

from django.db import models, migrations
from catalog.models import CarBrand


def update_car_brand_logo(apps, schema_editor):
    for car_brand in CarBrand.objects.all():
        try:
            car_brand.logo.name = re.sub('\.(.+)', '.jpg', car_brand.logo.name)
            car_brand.save(update_fields=['logo', ])
        except:
            continue
        print car_brand


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0068_auto_20160314_1217'),
    ]

    operations = [
        migrations.RunPython(update_car_brand_logo),
    ]
