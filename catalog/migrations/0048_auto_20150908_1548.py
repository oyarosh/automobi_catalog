# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0047_auto_20150908_1543'),
    ]

    operations = [
        migrations.RunSQL(
            """
                UPDATE catalog_partcriteria
                SET catalog_part_id = catalog_part.id
                FROM catalog_part
                WHERE catalog_part.tecdoc = catalog_partcriteria.part_id;
            """),
    ]
