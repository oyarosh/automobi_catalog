# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0017_auto_20150330_1310'),
    ]

    operations = [
        migrations.CreateModel(
            name='GeneratedPart',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('search_number', models.CharField(max_length=255)),
                ('catalog_number', models.CharField(max_length=255)),
                ('description', models.TextField(null=True)),
                ('part', models.ForeignKey(to='catalog.Part')),
                ('tm', models.ForeignKey(to='catalog.TradeMark')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GeneratedTradeMark',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('show_name', models.CharField(max_length=255)),
                ('tm', models.ForeignKey(to='catalog.TradeMark')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
