# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0030_carbrand_is_shown'),
    ]

    operations = [
        migrations.AddField(
            model_name='carbrand',
            name='slug',
            field=models.SlugField(null=True, max_length=280, blank=True, unique=True, verbose_name='URL'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='carmodel',
            name='slug',
            field=models.SlugField(null=True, max_length=280, blank=True, unique=True, verbose_name='URL'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='carmodification',
            name='slug',
            field=models.SlugField(null=True, max_length=280, blank=True, unique=True, verbose_name='URL'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='cartree',
            name='slug',
            field=models.SlugField(null=True, max_length=280, blank=True, unique=True, verbose_name='URL'),
            preserve_default=True,
        ),
    ]
