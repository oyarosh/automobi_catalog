# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

from catalog.models import CarBrand


def update_car_brand_logo_3(apps, schema_editor):
    car_brand_title = ['AC', 'Anadol', 'Autobianchi', 'Avia', 'Baic',
        'Baiyun', 'Bedford', 'Benelli Motorcycles', 'Bertone', 'Borgward',
        'Buell Motorcycles', 'Bufori', 'Callaway', 'Carbodies', 'Changhe',
        'Checker', 'CODA', 'Denza', 'DR', 'Ebro', 'Excalibur', 'Fengshen',
        'Fisker', 'Fpv', 'GAC Gonov', 'Glas', 'Gleagle (Geely)', 'Gonov',
        'Hanjiang', 'Hercules Motorcycles', 'Hindustan', 'Hobbycar',
        'Holder', 'Hongqi', 'Huali', 'Hyosung Motorcycles', 'Icml',
        'Isdera', 'Isotta Fraschini', 'Italjet Motorcycles', 'Jianghan',
        'Jiefang', 'Jinbei (Brilliance)', 'Jonway', 'Laforza', 'Laverda Motorcycles',
        'LDV', 'Lobini', 'Lotus (Youngman)', 'Luxgen', 'Malaguti Motorcycles',
        'Marcos', 'Maruti Suzuki', 'Maxus (Saic Motor)', 'Mbk Motorcycles',
        'Mclaren', 'Mega', 'Metrocab', 'Middlebridge', 'Mitsuoka', 'Morgan',
        'Moto-Morini Mc', 'MZ Motorcycles', 'Noble', 'Nsu', 'Oldsmobile',
        'Packard', 'Panoz', 'Panther', 'Paykan', 'Peugeot Motorcycles',
        'Piaggio', 'Piaggio Motorcycles', 'Pininfarina', 'Premier',
        'Qingling', 'Qoros', 'Ranger', 'Rayton Fissore', 'Reliant',
        'Reva', 'Riich', 'Riley', 'Saleen', 'San', 'Santana', 'Shelby',
        'Saturn', 'Shuguang', 'Spyker', 'Standard', 'Stanguellini',
        'Suzuki (Changan)', 'Suzuki (Changhe)', 'Talbot', 'Tianqi Meiya Auto',
        'Tongtian', 'Tornax Motorcycles', 'Trabant', 'Triumph', 'Triumph Motorcycles',
        'UMM', 'Vector', 'Venturi', 'Venucia', 'VPG', 'Wolseley', 'Wuling', 'Xinkai',
        'Ycaco', 'Yuejin', 'Yulon', 'Zastava', 'Zotye', 'GAC Gonow', 'Gonow',
        'Maple (SMA)', 'Workhorse', 'Yugo']
    for car_brand in CarBrand.objects.filter(title__in=car_brand_title):
        car_brand.logo = None
        car_brand.save(update_fields=['logo', ])
        print car_brand


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0070_auto_20160315_1955'),
    ]

    operations = [
        migrations.RunPython(update_car_brand_logo_3),
    ]
