# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

import os.path
from catalog.models import CarBrand


def update_car_brand_logo_2(apps, schema_editor):
    for car_brand in CarBrand.objects.all():
        print u'Скопируй /media/car_brand_logos с дева на прод'
        try:
            if not os.path.isfile(car_brand.logo.path):
                car_brand.logo = None
                car_brand.save(update_fields=['logo', ])
                print car_brand
        except:
            continue


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0069_auto_20160315_1825'),
    ]

    operations = [
        migrations.RunPython(update_car_brand_logo_2),
    ]
