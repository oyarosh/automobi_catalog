# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0052_cross_created'),
    ]

    operations = [
        migrations.AddField(
            model_name='partcriteria',
            name='car_modification_id',
            field=models.IntegerField(null=True, db_index=True),
            preserve_default=True,
        ),
    ]
