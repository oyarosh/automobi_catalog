# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0026_auto_20150703_1146'),
    ]

    operations = [
        migrations.AlterField(
            model_name='part',
            name='tecdoc',
            field=models.PositiveIntegerField(db_index=True, null=True, verbose_name='TecDoc', blank=True),
        ),
    ]
