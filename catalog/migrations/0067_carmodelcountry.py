# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0066_auto_20160301_1642'),
    ]

    operations = [
        migrations.CreateModel(
            name='CarModelCountry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u0422\u0438\u043f \u0410\u0432\u0442\u043e')),
                ('car_model', models.ForeignKey(to='catalog.CarModel')),
                ('country', models.ForeignKey(to='catalog.SalesCountry')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
