# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0048_auto_20150908_1548'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='partcriteria',
            name='part_id',
        ),
    ]
