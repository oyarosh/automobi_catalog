# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0046_auto_20150908_1244'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='partcriteria',
            name='text_value',
        ),
        migrations.AddField(
            model_name='partcriteria',
            name='catalog_part',
            field=models.ForeignKey(verbose_name='\u0417\u0430\u043f\u0447\u0430\u0441\u0442\u044c', to='catalog.Part', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='partcriteria',
            name='part_id',
            field=models.IntegerField(null=True, verbose_name='\u0422\u0435\u043a\u0414\u043e\u043a ID', db_index=True),
        ),
    ]
