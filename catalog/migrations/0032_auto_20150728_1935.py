# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, IntegrityError, transaction
import pytils


def update_car_brand(apps, schema_editor):
    print 'CarBrand slug update start'
    CarBrand = apps.get_model("catalog", "CarBrand")
    for item in CarBrand.objects.all():
        item.slug = pytils.translit.slugify(item.title)
        try:
            with transaction.atomic():
                item.save(update_fields=['slug'])
        except IntegrityError:
            item.slug += '-%s' % item.id
            item.save(update_fields=['slug'])


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0031_auto_20150728_1934'),
    ]

    operations = [
        migrations.RunPython(update_car_brand),
    ]
