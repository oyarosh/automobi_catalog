# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0079_auto_20160415_1528'),
    ]

    operations = [
        migrations.AddField(
            model_name='carbrand',
            name='end_date',
            field=models.DateField(null=True, verbose_name='\u0413\u043e\u0434 \u043a\u043e\u043d\u0446\u0430 \u0431\u0440\u0435\u043d\u0434\u0430', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='carbrand',
            name='start_date',
            field=models.DateField(null=True, verbose_name='\u0413\u043e\u0434 \u043d\u0430\u0447\u0430\u043b\u0430 \u0431\u0440\u0435\u043d\u0434\u0430', blank=True),
            preserve_default=True,
        ),
    ]
