# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

import os
import json

from django.conf import settings
from django.core.exceptions import MultipleObjectsReturned


def add_general_title_to_carmodelcountry(apps, schema_editor):
    CarModelCountry = apps.get_model("catalog", "CarModelCountry")
    CarText = apps.get_model("catalog", "CarText")

    catalog_dir = os.path.dirname(os.path.dirname(__file__))
    BASE_DIR = catalog_dir.replace('/apps/catalog', '')

    file_path = BASE_DIR + settings.STATIC_URL + 'json/car_model_country_general_title.json'

    with open(file_path) as data_file:
        data = json.load(data_file)

    for pk, general_title in data.iteritems():
        pk = pk.strip()
        general_title = general_title.strip()

        obj = CarModelCountry.objects.filter(id=pk)

        if not obj:
            continue

        obj = obj.first()

        try:
            car_text = CarText.objects.get(cartext=general_title)
        except CarText.DoesNotExist:
            car_text = CarText.objects.create(cartexttype_id=1, cartext=general_title)
        except MultipleObjectsReturned:
            car_text = CarText.objects.filter(cartext=general_title).first()

        obj.general_title = car_text
        obj.save(update_fields=['general_title', ])
        print obj.id


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0076_carmodelcountry_general_title'),
    ]

    operations = [
        migrations.RunPython(add_general_title_to_carmodelcountry),
    ]
