# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def fill_car_model_country_for_ukraine(apps, schema_editor):
    CarModelCountry = apps.get_model("catalog", "CarModelCountry")
    print 'Start fill_car_model_country_for_ukraine'
    for car_model_country in CarModelCountry.objects.filter(country_id=225):
        world_qs = CarModelCountry.objects.filter(country_id=251)
        result_qs = world_qs.filter(
            title=car_model_country.title,
            car_model=car_model_country.car_model)
        if result_qs.count() == 1:
            car_model_country.general_title = result_qs[0].general_title
            car_model_country.save(update_fields=['general_title', ])
        else:
            print '%s -- %s' % (car_model_country.id, car_model_country.title)
    print 'Finish fill_car_model_country_for_ukraine'


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0081_auto_20160415_2358'),
    ]

    operations = [
        migrations.RunPython(fill_car_model_country_for_ukraine),
    ]
