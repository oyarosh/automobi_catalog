# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0016_auto_20150303_1827'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='partdescriptiontext',
            unique_together=set([('part_description', 'language')]),
        ),
    ]
