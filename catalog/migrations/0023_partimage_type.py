# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0022_auto_20150507_1612'),
    ]

    operations = [
        migrations.AddField(
            model_name='partimage',
            name='type',
            field=models.CharField(max_length=16, null=True, verbose_name='\u0422\u0438\u043f', blank=True),
            preserve_default=True,
        ),
    ]
