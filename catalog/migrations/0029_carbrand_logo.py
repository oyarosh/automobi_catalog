# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0028_vinhistory'),
    ]

    operations = [
        migrations.AddField(
            model_name='carbrand',
            name='logo',
            field=models.ImageField(upload_to=b'car_brand_logos', null=True, verbose_name='\u0418\u043a\u043e\u043d\u043a\u0430', blank=True),
            preserve_default=True,
        ),
    ]
