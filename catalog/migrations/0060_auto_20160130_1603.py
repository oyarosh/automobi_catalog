# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import cabinet.mixins


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0059_acproperty_order'),
    ]

    operations = [
        migrations.AlterField(
            model_name='partimage',
            name='part',
            field=cabinet.mixins.ApiForeignKey(verbose_name='\u0417\u0430\u043f\u0447\u0430\u0441\u0442\u044c', to='catalog.Part'),
        ),
    ]
