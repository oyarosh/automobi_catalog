# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0015_auto_20150216_1612'),
    ]

    operations = [
        migrations.AlterField(
            model_name='trademark',
            name='show_name',
            field=models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0422\u043e\u0440\u0433\u043e\u0432\u043e\u0439 \u041c\u0430\u0440\u043a\u0438', db_index=True),
        ),
    ]
