# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import cabinet.mixins


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0088_laximocross_original_part'),
    ]

    operations = [
        migrations.AddField(
            model_name='laximocross',
            name='original_cross',
            field=cabinet.mixins.ApiForeignKey(related_name=b'original_cross', verbose_name='\u041a\u0440\u043e\u0441\u0441 \u0430\u0432\u0442\u043e\u043c\u043e\u0431\u0438', blank=True, to='catalog.Part', null=True),
            preserve_default=True,
        ),
    ]
