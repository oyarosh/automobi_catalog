# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import cabinet.mixins
import mptt.fields
import catalog.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AC',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
                ('parent', mptt.fields.TreeForeignKey(related_name=b'children', verbose_name='\u0420\u043e\u0434\u0438\u0442\u0435\u043b\u044c', blank=True, to='catalog.AC', null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ACProperty',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('acs', models.ManyToManyField(to='catalog.AC', verbose_name='\u041c\u0435\u043d\u044e \u043a\u0430\u0442\u0430\u043b\u043e\u0433\u0430')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ACPropertyValue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('ac_property', models.ForeignKey(verbose_name='\u0421\u0432\u043e\u0439\u0441\u0442\u0432\u043e \u043a\u0430\u0442\u0430\u043b\u043e\u0433\u0430', to='catalog.ACProperty')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Barcode',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=255, verbose_name='\u0428\u0442\u0440\u0438\u0445 \u043a\u043e\u0434')),
            ],
            options={
            },
            bases=(cabinet.mixins.SerializationMixin, models.Model),
        ),
        migrations.CreateModel(
            name='CarBrand',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('is_passenger', models.BooleanField(default=False, verbose_name='\u041f\u0430\u0441\u0441\u0430\u0436\u0438\u0440\u0441\u043a\u0438\u0439')),
                ('is_commercial', models.BooleanField(default=False, verbose_name='\u041a\u043e\u043c\u043c\u0435\u0440\u0447\u0435\u0441\u043a\u0438\u0439')),
                ('is_axis_manufacturer', models.BooleanField(default=False, verbose_name='\u041e\u0441\u044c')),
            ],
            options={
                'verbose_name': '\u043c\u0430\u0440\u043a\u0430 \u0438 \u0441\u0435\u0433\u043c\u0435\u043d\u0442 \u0430\u0432\u0442\u043e\u043c\u043e\u0431\u0438\u043b\u044f',
                'verbose_name_plural': '\u041c\u0430\u0440\u043a\u0438 \u0438 \u0441\u0435\u0433\u043c\u0435\u043d\u0442\u044b \u0430\u0432\u0442\u043e\u043c\u043e\u0431\u0438\u043b\u044f',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CarModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('car_type_shot_key', models.CharField(max_length=255, verbose_name='\u0422\u0438\u043f \u0410\u0432\u0442\u043e')),
                ('tcartype', models.CharField(max_length=255, null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('startdate_production', models.DateField(null=True, verbose_name='\u0414\u0430\u0442\u0430                                             \u041d\u0430\u0447\u0430\u043b\u0430 \u041f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0441\u0442\u0432\u0430', blank=True)),
                ('enddate_production', models.DateField(null=True, verbose_name='\u0414\u0430\u0442\u0430                                           \u041e\u043a\u043e\u043d\u0447\u0430\u043d\u0438\u044f \u041f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0441\u0442\u0432\u0430', blank=True)),
                ('is_passenger', models.BooleanField(default=False, verbose_name='\u041f\u0430\u0441\u0441\u0430\u0436\u0438\u0440\u0441\u043a\u0438\u0439')),
                ('is_commercial', models.BooleanField(default=False, verbose_name='\u041a\u043e\u043c\u043c\u0435\u0440\u0447\u0435\u0441\u043a\u0438\u0439')),
                ('is_axis_manufacturer', models.BooleanField(default=False, verbose_name='\u041e\u0441\u044c')),
                ('car_brand', models.ForeignKey(verbose_name='\u041c\u0430\u0440\u043a\u0430 \u0430\u0432\u0442\u043e\u043c\u043e\u0431\u0438\u043b\u044f', to='catalog.CarBrand')),
            ],
            options={
                'verbose_name': '\u043c\u043e\u0434\u0435\u043b\u044c \u0430\u0432\u0442\u043e\u043c\u043e\u0431\u0438\u043b\u044f',
                'verbose_name_plural': '\u041c\u043e\u0434\u0435\u043b\u0438 \u0430\u0432\u0442\u043e\u043c\u043e\u0431\u0438\u043b\u0435\u0439',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CarModification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('show_name', models.CharField(max_length=255, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u041c\u043e\u0434\u0438\u0444\u0438\u043a\u0430\u0446\u0438\u0438')),
                ('full_name', models.CharField(max_length=255, verbose_name='\u041f\u043e\u043b\u043d\u043e\u0435 \u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435')),
                ('startdate_production', models.DateField(null=True, verbose_name='\u0414\u0430\u0442\u0430                                             \u041d\u0430\u0447\u0430\u043b\u0430 \u041f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0441\u0442\u0432\u0430', blank=True)),
                ('enddate_production', models.DateField(null=True, verbose_name='\u0414\u0430\u0442\u0430                                           \u041e\u043a\u043e\u043d\u0447\u0430\u043d\u0438\u044f \u041f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0441\u0442\u0432\u0430', blank=True)),
                ('kw_engine_power', models.PositiveIntegerField(null=True, verbose_name='\u041c\u043e\u0449\u043d\u043e\u0441\u0442\u044c \u0414\u0432\u0438\u0433\u0430\u0442\u0435\u043b\u044f (kW)', blank=True)),
                ('hp_engine_power', models.PositiveIntegerField(null=True, verbose_name='\u041c\u043e\u0449\u043d\u043e\u0441\u0442\u044c \u0414\u0432\u0438\u0433\u0430\u0442\u0435\u043b\u044f \u0432 (HP)', blank=True)),
                ('engine_volume', models.PositiveIntegerField(null=True, verbose_name='\u041e\u0431\u044a\u0435\u043c \u0414\u0432\u0438\u0433\u0430\u0442\u0435\u043b\u044f (\u0441\u043c3)', blank=True)),
                ('cylinders_num', models.PositiveIntegerField(null=True, verbose_name='\u041a\u043e\u043b-\u0432\u043e \u0426\u0438\u043b\u0438\u043d\u0434\u0440\u043e\u0432', blank=True)),
                ('doors_num', models.PositiveIntegerField(null=True, verbose_name='\u041a\u043e\u043b-\u0432\u043e \u0414\u0432\u0435\u0440\u0435\u0439', blank=True)),
                ('tank_volume', models.PositiveIntegerField(null=True, verbose_name='\u041e\u0431\u044a\u0435\u043c \u0411\u0430\u043a\u0430 (\u043b)', blank=True)),
                ('valves_per_cylinder', models.PositiveIntegerField(null=True, verbose_name='\u041a\u043b\u0430\u043f\u0430\u043d\u043e\u0432 \u043d\u0430 \u0426\u0438\u043b\u0438\u043d\u0434\u0440', blank=True)),
            ],
            options={
                'verbose_name': '\u043c\u043e\u0434\u0438\u0444\u0438\u043a\u0430\u0446\u0438\u044f \u0430\u0432\u0442\u043e\u043c\u043e\u0431\u0438\u043b\u044f',
                'verbose_name_plural': '\u041c\u043e\u0434\u0438\u0444\u0438\u043a\u0430\u0446\u0438\u0438 \u0430\u0432\u0442\u043e\u043c\u043e\u0431\u0438\u043b\u0435\u0439',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CarText',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cartext', models.CharField(max_length=255, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435')),
            ],
            options={
                'verbose_name': '\u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0441\u0432\u043e\u0439\u0441\u0442\u0432\u0430 \u0434\u043b\u044f \u0430\u0432\u0442\u043e\u043c\u043e\u0431\u0438\u043b\u044f',
                'verbose_name_plural': '\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u044f \u0441\u0432\u043e\u0439\u0441\u0442\u0432 \u0434\u043b\u044f \u0430\u0432\u0442\u043e\u043c\u043e\u0431\u0438\u043b\u044f',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CarTextType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cartexttype', models.CharField(max_length=255, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0442\u0438\u043f\u0430 \u0441\u0432\u043e\u0439\u0441\u0442\u0432\u0430')),
            ],
            options={
                'verbose_name': '\u0442\u0438\u043f \u0441\u0432\u043e\u0439\u0441\u0442\u0432\u0430 \u0430\u0432\u0442\u043e\u043c\u043e\u0431\u0438\u043b\u044f',
                'verbose_name_plural': '\u0422\u0438\u043f\u044b \u0441\u0432\u043e\u0439\u0441\u0442\u0432\u0430 \u0430\u0432\u0442\u043e\u043c\u043e\u0431\u0438\u043b\u044f',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CarTree',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
                ('parent', mptt.fields.TreeForeignKey(related_name=b'children', blank=True, to='catalog.CarTree', null=True)),
            ],
            options={
                'verbose_name': '\u0443\u0441\u0442\u0440\u043e\u0439\u0441\u0442\u0432\u043e \u0430\u0432\u0442\u043e\u043c\u043e\u0431\u0438\u043b\u044f (\u0434\u0435\u0440\u0435\u0432\u043e)',
                'verbose_name_plural': '\u0423\u0441\u0442\u0440\u043e\u0439\u0441\u0442\u0432\u043e \u0410\u0432\u0442\u043e\u043c\u043e\u0431\u0438\u043b\u044f (\u0434\u0435\u0440\u0435\u0432\u043e)',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CatalogGood',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0442\u043e\u0432\u0430\u0440\u0430')),
                ('description', models.CharField(max_length=255, null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
            ],
            options={
                'verbose_name': '\u0422\u043e\u0432\u0430\u0440',
                'verbose_name_plural': '\u0422\u043e\u0432\u0430\u0440\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Cross',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_checked', models.BooleanField(default=False, verbose_name='\u041f\u0440\u043e\u0432\u0435\u0440\u0435\u043d')),
            ],
            options={
                'verbose_name': '\u043a\u0440\u043e\u0441\u0441 (\u0437\u0430\u043c\u0435\u043d\u0430)',
                'verbose_name_plural': '\u041a\u0440\u043e\u0441\u0441\u044b (\u0437\u0430\u043c\u0435\u043d\u044b)',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='EngineModification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('engcode', models.CharField(max_length=255, verbose_name='\u041a\u043e\u0434 \u0434\u0432\u0438\u0433\u0430\u0442\u0435\u043b\u044f')),
                ('setup_startdate', models.DateField(null=True, verbose_name='\u0414\u0430\u0442\u0430                                        \u041d\u0430\u0447\u0430\u043b\u0430 \u041f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0441\u0442\u0432\u0430', blank=True)),
                ('setup_enddate', models.DateField(null=True, verbose_name='\u0414\u0430\u0442\u0430                                      \u041e\u043a\u043e\u043d\u0447\u0430\u043d\u0438\u044f \u041f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0441\u0442\u0432\u0430', blank=True)),
                ('kw_power_from', models.PositiveIntegerField(null=True, verbose_name='\u041c\u043e\u0449\u043d\u043e\u0441\u0442\u044c \u043e\u0442 (kW)', blank=True)),
                ('kw_power_till', models.PositiveIntegerField(null=True, verbose_name='\u041c\u043e\u0449\u043d\u043e\u0441\u0442\u044c \u0434\u043e (kW)', blank=True)),
                ('hp_power_from', models.PositiveIntegerField(null=True, verbose_name='\u041c\u043e\u0449\u043d\u043e\u0441\u0442\u044c \u043e\u0442 (HP)', blank=True)),
                ('hp_power_till', models.PositiveIntegerField(null=True, verbose_name='\u041c\u043e\u0449\u043d\u043e\u0441\u0442\u044c \u0434\u043e (HP)', blank=True)),
                ('valves_num', models.PositiveIntegerField(null=True, verbose_name='\u041a\u043e\u043b-\u0432\u043e \u041a\u043b\u0430\u043f\u0430\u043d\u043e\u0432', blank=True)),
                ('cylinders_num', models.PositiveIntegerField(null=True, verbose_name='\u041a\u043e\u043b-\u0432\u043e \u0426\u0438\u043b\u0438\u043d\u0434\u0440\u043e\u0432', blank=True)),
                ('engine_volume', models.PositiveIntegerField(null=True, verbose_name='\u041e\u0431\u044a\u0435\u043c \u0414\u0432\u0438\u0433\u0430\u0442\u0435\u043b\u044f (\u0441\u043c3)', blank=True)),
                ('piston_diameter', models.PositiveIntegerField(null=True, verbose_name='\u0414\u0438\u0430\u043c\u0435\u0442\u0440 \u041f\u043e\u0440\u0448\u043d\u044f', blank=True)),
                ('stroke', models.PositiveIntegerField(null=True, verbose_name='\u0425\u043e\u0434 \u041f\u043e\u0440\u0448\u043d\u044f', blank=True)),
                ('crankshaft_number', models.PositiveIntegerField(null=True, verbose_name='\u041a\u043e\u043b-\u0432\u043e \u041f\u043e\u0434\u0448\u0438\u043f\u043d\u0438\u043a\u043e\u0432                                                     \u043d\u0430 \u041a\u043e\u043b\u0435\u043d\u0432\u0430\u043b\u0435', blank=True)),
                ('engremarks', models.CharField(max_length=255, null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
            ],
            options={
                'verbose_name': '\u043c\u043e\u0434\u0438\u0444\u0438\u043a\u0430\u0446\u0438\u044f \u043c\u043e\u0442\u043e\u0440\u0430',
                'verbose_name_plural': '\u041c\u043e\u0434\u0438\u0444\u0438\u043a\u0430\u0446\u0438\u0438 \u043c\u043e\u0442\u043e\u0440\u043e\u0432',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='EngineText',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('enginetext', models.CharField(max_length=255, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435')),
            ],
            options={
                'verbose_name': '\u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0441\u0432\u043e\u0439\u0441\u0442\u0432\u0430 \u0434\u043b\u044f \u0434\u0432\u0438\u0433\u0430\u0442\u0435\u043b\u044f',
                'verbose_name_plural': '\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u044f \u0441\u0432\u043e\u0439\u0441\u0442\u0432 \u0434\u043b\u044f \u0434\u0432\u0438\u0433\u0430\u0442\u0435\u043b\u044f',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='EngineTextType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('enginetexttype', models.CharField(max_length=255, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0442\u0438\u043f\u0430 \u0441\u0432\u043e\u0439\u0441\u0442\u0432\u0430 \u0434\u043b\u044f \u0434\u0432\u0438\u0433\u0430\u0442\u0435\u043b\u044f')),
            ],
            options={
                'verbose_name': '\u0442\u0438\u043f \u0441\u0432\u043e\u0439\u0441\u0442\u0432\u0430 \u0434\u043b\u044f \u0434\u0432\u0438\u0433\u0430\u0442\u0435\u043b\u044f',
                'verbose_name_plural': '\u0422\u0438\u043f\u044b \u0441\u0432\u043e\u0439\u0441\u0442\u0432\u0430 \u0434\u043b\u044f \u0434\u0432\u0438\u0433\u0430\u0442\u0435\u043b\u0435\u0439',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GoodsCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438')),
            ],
            options={
                'verbose_name': '\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f \u0442\u043e\u0432\u0430\u0440\u0430',
                'verbose_name_plural': '\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438 \u0442\u043e\u0432\u0430\u0440\u043e\u0432',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GoodsCategoryGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0433\u0440\u0443\u043f\u043f\u044b \u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0439')),
                ('icon', models.ImageField(upload_to=b'goods_category_group', null=True, verbose_name='\u0418\u043a\u043e\u043d\u043a\u0430 \u0433\u0440\u0443\u043f\u043f\u044b', blank=True)),
            ],
            options={
                'verbose_name': '\u0433\u0440\u0443\u043f\u043f\u0430 \u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0439 \u0442\u043e\u0432\u0430\u0440\u043e\u0432',
                'verbose_name_plural': '\u0413\u0440\u0443\u043f\u043f\u044b \u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0439 \u0442\u043e\u0432\u0430\u0440\u043e\u0432',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Part',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('search_number', models.CharField(max_length=255, verbose_name='\u041d\u043e\u043c\u0435\u0440 \u041f\u043e\u0438\u0441\u043a\u0430', db_index=True)),
                ('catalog_number', models.CharField(max_length=255, verbose_name='\u041a\u0430\u0442\u0430\u043b\u043e\u0436\u043d\u044b\u0439 \u041d\u043e\u043c\u0435\u0440', db_index=True)),
                ('description', models.CharField(max_length=255, null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('marks', models.CharField(max_length=255, null=True, verbose_name='\u041f\u0440\u0438\u043c\u0435\u0447\u0430\u043d\u0438\u0435', blank=True)),
                ('tecdoc', models.PositiveIntegerField(null=True, verbose_name='TecDoc', blank=True)),
                ('car_modification', models.ManyToManyField(to='catalog.CarModification', null=True, verbose_name='\u041c\u043e\u0434\u0438\u0444\u0438\u043a\u0430\u0446\u0438\u044f                                               \u0430\u0432\u0442\u043e\u043c\u043e\u0431\u0438\u043b\u044f', blank=True)),
                ('car_tree', models.ForeignKey(verbose_name='\u0423\u0437\u0435\u043b \u0434\u0435\u0440\u0435\u0432\u0430', blank=True, to='catalog.CarTree', null=True)),
            ],
            options={
                'verbose_name': '\u0437\u0430\u043f\u0447\u0430\u0441\u0442\u044c',
                'verbose_name_plural': '\u0417\u0430\u043f\u0447\u0430\u0441\u0442\u0438',
            },
            bases=(cabinet.mixins.SerializationMixin, models.Model),
        ),
        migrations.CreateModel(
            name='PartDescription',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.TextField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435')),
                ('language_id', models.PositiveIntegerField(null=True)),
                ('part', models.ForeignKey(to='catalog.Part', unique=True)),
            ],
            options={
            },
            bases=(cabinet.mixins.SerializationMixin, models.Model),
        ),
        migrations.CreateModel(
            name='PartEngine',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('engine', models.ForeignKey(verbose_name='\u0414\u0432\u0438\u0433\u0430\u0442\u0435\u043b\u044c', blank=True, to='catalog.EngineModification', null=True)),
                ('part', models.ForeignKey(verbose_name='\u0417\u0430\u043f\u0447\u0430\u0441\u0442\u044c', blank=True, to='catalog.Part', null=True)),
            ],
            options={
                'verbose_name': '\u043f\u0440\u0438\u043c\u0435\u043d\u0435\u043d\u0438\u0435 \u0437\u0430\u043f\u0447\u0430\u0441\u0442\u0438 \u0432 \u0434\u0432\u0438\u0433\u0430\u0442\u0435\u043b\u0435',
                'verbose_name_plural': '\u041f\u0440\u0438\u043c\u0435\u043d\u0435\u043d\u0438\u0435 \u0437\u0430\u043f\u0447\u0430\u0441\u0442\u0438 \u0432 \u0434\u0432\u0438\u0433\u0430\u0442\u0435\u043b\u0435',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PartImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(upload_to=catalog.models.partimage_upload_to, verbose_name='\u0424\u0430\u0439\u043b')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='\u0412\u0440\u0435\u043c\u044f \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('part', models.ForeignKey(verbose_name='\u0417\u0430\u043f\u0447\u0430\u0441\u0442\u044c', to='catalog.Part')),
            ],
            options={
                'verbose_name': '\u0424\u043e\u0442\u043e\u0433\u0440\u0430\u0444\u0438\u044f',
                'verbose_name_plural': '\u0424\u043e\u0442\u043e\u0433\u0440\u0430\u0444\u0438\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PartProperty',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=255, verbose_name='\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u0435 \u0425\u0430\u0440\u0430\u043a\u0442\u0435\u0440\u0438\u0441\u0442\u0438\u043a\u0438')),
            ],
            options={
                'verbose_name': '\u0445\u0430\u0440\u0430\u043a\u0442\u0435\u0440\u0438\u0441\u0442\u0438\u043a\u0430 \u0442\u043e\u0432\u0430\u0440\u0430',
                'verbose_name_plural': '\u0425\u0430\u0440\u0430\u043a\u0442\u0435\u0440\u0438\u0441\u0442\u0438\u043a\u0438 \u0442\u043e\u0432\u0430\u0440\u043e\u0432',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PartPropertyType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435')),
            ],
            options={
                'verbose_name': '\u0442\u0438\u043f \u0445\u0430\u0440\u0430\u043a\u0442\u0435\u0440\u0438\u0441\u0442\u0438\u043a \u0437\u0430\u043f\u0447\u0430\u0441\u0442\u0435\u0439',
                'verbose_name_plural': '\u0422\u0438\u043f\u044b \u0445\u0430\u0440\u0430\u043a\u0442\u0435\u0440\u0438\u0441\u0442\u0438\u043a \u0437\u0430\u043f\u0447\u0430\u0441\u0442\u0435\u0439',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PartSet',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('quantity', models.PositiveIntegerField(null=True, verbose_name='\u041a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e', blank=True)),
                ('car_tree', models.ForeignKey(verbose_name='\u0423\u0437\u0435\u043b \u0434\u0435\u0440\u0435\u0432\u0430', blank=True, to='catalog.CarTree', null=True)),
            ],
            options={
                'verbose_name': '\u043a\u043e\u043c\u043f\u043b\u0435\u043a\u0442',
                'verbose_name_plural': '\u041a\u043e\u043c\u043f\u043b\u0435\u043a\u0442\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TradeMark',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('show_name', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0422\u043e\u0440\u0433\u043e\u0432\u043e\u0439 \u041c\u0430\u0440\u043a\u0438')),
                ('full_name', models.CharField(max_length=255, verbose_name='\u041f\u043e\u043b\u043d\u043e\u0435 \u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
            ],
            options={
                'verbose_name': '\u0442\u043e\u0440\u0433\u043e\u0432\u0430\u044f \u043c\u0430\u0440\u043a\u0430',
                'verbose_name_plural': '\u0422\u043e\u0440\u0433\u043e\u0432\u044b\u0435 \u043c\u0430\u0440\u043a\u0438',
            },
            bases=(cabinet.mixins.SerializationMixin, models.Model),
        ),
        migrations.CreateModel(
            name='Unit',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435')),
            ],
            options={
                'verbose_name': '\u0435\u0434\u0438\u043d\u0438\u0446\u0430 \u0438\u0437\u043c\u0435\u0440\u0435\u043d\u0438\u044f',
                'verbose_name_plural': '\u0415\u0434\u0438\u043d\u0438\u0446\u044b \u0438\u0437\u043c\u0435\u0440\u0435\u043d\u0438\u044f',
            },
            bases=(cabinet.mixins.SerializationMixin, models.Model),
        ),
        migrations.CreateModel(
            name='WeightSizeSpecification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('catalog_number', models.CharField(db_index=True, max_length=255, verbose_name='\u041a\u0430\u0442\u0430\u043b\u043e\u0436\u043d\u044b\u0439 \u043d\u043e\u043c\u0435\u0440', blank=True)),
                ('weight', models.DecimalField(null=True, verbose_name='\u0412\u0435\u0441, \u043a\u0433.', max_digits=10, decimal_places=2, blank=True)),
                ('m3', models.DecimalField(null=True, verbose_name='\u0413\u0430\u0431\u0430\u0440\u0438\u0442\u044b, \u043c3', max_digits=10, decimal_places=2, blank=True)),
                ('length', models.DecimalField(null=True, verbose_name='\u0414\u043b\u0438\u043d\u0430, \u0441\u043c', max_digits=10, decimal_places=2, blank=True)),
                ('width', models.DecimalField(null=True, verbose_name='\u0428\u0438\u0440\u0438\u043d\u0430, \u0441\u043c', max_digits=10, decimal_places=2, blank=True)),
                ('height', models.DecimalField(null=True, verbose_name='\u0412\u044b\u0441\u043e\u0442\u0430, \u0441\u043c', max_digits=10, decimal_places=2, blank=True)),
                ('tm', models.ForeignKey(verbose_name='\u0422\u043e\u0440\u0433\u043e\u0432\u0430\u044f \u043c\u0430\u0440\u043a\u0430', to='catalog.TradeMark')),
                ('unit', models.ForeignKey(verbose_name='\u0415\u0434\u0438\u043d\u0438\u0446\u0430 \u0438\u0437\u043c\u0435\u0440\u0435\u043d\u0438\u044f \u0410\u0432\u0442\u043e\u043c\u043e\u0431\u0438', to='catalog.Unit')),
            ],
            options={
            },
            bases=(cabinet.mixins.SerializationMixin, models.Model),
        ),
        migrations.AlterIndexTogether(
            name='weightsizespecification',
            index_together=set([('catalog_number', 'tm')]),
        ),
        migrations.AddField(
            model_name='partpropertytype',
            name='unit',
            field=models.ForeignKey(verbose_name='\u0415\u0434\u0438\u043d\u0438\u0446\u0430 \u0438\u0437\u043c\u0435\u0440\u0435\u043d\u0438\u044f', blank=True, to='catalog.Unit', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='partproperty',
            name='part_property',
            field=models.ForeignKey(verbose_name='\u0422\u0438\u043f                                       \u0445\u0430\u0440\u0430\u043a\u0442\u0435\u0440\u0438\u0441\u0442\u0438\u043a\u0438 \u0437\u0430\u043f\u0447\u0430\u0441\u0442\u0438', blank=True, to='catalog.PartPropertyType', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='partengine',
            name='part_set',
            field=models.ForeignKey(verbose_name='\u041a\u043e\u043c\u043f\u043b\u0435\u043a\u0442', blank=True, to='catalog.PartSet', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='part',
            name='part_set',
            field=models.ManyToManyField(to='catalog.PartSet', null=True, verbose_name='\u041a\u043e\u043c\u043f\u043b\u0435\u043a\u0442', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='part',
            name='properties',
            field=models.ManyToManyField(to='catalog.PartProperty', null=True, verbose_name='\u0425\u0430\u0440\u0430\u043a\u0442\u0435\u0440\u0438\u0441\u0442\u0438\u043a\u0438', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='part',
            name='tm',
            field=models.ForeignKey(verbose_name='\u0422\u043e\u0440\u0433\u043e\u0432\u0430\u044f \u043c\u0430\u0440\u043a\u0430', to='catalog.TradeMark'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='goodscategory',
            name='categoty_group',
            field=models.ForeignKey(verbose_name='\u0413\u0440\u0443\u043f\u043f\u0430', blank=True, to='catalog.GoodsCategoryGroup', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='enginetext',
            name='enginetexttype',
            field=models.ForeignKey(verbose_name='\u0422\u0438\u043f \u0421\u0432\u043e\u0439\u0441\u0442\u0432\u0430                                        \u0434\u043b\u044f \u0434\u0432\u0438\u0433\u0430\u0442\u0435\u043b\u044f', to='catalog.EngineTextType'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='enginemodification',
            name='camdrive',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u0422\u0438\u043f \u041f\u0440\u0438\u0432\u043e\u0434\u0430 \u0420\u0430\u0441\u043f\u0440\u0435\u0434\u0432\u0430\u043b\u0430', blank=True, to='catalog.EngineText', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='enginemodification',
            name='camtype',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u0422\u0438\u043f \u0421\u0438\u0441\u0442\u0435\u043c\u044b \u0420\u0430\u0441\u043f\u0440\u0435\u0434\u0432\u0430\u043b\u0430', blank=True, to='catalog.EngineText', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='enginemodification',
            name='charge',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u0422\u0438\u043f \u041d\u0430\u0434\u0434\u0443\u0432\u0430', blank=True, to='catalog.EngineText', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='enginemodification',
            name='cooltype',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u0422\u0438\u043f \u0421\u0438\u0441\u0442\u0435\u043c\u044b \u041e\u0445\u043b\u0430\u0436\u0434\u0435\u043d\u0438\u044f', blank=True, to='catalog.EngineText', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='enginemodification',
            name='cylpos',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u0420\u0430\u0441\u043f\u043e\u043b\u043e\u0436\u0435\u043d\u0438\u0435 \u0426\u0438\u043b\u0438\u043d\u0434\u0440\u043e\u0432', blank=True, to='catalog.EngineText', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='enginemodification',
            name='econorm',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u042d\u043a\u043e\u043b\u043e\u0433\u0438\u0447\u0435\u0441\u043a\u0430\u044f \u041d\u043e\u0440\u043c\u0430', blank=True, to='catalog.EngineText', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='enginemodification',
            name='fuel',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u0422\u0438\u043f \u0422\u043e\u043f\u043b\u0438\u0432\u0430', blank=True, to='catalog.EngineText', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='enginemodification',
            name='fueltype',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u0422\u0438\u043f \u0422\u043e\u043f\u043b\u0438\u0432\u0430', blank=True, to='catalog.EngineText', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='enginemodification',
            name='injtype',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u0422\u0438\u043f \u041f\u043e\u0434\u0430\u0447\u0438 \u0422\u043e\u043f\u043b\u0438\u0432\u0430', blank=True, to='catalog.EngineText', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='enginemodification',
            name='tm',
            field=models.ForeignKey(verbose_name='\u0422\u043e\u0440\u0433\u043e\u0432\u0430\u044f \u043c\u0430\u0440\u043a\u0430', to='catalog.TradeMark'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='enginemodification',
            name='valvedrive',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u0422\u0438\u043f \u0423\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u0438\u044f \u041a\u043b\u0430\u043f\u0430\u043d\u0430\u043c\u0438', blank=True, to='catalog.EngineText', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='cross',
            name='cross',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u0417\u0430\u043c\u0435\u043d\u0438\u0442\u0435\u043b\u044c', to='catalog.Part'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='cross',
            name='part',
            field=models.ForeignKey(related_name=b'crosses', verbose_name='\u0417\u0430\u043f\u0447\u0430\u0441\u0442\u044c', to='catalog.Part'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='cataloggood',
            name='category',
            field=models.ForeignKey(verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f', to='catalog.GoodsCategory'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='cataloggood',
            name='tm',
            field=models.ForeignKey(verbose_name='\u0422\u043e\u0440\u0433\u043e\u0432\u0430\u044f \u043c\u0430\u0440\u043a\u0430', to='catalog.TradeMark'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='cartext',
            name='cartexttype',
            field=models.ForeignKey(verbose_name='\u0422\u0438\u043f \u0421\u0432\u043e\u0439\u0441\u0442\u0432\u0430 \u0410\u0432\u0442\u043e', to='catalog.CarTextType'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='carmodification',
            name='abs',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u0422\u0438\u043f ABS', blank=True, to='catalog.CarText', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='carmodification',
            name='asr',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u0422\u0438\u043f ASR', blank=True, to='catalog.CarText', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='carmodification',
            name='axle',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u0422\u0438\u043f \u041e\u0441\u0438', blank=True, to='catalog.CarText', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='carmodification',
            name='body',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u0422\u0438\u043f \u041a\u0443\u0437\u043e\u0432\u0430', blank=True, to='catalog.CarText', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='carmodification',
            name='brakesys',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u041a\u043e\u0434 \u0422\u043e\u0440\u043c\u043e\u0437\u043d\u043e\u0439 \u0421\u0438\u0441\u0442\u0435\u043c\u044b', blank=True, to='catalog.CarText', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='carmodification',
            name='braketype',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u0422\u0438\u043f \u0422\u043e\u0440\u043c\u043e\u0437\u043d\u043e\u0439 \u0421\u0438\u0441\u0442\u0435\u043c\u044b', blank=True, to='catalog.CarText', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='carmodification',
            name='car_model',
            field=models.ForeignKey(verbose_name='\u041c\u043e\u0434\u0435\u043b\u044c \u0430\u0432\u0442\u043e\u043c\u043e\u0431\u0438\u043b\u044f', to='catalog.CarModel'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='carmodification',
            name='catalyst',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u0422\u0438\u043f \u041a\u0430\u0442\u0430\u043b\u0438\u0437\u0430\u0442\u043e\u0440\u0430', blank=True, to='catalog.CarText', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='carmodification',
            name='chassis',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u0422\u0438\u043f \u0428\u0430\u0441\u0441\u0438', blank=True, to='catalog.CarText', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='carmodification',
            name='drive',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u041a\u043e\u0434 \u041f\u0440\u0438\u0432\u043e\u0434\u0430', blank=True, to='catalog.CarText', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='carmodification',
            name='engine',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u0422\u0438\u043f \u0414\u0432\u0438\u0433\u0430\u0442\u0435\u043b\u044f', blank=True, to='catalog.CarText', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='carmodification',
            name='engine_modifications',
            field=models.ManyToManyField(related_name=b'car_modifications', verbose_name='\u041c\u043e\u0434\u0438\u0444\u0438\u043a\u0430\u0446\u0438\u044f \u043c\u043e\u0442\u043e\u0440\u0430', to='catalog.EngineModification'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='carmodification',
            name='fuel',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u041a\u043e\u0434 \u0422\u043e\u043f\u043b\u0438\u0432\u0430', blank=True, to='catalog.CarText', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='carmodification',
            name='fuelin',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u041a\u043e\u0434 \u0421\u0438\u0441\u0442\u0435\u043c\u044b \u0412\u043f\u0440\u044b\u0441\u043a\u0430', blank=True, to='catalog.CarText', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='carmodification',
            name='transmission',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u041a\u043e\u0434 \u0422\u0440\u0430\u043d\u0441\u043c\u0438\u0441\u0441\u0438\u0438', blank=True, to='catalog.CarText', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='carmodification',
            name='voltage',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u041a\u043e\u0434 \u0412\u043e\u043b\u044c\u0442\u0430\u0436\u0430', blank=True, to='catalog.CarText', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='barcode',
            name='part',
            field=models.ForeignKey(verbose_name='\u0414\u0435\u0442\u0430\u043b\u044c', to='catalog.Part'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='barcode',
            name='wws',
            field=models.ForeignKey(verbose_name='\u0415\u0434\u0438\u043d\u0438\u0446\u0430 \u0438\u0437\u043c\u0435\u0440\u0435\u043d\u0438\u044f', to='catalog.WeightSizeSpecification'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='acpropertyvalue',
            name='parts',
            field=models.ManyToManyField(to='catalog.Part', verbose_name='\u0417\u0430\u043f\u0447\u0430\u0441\u0442\u0438', blank=True),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='acpropertyvalue',
            unique_together=set([('title', 'ac_property')]),
        ),
        migrations.AddField(
            model_name='ac',
            name='parts',
            field=models.ManyToManyField(to='catalog.Part', verbose_name='\u0417\u0430\u043f\u0447\u0430\u0441\u0442\u0438', blank=True),
            preserve_default=True,
        ),
    ]
