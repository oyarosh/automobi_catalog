# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0045_partinfo'),
    ]

    operations = [
        migrations.RunSQL(
            """
                UPDATE catalog_partcriteria
                SET value = text_value
                WHERE value IS NULL AND text_value IS NOT NULL;
            """),
    ]
