# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0042_auto_20150825_1400'),
    ]

    operations = [
        migrations.CreateModel(
            name='Criteria',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.CharField(max_length=255, null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('short_description', models.CharField(max_length=255, null=True, verbose_name='\u041a\u043e\u0440\u043e\u0442\u043a\u043e\u0435 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('unit_text', models.CharField(max_length=255, null=True, verbose_name='\u0415\u0434\u0438\u043d\u0438\u0446\u0430 \u0438\u0437\u043c\u0435\u0440\u0435\u043d\u0438\u044f', blank=True)),
                ('type', models.CharField(max_length=6, null=True, verbose_name='\u0422\u0438\u043f', blank=True)),
                ('is_interval', models.BooleanField(default=False, verbose_name='\u0418\u043d\u0442\u0435\u0440\u0432\u0430\u043b')),
                ('successor', models.ForeignKey(verbose_name='\u0412\u0442\u043e\u0440\u043e\u0439 \u043a\u0440\u0438\u0442\u0435\u0440\u0438\u0439', blank=True, to='catalog.Criteria', null=True)),
            ],
            options={
                'verbose_name': '\u043a\u0440\u0438\u0442\u0435\u0440\u0438\u0439',
                'verbose_name_plural': '\u041a\u0440\u0438\u0442\u0435\u0440\u0438\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PartCriteria',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('part_id', models.IntegerField(null=True, db_index=True)),
                ('value', models.CharField(max_length=255, null=True, verbose_name='\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u0435', blank=True)),
                ('text_value', models.CharField(max_length=255, null=True, verbose_name='\u0422\u0435\u043a\u0441\u0442\u043e\u0432\u043e\u0435 \u0437\u043d\u0430\u0447\u0435\u043d\u0438\u0435', blank=True)),
                ('is_shown', models.BooleanField(default=False, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u0441\u043f\u0438\u0441\u043a\u0435')),
                ('criteria', models.ForeignKey(verbose_name='\u041a\u0440\u0438\u0442\u0435\u0440\u0438\u0439', blank=True, to='catalog.Criteria', null=True)),
            ],
            options={
                'verbose_name': '\u0437\u043d\u0430\u0447\u0435\u043d\u0438\u0435 \u043a\u0440\u0438\u0442\u0435\u0440\u0438\u044f',
                'verbose_name_plural': '\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u044f \u043a\u0440\u0438\u0442\u0435\u0440\u0438\u0435\u0432',
            },
            bases=(models.Model,),
        ),
    ]
