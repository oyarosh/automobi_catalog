# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import catalog.models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0057_auto_20150922_1609'),
    ]

    operations = [
        migrations.CreateModel(
            name='CatalogUploadObject',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.CharField(default=b'wait', max_length=255, verbose_name='\u0421\u0442\u0430\u0442\u0443\u0441 \u0437\u0430\u0433\u0440\u0443\u0437\u043a\u0438', choices=[(b'wait', '\u041e\u0436\u0438\u0434\u0430\u0435\u0442 \u043e\u0431\u0440\u0430\u0431\u043e\u0442\u043a\u0438'), (b'proccessing', '\u041e\u0431\u0440\u0430\u0431\u0430\u0442\u044b\u0432\u0430\u0435\u0442\u0441\u044f'), (b'error', '\u041e\u0448\u0438\u0431\u043a\u0430'), (b'done', '\u041e\u0431\u0440\u0430\u0431\u043e\u0442\u0430\u043d')])),
                ('upload_file', models.FileField(upload_to=catalog.models.get_upload_path_catalog, verbose_name='\u0424\u0430\u0439\u043b \u0434\u043b\u044f \u0437\u0430\u0433\u0440\u0443\u0437\u043a\u0438')),
                ('error_msg', models.TextField(null=True, verbose_name='\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u0435 \u043e\u0431 \u043e\u0448\u0438\u0431\u043a\u0435', blank=True)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'verbose_name': '\u0417\u0430\u0433\u0440\u0443\u0437\u043a\u0430 \u043d\u043e\u0432\u044b\u0445 \u0434\u0430\u043d\u043d\u044b\u0445 \u0432 \u043a\u0430\u0442\u0430\u043b\u043e\u0433',
                'verbose_name_plural': '\u0417\u0430\u0433\u0440\u0443\u0437\u043a\u0438 \u043d\u043e\u0432\u044b\u0445 \u0434\u0430\u043d\u043d\u044b\u0445 \u0432 \u043a\u0430\u0442\u0430\u043b\u043e\u0433',
            },
            bases=(models.Model,),
        ),
    ]
