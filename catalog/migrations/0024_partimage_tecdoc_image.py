# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0023_partimage_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='partimage',
            name='tecdoc_image',
            field=models.BooleanField(default=False, verbose_name='\u0418\u0437 \u0422\u0435\u043a\u0414\u043e\u043a\u0430'),
            preserve_default=True,
        ),
    ]
