# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0003_auto_20150207_2016'),
    ]

    operations = [
        migrations.AlterField(
            model_name='part',
            name='catalog_number',
            field=models.CharField(max_length=255, null=True, verbose_name='\u041a\u0430\u0442\u0430\u043b\u043e\u0436\u043d\u044b\u0439 \u041d\u043e\u043c\u0435\u0440', db_index=True),
        ),
        migrations.AlterField(
            model_name='part',
            name='search_number',
            field=models.CharField(max_length=255, null=True, verbose_name='\u041d\u043e\u043c\u0435\u0440 \u041f\u043e\u0438\u0441\u043a\u0430', db_index=True),
        ),
    ]
