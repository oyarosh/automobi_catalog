# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0010_part_car_tree'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='acpropertyvalue',
            unique_together=None,
        ),
    ]
