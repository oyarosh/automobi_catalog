# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0051_auto_20150908_1726'),
    ]

    operations = [
        migrations.AddField(
            model_name='cross',
            name='created',
            field=models.DateTimeField(auto_now_add=True, verbose_name='\u0412\u0440\u0435\u043c\u044f \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f', null=True),
            preserve_default=True,
        ),
    ]
