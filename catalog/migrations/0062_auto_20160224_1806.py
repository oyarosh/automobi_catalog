# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0060_auto_20160130_1603'),
    ]

    operations = [
        migrations.AddField(
            model_name='part',
            name='weight',
            field=models.DecimalField(decimal_places=3, max_length=255, max_digits=13, blank=True, null=True, verbose_name='\u0412\u0435\u0441, \u043a\u0433.'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='part',
            name='weight_volume',
            field=models.DecimalField(decimal_places=3, max_length=255, max_digits=13, blank=True, null=True, verbose_name='\u041e\u0431\u044a\u0435\u043c\u043d\u044b\u0439 \u0432\u0435\u0441, \u043a\u0433.'),
            preserve_default=True,
        ),
    ]
