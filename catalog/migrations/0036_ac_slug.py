# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0035_auto_20150728_1950'),
    ]

    operations = [
        migrations.AddField(
            model_name='ac',
            name='slug',
            field=models.SlugField(null=True, max_length=280, blank=True, unique=True, verbose_name='URL'),
            preserve_default=True,
        ),
    ]
