# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def remove_diplicates(apps, schema_editor):
    ACProperty = apps.get_model("catalog", "ACProperty")
    ACPropertyValue = apps.get_model("catalog", "ACPropertyValue")

    for unique in ACProperty.objects.distinct('title'):
        for non_unique in ACProperty.objects.filter(title=unique.title).exclude(id=unique.id):
            non_unique_acpropertyvalue_all = non_unique.acpropertyvalue_set.all()
            non_unique_acpropertyvalue_all.update(ac_property=unique)

            non_unique_acs = non_unique.acs.all()
            unique.acs.add(*non_unique_acs)

            non_unique.delete()

    for unique_value in ACPropertyValue.objects.distinct('title', 'ac_property'):
        for i, non_unique_value in enumerate(ACPropertyValue.objects \
                .filter(title=unique_value.title, ac_property=unique_value.ac_property) \
                .exclude(id=unique_value.id)):
            non_unique_value_parts_all = non_unique_value.parts.all()
            unique_value.parts.add(*non_unique_value_parts_all)

            non_unique_value.delete()


def noop(*args, **kwargs):
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0020_trademark_oe'),
    ]

    operations = [
        migrations.RunPython(remove_diplicates, reverse_code=noop),
    ]
