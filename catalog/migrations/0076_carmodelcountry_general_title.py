# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0075_auto_20160406_1645'),
    ]

    operations = [
        migrations.AddField(
            model_name='carmodelcountry',
            name='general_title',
            field=models.ForeignKey(related_name=b'+', verbose_name='\u041e\u0431\u043e\u0431\u0449\u0435\u043d\u043d\u043e\u0435 \u043d\u0430\u0437\u0432\u0430\u043d\u0438\u0435', blank=True, to='catalog.CarText', null=True),
            preserve_default=True,
        ),
    ]
