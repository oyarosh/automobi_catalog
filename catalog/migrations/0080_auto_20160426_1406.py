# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations

import os
from django.conf import settings


def update_tree_titles(apps, schema_editor):
    CarTree = apps.get_model("catalog", "CarTree")
    catalog_dir = os.path.dirname(os.path.dirname(__file__))
    BASE_DIR = catalog_dir.replace('/apps/catalog', '')

    file_path = BASE_DIR + settings.STATIC_URL + 'data_migrations/cartree_export_new.csv'

    file_data = open(file_path, 'r')
    file_data.seek(0)
    rownum = -1
    for line in file_data:
        rownum += 1
        if rownum == 0:
            continue

        line = line.replace('\n', '').replace('\r', '')
        row = line.split('\t')

        try:
            id = row[0]
            title = row[1]
            parent_id = row[2]
            image = row[3]
        except (IndexError, ValueError):
            continue

        try:
            car_tree = CarTree.objects.get(id=id)
        except (IndexError, ValueError, CarTree.DoesNotExist):
            continue

        if parent_id:
            try:
                CarTree.objects.get(id=parent_id)
            except (IndexError, ValueError, CarTree.DoesNotExist):
                continue
        print row

        car_tree.title = title
        car_tree.parent_id = parent_id
        car_tree.show_on_main = False
        car_tree.image = image
        car_tree.save()
        print car_tree


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0079_trademark_created_by'),
    ]

    operations = [
        migrations.RunPython(update_tree_titles),
    ]
