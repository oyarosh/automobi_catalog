# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0073_auto_20160331_1858'),
    ]

    operations = [
        migrations.AddField(
            model_name='cartree',
            name='image',
            field=models.ImageField(upload_to=b'car_tree', null=True, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='cartree',
            name='show_on_main',
            field=models.BooleanField(default=False, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u043d\u0430 \u0433\u043b\u0430\u0432\u043d\u043e\u0439 \u0434\u0435\u0440\u0435\u0432\u0430'),
            preserve_default=True,
        ),
    ]
