# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0025_auto_20150630_1803'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='part',
            name='car_tree',
        ),
        migrations.RemoveField(
            model_name='partset',
            name='car_tree',
        ),
    ]
