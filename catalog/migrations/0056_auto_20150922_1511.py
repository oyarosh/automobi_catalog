# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0055_linkgenericarticlepart_tecdoc_id'),
    ]

    operations = [
        migrations.RunSQL(
            """
                UPDATE catalog_linkgenericarticlepart
                SET tecdoc_id = part_id, part_id = catalog_part.id
                FROM catalog_part
                WHERE catalog_part.tecdoc = catalog_linkgenericarticlepart.part_id;
            """),
    ]
