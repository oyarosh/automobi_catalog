# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0029_carbrand_logo'),
    ]

    operations = [
        migrations.AddField(
            model_name='carbrand',
            name='is_shown',
            field=models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c'),
            preserve_default=True,
        ),
    ]
