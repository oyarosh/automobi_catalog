# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0001_initial'),
        ('catalog', '0064_auto_20160301_1448'),
    ]

    operations = [
        migrations.AddField(
            model_name='unit',
            name='created_by_content_type',
            field=models.ForeignKey(related_name=b'+', to='contenttypes.ContentType', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='unit',
            name='created_by_object_id',
            field=models.PositiveIntegerField(null=True),
            preserve_default=True,
        ),
    ]
