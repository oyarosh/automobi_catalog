# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0018_generatedpart_generatedtrademark'),
    ]

    operations = [
        migrations.AddField(
            model_name='generatedpart',
            name='timestamp',
            field=models.DateTimeField(default=datetime.datetime(2015, 4, 15, 18, 36, 10, 430922), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='generatedtrademark',
            name='timestamp',
            field=models.DateTimeField(default=datetime.datetime(2015, 4, 15, 18, 36, 16, 521169), auto_now_add=True),
            preserve_default=False,
        ),
    ]
