# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0067_carmodelcountry'),
    ]

    operations = [
        migrations.CreateModel(
            name='CarModificationCountry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041c\u043e\u0434\u0438\u0444\u0438\u043a\u0430\u0446\u0438\u0438 \u0410\u0432\u0442\u043e')),
                ('car_modification', models.ForeignKey(related_name=b'modification_country', to='catalog.CarModification')),
                ('country', models.ForeignKey(to='catalog.SalesCountry')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='carmodelcountry',
            name='car_model',
            field=models.ForeignKey(related_name=b'model_country', to='catalog.CarModel'),
        ),
    ]
