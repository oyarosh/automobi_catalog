# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations

import os
import json

from django.conf import settings


def add_img_to_carmodel(apps, schema_editor):
    CarModel = apps.get_model("catalog", "CarModel")

    catalog_dir = os.path.dirname(os.path.dirname(__file__))
    BASE_DIR = catalog_dir.replace('/apps/catalog', '')

    file_path = BASE_DIR + settings.STATIC_URL + 'json/car_model.json'

    with open(file_path) as data_file:
        data = json.load(data_file)

    for item in data:
        pk = item['pk ']
        prod_img = item['fields ']['prod_img ']

        if not prod_img:
            continue

        if prod_img == ' ':
            continue

        obj = CarModel.objects.filter(id=pk)

        if not obj:
            continue

        obj = obj.first()
        obj.img.name = prod_img.replace('existparse/images/', 'car_model_photo/')
        obj.save()
        print obj.id

    print 'Don`t forget about coping car_model_photo directory to media'


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0074_carmodel_img'),
    ]

    operations = [
        migrations.RunPython(add_img_to_carmodel),
    ]
