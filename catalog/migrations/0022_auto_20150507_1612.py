# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0021_auto_20150507_1605'),
    ]

    operations = [
        migrations.AlterField(
            model_name='acproperty',
            name='title',
            field=models.CharField(unique=True, max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435'),
        ),
        migrations.AlterUniqueTogether(
            name='acpropertyvalue',
            unique_together=set([('title', 'ac_property')]),
        ),
    ]
