# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0058_cataloguploadobject'),
    ]

    operations = [
        migrations.AddField(
            model_name='acproperty',
            name='order',
            field=models.IntegerField(default=10, null=True, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a \u0441\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0438'),
            preserve_default=True,
        ),
    ]
