# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def update_to_main_car_brand(apps, schema_editor):
    CarBrand = apps.get_model("catalog", "CarBrand")
    not_update_titles = ['VW', 'Acura', 'Alfa Romeo', 'Audi', 'BMW', 'Brilliance', 'Buick', 'BYD', 'Cadillac', 'Chery', 'Chevrolet', 'Chrysler', 'Citroen', 'Dacia', 'Daewoo', 'Daihatsu', 'Dodge', 'Fiat', 'Ford', 'Geely', 'Great Wall', 'Honda', 'Hummer', 'Hyundai', 'Infinity', 'Isuzu', 'Jaguar', 'Jeep', 'Kia', 'Lada', 'Lancia', 'Land Rover', 'Lexus', 'Lifan', 'Mazda', 'Mercedes-Benz', 'MG', 'Mini', 'Mitsubishi', 'Nissan', 'Opel', 'Peugeot', 'Pontiac', 'Porsche', 'Renault', 'Rover', 'Saab', 'Seat', 'Skoda', 'Smart', 'Ssangyong', 'Subaru', 'Suzuki', 'Toyota', 'Volvo', 'Volkswagen', 'ZAZ']
    CarBrand.objects.exclude(title__in=not_update_titles).update(to_main=False)

class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0072_carbrand_to_main'),
    ]

    operations = [
        migrations.RunPython(update_to_main_car_brand),
    ]

