# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0038_part_slug'),
    ]

    operations = [
        migrations.RunSQL(
            """
                CREATE OR REPLACE FUNCTION translit_str(p_string character varying)
                  RETURNS character varying AS
                $BODY$
                select 
                replace(
                replace(
                replace(
                replace(
                replace(
                replace(
                replace(
                replace(
                replace(
                translate(lower($1), 
                'абвгдеёзийклмнопрстуфхць', 'abvgdeezijklmnoprstufхc`'),
                'ж', 'zh'),
                'ч', 'ch'),
                'ш', 'sh'),
                'щ', 'shh'),
                'ъ', '``'),
                'ы', 'y`'),
                'э', 'e`'),
                'ю', 'yu'),
                'я', 'ya');
                $BODY$
                  LANGUAGE sql IMMUTABLE
                  COST 100;
                UPDATE
                    catalog_part part1
                SET slug = lower(regexp_replace(regexp_replace(translit_str(part2.search_number || ' ' || tm.show_name || ' ' || COALESCE(pdt.value, part2.description)), '[_ ]', '-', 'g'), '[^a-zA-Z0-9\-]', '', 'g')) || '-' || part2.id
                FROM catalog_part part2
                LEFT JOIN catalog_trademark tm on tm.id = part2.tm_id
                LEFT JOIN catalog_partdescriptiontext pdt ON pdt.part_description_id = part2.default_description_id AND pdt.language_id = 16
                WHERE part2.id = part1.id;
            """),
    ]
