# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0008_cartree_node_id'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='part',
            name='car_tree',
        ),
    ]
