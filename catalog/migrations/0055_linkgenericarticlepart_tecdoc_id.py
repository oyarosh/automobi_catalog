# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0054_auto_20150921_1346'),
    ]

    operations = [
        migrations.AddField(
            model_name='linkgenericarticlepart',
            name='tecdoc_id',
            field=models.IntegerField(null=True, db_index=True),
            preserve_default=True,
        ),
    ]
