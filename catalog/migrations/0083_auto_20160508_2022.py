# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0082_auto_20160429_1909'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='trademark',
            name='created_by',
        ),
        migrations.AddField(
            model_name='trademark',
            name='created_by_id',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
