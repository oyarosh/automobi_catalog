# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0043_criteria_partcriteria'),
    ]

    operations = [
        migrations.CreateModel(
            name='ModificationSalesCountry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('modification_id', models.IntegerField(null=True, db_index=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SalesCountry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=255, null=True, verbose_name='\u041a\u043e\u0434 \u0441\u0442\u0440\u0430\u043d\u044b (3-\u0445 \u0431\u0443\u043a\u0432\u0435\u043d\u043d\u044b\u0439)', blank=True)),
                ('name', models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0441\u0442\u0440\u0430\u043d\u044b', blank=True)),
                ('currency_code', models.CharField(max_length=255, null=True, verbose_name='\u041a\u043e\u0434 \u0432\u0430\u043b\u044e\u0442\u044b', blank=True)),
                ('iso2', models.CharField(max_length=255, null=True, verbose_name='\u041a\u043e\u0434 \u0441\u0442\u0440\u0430\u043d\u044b (2-\u0445 \u0431\u0443\u043a\u0432\u0435\u043d\u043d\u044b\u0439)', blank=True)),
                ('is_group', models.BooleanField(default=False, verbose_name='\u0413\u0440\u0443\u043f\u043f\u0430 \u0441\u0442\u0440\u0430\u043d')),
            ],
            options={
                'verbose_name': '\u0441\u0442\u0440\u0430\u043d\u0430',
                'verbose_name_plural': '\u0421\u0442\u0440\u0430\u043d\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='modificationsalescountry',
            name='sales_country',
            field=models.ForeignKey(verbose_name='\u0441\u0442\u0440\u0430\u043d\u0430', blank=True, to='catalog.SalesCountry', null=True),
            preserve_default=True,
        ),
    ]
