# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0084_auto_20160531_2059'),
    ]

    operations = [
        migrations.AddField(
            model_name='part',
            name='img_quantity',
            field=models.PositiveIntegerField(default=0, null=True, verbose_name=b'Image quantity', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='carmodel',
            name='enddate_production',
            field=models.DateField(null=True, verbose_name='\u0414\u0430\u0442\u0430 \u041e\u043a\u043e\u043d\u0447\u0430\u043d\u0438\u044f \u041f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0441\u0442\u0432\u0430', blank=True),
        ),
        migrations.AlterField(
            model_name='carmodel',
            name='startdate_production',
            field=models.DateField(null=True, verbose_name='\u0414\u0430\u0442\u0430 \u041d\u0430\u0447\u0430\u043b\u0430 \u041f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0441\u0442\u0432\u0430', blank=True),
        ),
        migrations.AlterField(
            model_name='carmodification',
            name='enddate_production',
            field=models.DateField(null=True, verbose_name='\u0414\u0430\u0442\u0430 \u041e\u043a\u043e\u043d\u0447\u0430\u043d\u0438\u044f \u041f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0441\u0442\u0432\u0430', blank=True),
        ),
        migrations.AlterField(
            model_name='carmodification',
            name='startdate_production',
            field=models.DateField(null=True, verbose_name='\u0414\u0430\u0442\u0430 \u041d\u0430\u0447\u0430\u043b\u0430 \u041f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0441\u0442\u0432\u0430', blank=True),
        ),
        migrations.AlterField(
            model_name='enginemodification',
            name='crankshaft_number',
            field=models.PositiveIntegerField(null=True, verbose_name='\u041a\u043e\u043b-\u0432\u043e \u041f\u043e\u0434\u0448\u0438\u043f\u043d\u0438\u043a\u043e\u0432 \u043d\u0430 \u041a\u043e\u043b\u0435\u043d\u0432\u0430\u043b\u0435', blank=True),
        ),
        migrations.AlterField(
            model_name='enginemodification',
            name='setup_enddate',
            field=models.DateField(null=True, verbose_name='\u0414\u0430\u0442\u0430 \u041e\u043a\u043e\u043d\u0447\u0430\u043d\u0438\u044f \u041f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0441\u0442\u0432\u0430', blank=True),
        ),
        migrations.AlterField(
            model_name='enginemodification',
            name='setup_startdate',
            field=models.DateField(null=True, verbose_name='\u0414\u0430\u0442\u0430 \u041d\u0430\u0447\u0430\u043b\u0430 \u041f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0441\u0442\u0432\u0430', blank=True),
        ),
        migrations.AlterField(
            model_name='enginetext',
            name='enginetexttype',
            field=models.ForeignKey(verbose_name='\u0422\u0438\u043f \u0421\u0432\u043e\u0439\u0441\u0442\u0432\u0430 \u0434\u043b\u044f \u0434\u0432\u0438\u0433\u0430\u0442\u0435\u043b\u044f', to='catalog.EngineTextType'),
        ),
        migrations.AlterField(
            model_name='part',
            name='car_modification',
            field=models.ManyToManyField(to=b'catalog.CarModification', null=True, verbose_name='\u041c\u043e\u0434\u0438\u0444\u0438\u043a\u0430\u0446\u0438\u044f \u0430\u0432\u0442\u043e\u043c\u043e\u0431\u0438\u043b\u044f', blank=True),
        ),
        migrations.AlterField(
            model_name='partproperty',
            name='part_property',
            field=models.ForeignKey(verbose_name='\u0422\u0438\u043f \u0445\u0430\u0440\u0430\u043a\u0442\u0435\u0440\u0438\u0441\u0442\u0438\u043a\u0438 \u0437\u0430\u043f\u0447\u0430\u0441\u0442\u0438', blank=True, to='catalog.PartPropertyType', null=True),
        ),
    ]
