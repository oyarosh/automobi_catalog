# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0056_auto_20150922_1511'),
    ]

    operations = [
        migrations.RunSQL(
            """
                DELETE
                FROM catalog_linkgenericarticlepart
                WHERE tecdoc_id IS NULL;
            """),
    ]
