# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0013_auto_20150213_1807'),
    ]

    operations = [
        migrations.AddField(
            model_name='trademark',
            name='short_name',
            field=models.CharField(max_length=255, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='partdescriptiontext',
            name='part_description',
            field=models.ForeignKey(related_name=b'texts', to='catalog.PartDescription'),
        ),
    ]
