# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_language'),
        ('catalog', '0006_auto_20150209_2135'),
    ]

    operations = [
        migrations.AddField(
            model_name='unit',
            name='language',
            field=models.ForeignKey(default=0, to='core.Language'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='unit',
            name='tec_doc_id',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
    ]
