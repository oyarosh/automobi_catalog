# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0004_auto_20150209_1342'),
    ]

    operations = [
        migrations.AddField(
            model_name='trademark',
            name='tec_doc_id',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
    ]
