# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0074_auto_20160406_1438'),
    ]

    operations = [
        migrations.RunSQL("""
            UPDATE catalog_cartree SET show_on_main = True
            WHERE parent_id = 10001
            OR id IN (
                SELECT query.id FROM (
                    SELECT id, row_number() OVER (PARTITION BY parent_id ORDER BY title) as n
                    FROM catalog_cartree WHERE parent_id IN (SELECT id FROM catalog_cartree WHERE parent_id = 10001)
                ) AS query
                WHERE query.n < 8
            )
        """),
    ]
