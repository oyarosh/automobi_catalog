# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_language'),
        ('catalog', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='partdescription',
            name='language_id',
        ),
        migrations.AddField(
            model_name='partdescription',
            name='language',
            field=models.ForeignKey(default=1, to='core.Language'),
            preserve_default=False,
        ),
    ]
