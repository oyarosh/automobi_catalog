# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def add_general_title_to_carmodelcountry3(apps, schema_editor):
    CarBrand = apps.get_model('catalog', 'CarBrand')
    citroen = CarBrand.objects.get(title='Citroen')
    citroen.logo.name = 'car_brand_logos/citroen.jpg'
    citroen.save()
    changan = CarBrand.objects.get(title='Changan (Chana)')
    changan.logo.name = 'car_brand_logos/changan.jpg'
    changan.save()
    car_brand = CarBrand.objects.get(title='Daewoo')
    car_brand.logo.name = 'car_brand_logos/dewoo.jpg'
    car_brand.save()
    car_brand = CarBrand.objects.get(title='Ducati Motorcycles')
    car_brand.logo.name = 'car_brand_logos/ducati.jpg'
    car_brand.save()
    car_brand = CarBrand.objects.get(title='Infiniti')
    car_brand.logo.name = 'car_brand_logos/infinity.jpg'
    car_brand.save()
    car_brand = CarBrand.objects.get(title='Jawa Motorcycles')
    car_brand.logo.name = 'car_brand_logos/jawa.jpg'
    car_brand.save()
    car_brand = CarBrand.objects.get(title='Lancia')
    car_brand.logo.name = 'car_brand_logos/lanchia.jpg'
    car_brand.save()
    car_brand = CarBrand.objects.get(title='BMW Motorcycles')
    car_brand.logo.name = 'car_brand_logos/new-bmw-motorcycles.jpg'
    car_brand.save()
    car_brand = CarBrand.objects.get(title='Caterpillar')
    car_brand.logo.name = 'car_brand_logos/new-caterpillar.jpg'
    car_brand.save()
    car_brand = CarBrand.objects.get(title='Ikarus')
    car_brand.logo.name = 'car_brand_logos/new-ikarus.jpg'
    car_brand.save()
    car_brand = CarBrand.objects.get(title='John Deere')
    car_brand.logo.name = 'car_brand_logos/new-john-deere.jpg'
    car_brand.save()
    car_brand = CarBrand.objects.get(title='Kamaz')
    car_brand.logo.name = 'car_brand_logos/new-kamaz.jpg'
    car_brand.save()
    car_brand = CarBrand.objects.get(title='Man')
    car_brand.logo.name = 'car_brand_logos/new-man.jpg'
    car_brand.save()
    car_brand = CarBrand.objects.get(title='Maz')
    car_brand.logo.name = 'car_brand_logos/new-maz.jpg'
    car_brand.save()
    car_brand = CarBrand.objects.get(title='Neoplan')
    car_brand.logo.name = 'car_brand_logos/new-neoplan.jpg'
    car_brand.save()
    car_brand = CarBrand.objects.get(title='Scania')
    car_brand.logo.name = 'car_brand_logos/new-scania.jpg'
    car_brand.save()
    car_brand = CarBrand.objects.get(title='Tatra')
    car_brand.logo.name = 'car_brand_logos/new-tatra.jpg'
    car_brand.save()
    car_brand = CarBrand.objects.get(title='Rolls-Royce')
    car_brand.logo.name = 'car_brand_logos/rolls royce.jpg'
    car_brand.save()
    car_brand = CarBrand.objects.get(title='Ssangyong')
    car_brand.logo.name = 'car_brand_logos/ssang yong.jpg'
    car_brand.save()
    car_brand = CarBrand.objects.get(title='Suzuki Motorcycles')
    car_brand.logo.name = 'car_brand_logos/suzuki motorcycles.jpg'
    car_brand.save()


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0083_auto_20160508_2022'),
    ]

    operations = [
        migrations.RunPython(add_general_title_to_carmodelcountry3),
    ]
