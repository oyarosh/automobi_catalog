# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import cabinet.mixins


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0085_auto_20160617_1340'),
    ]

    operations = [
        migrations.AlterField(
            model_name='part',
            name='tm',
            field=cabinet.mixins.ApiForeignKey(verbose_name='\u0422\u043e\u0440\u0433\u043e\u0432\u0430\u044f \u043c\u0430\u0440\u043a\u0430', to='catalog.TradeMark'),
        ),
    ]
