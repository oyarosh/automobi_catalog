# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


import os
from django.conf import settings
from catalog.utils import add_general_title_to_carmodelcountry_by_json


def add_general_title_to_carmodelcountry3(apps, schema_editor):
    catalog_dir = os.path.dirname(os.path.dirname(__file__))
    BASE_DIR = catalog_dir.replace('/apps/catalog', '')

    file_path = BASE_DIR + settings.STATIC_URL + 'json/car_model_country_general_title_3.json'
    add_general_title_to_carmodelcountry_by_json(file_path)


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0082_auto_20160418_1856'),
    ]

    operations = [
        migrations.RunPython(add_general_title_to_carmodelcountry3),
    ]
