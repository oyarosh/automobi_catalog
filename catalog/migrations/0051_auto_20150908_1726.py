# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0050_auto_20150908_1725'),
    ]

    operations = [
        migrations.RunSQL(
            """
                DELETE FROM catalog_partcriteria WHERE id IN (
                    SELECT max_id
                    FROM
                    (SELECT catalog_part_id, criteria_id, value, COUNT(*), MAX(id) as max_id
                        FROM catalog_partcriteria
                        GROUP BY catalog_part_id, criteria_id, value
                        HAVING COUNT(*) > 1
                    ) as tty
                );
            """),
    ]
