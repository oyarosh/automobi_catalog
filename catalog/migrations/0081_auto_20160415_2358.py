# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def fill_start_and_end_year_carbrand(apps, schema_editor):
    CarBrand = apps.get_model("catalog", "CarBrand")
    CarModel = apps.get_model("catalog", "CarModel")
    for car_brand in CarBrand.objects.all():
        qs = CarModel.objects.filter(car_brand=car_brand)
        if qs.order_by('startdate_production'):
            car_brand.start_date = qs.order_by('startdate_production')[0].startdate_production
        if qs.order_by('-enddate_production'):
            car_brand.end_date = qs.order_by('-enddate_production')[0].enddate_production
        car_brand.save(update_fields=['start_date', 'end_date'])
        print car_brand.title


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0080_auto_20160415_2251'),
    ]

    operations = [
        migrations.RunPython(fill_start_and_end_year_carbrand),
    ]
