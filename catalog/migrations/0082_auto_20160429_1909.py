# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def nothing_important(apps, schema_editor):
    CarText = apps.get_model("catalog", "CarText")
    tmp = CarText.objects.get(id=110150)
    tmp.cartext = u'Гибрид'
    tmp.save()
    tmp2 = CarText.objects.get(id=14190)
    tmp2.cartext = u'с&nbspбортовой платформой'
    tmp2.save()


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0081_auto_20160427_1926'),
    ]

    operations = [
        migrations.RunPython(nothing_important),
    ]
