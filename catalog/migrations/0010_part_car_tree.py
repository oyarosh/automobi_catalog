# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0009_remove_part_car_tree'),
    ]

    operations = [
        migrations.AddField(
            model_name='part',
            name='car_tree',
            field=models.ManyToManyField(to='catalog.CarTree', null=True, verbose_name='\u0423\u0437\u043b\u044b \u0434\u0435\u0440\u0435\u0432\u0430', blank=True),
            preserve_default=True,
        ),
    ]
