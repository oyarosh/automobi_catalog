# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0053_partcriteria_car_modification_id'),
    ]

    operations = [
        migrations.CreateModel(
            name='PartCriteriaLA',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('link_generic_article_part_id', models.IntegerField(null=True, db_index=True)),
                ('value', models.CharField(max_length=255, null=True, verbose_name='\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u0435', blank=True)),
                ('is_shown', models.BooleanField(default=False, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u0441\u043f\u0438\u0441\u043a\u0435')),
                ('criteria', models.ForeignKey(verbose_name='\u041a\u0440\u0438\u0442\u0435\u0440\u0438\u0439', blank=True, to='catalog.Criteria', null=True)),
            ],
            options={
                'verbose_name': '\u0437\u043d\u0430\u0447\u0435\u043d\u0438\u0435 \u043a\u0440\u0438\u0442\u0435\u0440\u0438\u044f \u0434\u043b\u044f \u0433\u0440\u0443\u043f\u043f',
                'verbose_name_plural': '\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u044f \u043a\u0440\u0438\u0442\u0435\u0440\u0438\u0435\u0432 \u0434\u043b\u044f \u0433\u0440\u0443\u043f\u043f',
            },
            bases=(models.Model,),
        ),
        migrations.DeleteModel(
            name='PartInfo',
        ),
        migrations.RemoveField(
            model_name='partcriteria',
            name='car_modification_id',
        ),
    ]
