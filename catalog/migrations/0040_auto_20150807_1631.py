# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0039_auto_20150731_1635'),
    ]

    operations = [
        migrations.AddField(
            model_name='acproperty',
            name='show_by_count',
            field=models.IntegerField(default=8, null=True, verbose_name='\u041a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e \u0432\u044b\u0432\u043e\u0434\u0438\u043c\u044b\u0445 \u0437\u043d\u0430\u0447\u0435\u043d\u0438\u0439'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='acproperty',
            name='show_by_parts',
            field=models.IntegerField(default=20, null=True, verbose_name='\u041a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e \u0432\u044b\u0432\u043e\u0434\u0438\u043c\u044b\u0445 \u0437\u043d\u0430\u0447\u0435\u043d\u0438\u0439 \u043f\u043e \u043f\u0430\u0440\u0442\u0430\u043c'),
            preserve_default=True,
        ),
    ]
