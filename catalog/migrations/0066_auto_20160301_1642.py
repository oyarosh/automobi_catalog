# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from catalog.models import Unit
from users.models import DC
from core.models import Translation


def create_default_unit_for_dc(apps, schema_editor):
    for dc in DC.objects.all():
        unit = Unit.objects.filter(
            created_by_content_type=dc.get_content_type(),
            created_by_object_id=dc.id)
        if unit:
            continue

        translation = Translation()
        translation.save()

        data = {
            'created_by': dc,
            'title': u'шт',
            'code': u'2009',
            'full_name': u'Штука',
            'translation': translation
        }
        Unit.objects.create(**data)


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0065_auto_20160301_1553'),
    ]

    operations = [
        migrations.RunPython(create_default_unit_for_dc),
    ]
