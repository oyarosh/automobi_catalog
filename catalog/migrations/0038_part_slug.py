# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0037_auto_20150730_1218'),
    ]

    operations = [
        migrations.AddField(
            model_name='part',
            name='slug',
            field=models.SlugField(null=True, max_length=280, blank=True, unique=True, verbose_name='URL'),
            preserve_default=True,
        ),
    ]
