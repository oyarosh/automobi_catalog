# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0019_auto_20150415_1836'),
    ]

    operations = [
        migrations.AddField(
            model_name='trademark',
            name='oe',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
