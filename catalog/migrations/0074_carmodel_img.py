# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0073_auto_20160331_1858'),
    ]

    operations = [
        migrations.AddField(
            model_name='carmodel',
            name='img',
            field=models.ImageField(upload_to=b'car_model_photo', null=True, verbose_name='\u0424\u043e\u0442\u043e \u043c\u043e\u0434\u0435\u043b\u0438', blank=True),
            preserve_default=True,
        ),
    ]
