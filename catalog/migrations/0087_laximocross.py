# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0086_auto_20160629_1300'),
    ]

    operations = [
        migrations.CreateModel(
            name='LaximoCross',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('part_detailid', models.PositiveIntegerField()),
                ('part_oem', models.CharField(max_length=255, null=True, verbose_name='\u041e\u0440\u0438\u0433\u0438\u043d\u0430\u043b\u044c\u043d\u044b\u0439 \u043d\u043e\u043c\u0435\u0440', db_index=True)),
                ('part_formattedoem', models.CharField(max_length=255, null=True, verbose_name='\u041e\u0440\u0438\u0433\u0438\u043d\u0430\u043b\u044c\u043d\u044b\u0439 \u043f\u043e\u0438\u0441\u043a\u043e\u0432\u044b\u0439 \u043d\u043e\u043c\u0435\u0440', db_index=True)),
                ('part_manufacturer', models.CharField(max_length=255, null=True, verbose_name='\u0411\u0440\u0435\u043d\u0434 \u043e\u0440\u0438\u0433\u0438\u043d\u0430\u043b\u044c\u043d\u043e\u0433\u043e \u043d\u043e\u043c\u0435\u0440\u0430')),
                ('part_manufacturerid', models.PositiveIntegerField()),
                ('part_name', models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u0435')),
                ('part_weight', models.DecimalField(decimal_places=3, max_length=255, max_digits=13, blank=True, null=True, verbose_name='\u0412\u0435\u0441')),
                ('cross_detailid', models.PositiveIntegerField()),
                ('cross_oem', models.CharField(max_length=255, null=True, verbose_name='\u041d\u043e\u043c\u0435\u0440 \u0437\u0430\u043c\u0435\u043d\u044b', db_index=True)),
                ('cross_formattedoem', models.CharField(max_length=255, null=True, verbose_name='\u041f\u043e\u0438\u0441\u043a\u043e\u0432\u044b\u0439 \u043d\u043e\u043c\u0435\u0440 \u0437\u0430\u043c\u0435\u043d\u044b', db_index=True)),
                ('cross_manufacturer', models.CharField(max_length=255, null=True, verbose_name='\u0411\u0440\u0435\u043d\u0434 \u043d\u043e\u043c\u0435\u0440\u0430 \u0437\u0430\u043c\u0435\u043d\u044b')),
                ('cross_manufacturerid', models.PositiveIntegerField()),
                ('cross_name', models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u0435 \u0437\u0430\u043c\u0435\u043d\u044b')),
                ('cross_weight', models.DecimalField(decimal_places=3, max_length=255, max_digits=13, blank=True, null=True, verbose_name='\u0412\u0435\u0441 \u0437\u0430\u043c\u0435\u043d\u044b')),
                ('cross_rate', models.CharField(max_length=255, null=True, verbose_name='\u043d\u043e\u043c\u0435\u0440\u0430 \u0437\u0430\u043c\u0435\u043d\u044b - rate')),
                ('cross_type', models.CharField(max_length=255, null=True, verbose_name='\u043d\u043e\u043c\u0435\u0440\u0430 \u0437\u0430\u043c\u0435\u043d\u044b - type')),
                ('cross_way', models.CharField(max_length=255, null=True, verbose_name='\u043d\u043e\u043c\u0435\u0440\u0430 \u0437\u0430\u043c\u0435\u043d\u044b - way')),
                ('is_checked', models.BooleanField(default=False, verbose_name='\u041f\u0440\u043e\u0432\u0435\u0440\u0435\u043d')),
                ('is_copied', models.BooleanField(default=False, verbose_name='\u041f\u0440\u043e\u0432\u0435\u0440\u0435\u043d')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='\u0412\u0440\u0435\u043c\u044f \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f', null=True)),
            ],
            options={
                'verbose_name': '\u043b\u0430\u043a\u0441\u0438\u043c\u0430 \u043a\u0440\u043e\u0441\u0441 (\u0437\u0430\u043c\u0435\u043d\u0430)',
                'verbose_name_plural': '\u043b\u0430\u043a\u0441\u0438\u043c\u0430 \u043a\u0440\u043e\u0441\u0441\u044b (\u0437\u0430\u043c\u0435\u043d\u044b)',
            },
            bases=(models.Model,),
        ),
    ]
