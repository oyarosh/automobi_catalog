# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0034_auto_20150728_1949'),
    ]

    operations = [
        migrations.RunSQL(
            """
                CREATE OR REPLACE FUNCTION translit_str(p_string character varying)
                  RETURNS character varying AS
                $BODY$
                select 
                replace(
                replace(
                replace(
                replace(
                replace(
                replace(
                replace(
                replace(
                replace(
                translate(lower($1), 
                'абвгдеёзийклмнопрстуфхць', 'abvgdeezijklmnoprstufхc`'),
                'ж', 'zh'),
                'ч', 'ch'),
                'ш', 'sh'),
                'щ', 'shh'),
                'ъ', '``'),
                'ы', 'y`'),
                'э', 'e`'),
                'ю', 'yu'),
                'я', 'ya');
                $BODY$
                  LANGUAGE sql IMMUTABLE
                  COST 100;
                UPDATE catalog_cartree
                SET slug = lower(regexp_replace(regexp_replace(translit_str(title), '[_ ]', '-', 'g'), '[^a-zA-Z0-9\-]', '', 'g')) || '-' || id;
            """),
    ]
