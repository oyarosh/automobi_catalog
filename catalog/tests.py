# coding: utf-8
import datetime
import unittest
from decimal import Decimal

from django.test import TestCase
from django.contrib.auth.models import AnonymousUser

from core.models import Currency
from core.factories import CurrencyFactory
from core.test_utils import FactoriesTestsMetaclass

from superuser.models import Settings, BasicDiscount

from price_generator.models import DiscountGroup, SupplierDiscountGroup
from price_generator.exceptions import NoPriceFound
from price_generator.factories import PurchasePriceListFactory, \
    SalesPriceListFactory, SOPriceRatioFactory, PriceListItemFactory, \
    SalesPriceListItemFactory

from users.models import ClientGroup
from users.test_utils import get_packed, create_role_with_staff
from users.factories import SuperuserFactory, SupplierFactory, DCFactory,\
    SOFactory, EntityClientFactory
from cabinet.factories import CurrencySettingsFactory

from catalog import factories
from catalog.utils import GetPriceLists, GetItems, GetPrice, GetCurrencySettings


@unittest.skip('skip test')
class FactoriesTests(TestCase):
    __metaclass__ = FactoriesTestsMetaclass
    factories_module = factories
    fixtures = ['permissions', 'position', 'currencies', 'payment_methods', 'payment_terms',
                'rounding_methods', 'settings', 'default_dc', 'ftp_group']


@unittest.skip('skip test')
class GetPriceListsTests(TestCase):
    fixtures = ['permissions', 'position', 'currencies', 'payment_methods', 'payment_terms',
                'rounding_methods', 'settings', 'default_dc', 'ftp_group']

    @unittest.skip('skip test')
    def test_is_anonymous(self):
        default_dc = DCFactory(is_default=True)
        so = SOFactory(created_by=default_dc)
        spls = SalesPriceListFactory.create_batch(10, dc=default_dc, so=[so])
        user = AnonymousUser()

        result = GetPriceLists(user).process()
        self.assertItemsEqual(result, spls)

    def test_superuser(self):
        default_dc = DCFactory(is_default=True)
        so = SOFactory(created_by=default_dc)
        spls = SalesPriceListFactory.create_batch(10, dc=default_dc, so=[so])
        superuser = SuperuserFactory()

        result = GetPriceLists(superuser).process()
        self.assertItemsEqual(result, spls)

    @unittest.skip('skip test')
    def test_supplier(self):
        supplier = SupplierFactory()
        ppls = PurchasePriceListFactory.create_batch(10, supplier=supplier)

        result = GetPriceLists(supplier).process()
        self.assertItemsEqual(result, ppls)

    def test_dc(self):
        dc = DCFactory()
        spls = SalesPriceListFactory.create_batch(10, dc=dc)

        result = GetPriceLists(dc).process()
        self.assertItemsEqual(result, spls)

    def test_so(self):
        dc = create_role_with_staff(DCFactory)
        so = create_role_with_staff(SOFactory)
        spls = SalesPriceListFactory.create_batch(10, dc=dc, so=[so])

        result = GetPriceLists(so).process()
        self.assertItemsEqual(result, spls)

    def test_client(self):
        dc = create_role_with_staff(DCFactory)
        so = create_role_with_staff(SOFactory)
        spls = SalesPriceListFactory.create_batch(10, dc=dc, so=[so])
        client = EntityClientFactory(current_so=so)

        result = GetPriceLists(client).process()
        self.assertItemsEqual(result, spls)


@unittest.skip('skip test')
class GetItemsTests(TestCase):
    fixtures = ['permissions', 'position', 'currencies', 'payment_methods', 'payment_terms',
                'rounding_methods', 'settings', 'default_dc', 'ftp_group']

    def setUp(self):
        self.part = factories.PartFactory()
        self.pli_kwargs = {'part': self.part}

    def assertSetContains(self, set_, *args):
        for arg in args:
            self.assertIn(arg, set_)

    def _get_packed(self, *args, **kwargs):
        return get_packed(*args, pli_kwargs=self.pli_kwargs, **kwargs)

    @unittest.skip('skip test')
    def test_is_anonymous(self):
        pli, spl = self._get_packed('pli', 'spl',
                                    dc_kwargs={'is_default': True},
                                    so_created_by=True)
        user = AnonymousUser()

        result = GetItems(part=self.part, user=user).process()
        self.assertSetContains(result.pop(), pli, spl)

    @unittest.skip('error test for superuser')
    def test_superuser(self):
        pli, spl = self._get_packed('pli', 'spl',
                                    dc_kwargs={'is_default': True},
                                    so_created_by=True)
        superuser = SuperuserFactory()

        result = GetItems(part=self.part, user=superuser).process()
        self.assertSetContains(result.pop(), pli, spl)

    @unittest.skip('skip test')
    def test_supplier(self):
        pli, ppl, supplier = self._get_packed('pli', 'ppl', 'supplier')

        result = GetItems(part=self.part, user=supplier).process()
        self.assertSetContains(result.pop(), pli, ppl)

    @unittest.skip('skip test')
    def test_dc(self):
        pli, spl, dc = self._get_packed('pli', 'spl', 'dc')

        result = GetItems(part=self.part, user=dc).process()
        self.assertSetContains(result.pop(), pli, spl)

    @unittest.skip('skip test')
    def test_so(self):
        pli, spl, so = self._get_packed('pli', 'spl', 'so')

        result = GetItems(part=self.part, user=so).process()
        self.assertSetContains(result.pop(), pli, spl)

    @unittest.skip('skip test')
    def test_client(self):
        pli, spl, client = self._get_packed('pli', 'spl', 'client')

        result = GetItems(part=self.part, user=client).process()
        self.assertSetContains(result.pop(), pli, spl)


@unittest.skip('skip test')
class GetPriceTests(TestCase):
    fixtures = ['permissions', 'position', 'currencies', 'payment_methods', 'payment_terms',
                'rounding_methods', 'settings', 'default_dc', 'ftp_group']

    # def setUp(self):
    #     if settings.TEST_RUNNER == 'core.test_utils.Runner':
    #         RoundingMethod.objects.all().delete()

    @unittest.skip('skip test')
    def test_is_anonymous(self):
        currency_usd = Currency.objects.get(id=2)
        currency_uan = Currency.objects.get(id=1)
        currency_rub = Currency.objects.get(id=3)
        currency_eur = Currency.objects.get(id=6)
        pli, spl, bc = get_packed('pli', 'spl', 'basic_currency',
                                  dc_kwargs={'is_default': True},
                                  pli_kwargs={'id': 8012873, 'currency': currency_usd},
                                  spl_kwargs={'id': 102},
                                  so_created_by=True)

        # test currency
        #basic curency uan
        result = GetPrice(user=None, item=pli, spl=spl).process()
        price = Decimal('6.4200')
        self.assertTupleEqual(result, (price, bc))

        # rub
        result = GetPrice(user=None, item=pli, spl=spl, currency=currency_rub).process()
        print result
        price = Decimal('6.4200')
        self.assertTupleEqual(result, (price, bc))

        # usd
        result = GetPrice(user=None, item=pli, spl=spl, currency=currency_usd).process()
        print result
        price = Decimal('6.4200')
        self.assertTupleEqual(result, (price, bc))

        # eur
        result = GetPrice(user=None, item=pli, spl=spl, currency=currency_eur).process()
        print result
        price = Decimal('6.4200')
        self.assertTupleEqual(result, (price, bc))

    @unittest.skip('error test for superuser')
    def test_superuser(self):
        pli, spl, bc = get_packed('pli', 'spl', 'basic_currency',
                                  dc_kwargs={'is_default': True})
        superuser = SuperuserFactory()

        result = GetPrice(user=superuser, item=pli, spl=spl).process()
        price = spl.rounding_method.calculate(
            pli.price_set.last().client_price * Decimal('2'))
        self.assertTupleEqual(result, (price, bc))

    @unittest.skip('skip test')
    def test_supplier(self):
        pli, ppl, supplier, bc = get_packed('pli', 'ppl',
                                            'supplier', 'basic_currency')

        result = GetPrice(user=supplier, item=pli, spl=ppl).process()
        expected = pli.original_price * Decimal('2')
        self.assertTupleEqual(result, (pli.original_price * Decimal('2'), bc))

    @unittest.skip('price not found/ Refactor for api')
    def test_dc(self):
        pli, spl, dc, bc = get_packed('pli', 'spl', 'dc', 'basic_currency')

        purchase = GetPrice(user=dc, item=pli, spl=spl).process()
        sales = GetPrice(user=dc, item=pli, spl=spl,
                         get_purchase=False).process()

        expected_purchase = spl.rounding_method.calculate(pli.original_price * Decimal('2'))
        self.assertTupleEqual(purchase, (expected_purchase, bc))

        expected_sales = spl.rounding_method.calculate(
            pli.price_set.last().client_price * Decimal('2'))
        self.assertTupleEqual(sales, (expected_sales, bc))

    @unittest.skip('price not found/ Refactor for api')
    def test_so(self):
        pli, spl, so, dc, bc = get_packed('pli', 'spl', 'so',
                                          'dc', 'basic_currency')
        SOPriceRatioFactory(dc=dc, so=so, sales_price_list=spl, ratio=2)

        purchase = GetPrice(user=so, item=pli, spl=spl).process()
        sales = GetPrice(user=so, item=pli, spl=spl,
                         get_purchase=False).process()
        price = pli.price_set.last().client_price * Decimal('2')

        self.assertTupleEqual(purchase, (
            spl.rounding_method.calculate(price * Decimal('2')), bc))
        self.assertTupleEqual(sales, (spl.rounding_method.calculate(price), bc))

    @unittest.skip('price not found/ Refactor for api')
    def test_client(self):
        pli, spl, client, so, bc = get_packed('pli', 'spl', 'client',
                                              'so', 'basic_currency')
        result = GetPrice(user=client, item=pli, spl=spl).process()
        price = pli.price_set.last().client_price * Decimal('2')
        self.assertTupleEqual(result, (spl.rounding_method.calculate(price), bc))

    @unittest.skip('price not found/ Refactor for api')
    def test_client_with_discount_group(self):
        pli, spl, client, so, bc = get_packed('pli', 'spl', 'client',
                                              'so', 'basic_currency')
        price = pli.price_set.last().client_price

        DiscountGroup.objects.create(so=so, content_object=client,
                                     sales_price_list=spl, discount=5)
        result = GetPrice(user=client, item=pli, spl=spl).process()

        expected = spl.rounding_method.calculate(price * Decimal('2') * Decimal('5'))
        self.assertTupleEqual(result, (expected, bc))

    @unittest.skip('price not found/ Refactor for api')
    def test_client_with_client_group_and_discount_group(self):
        pli, spl, client, so, bc = get_packed('pli', 'spl', 'client',
                                              'so', 'basic_currency')
        price = pli.price_set.last().client_price

        cg = ClientGroup.objects.create(so=so, title='some group')
        cg.clients.add(client)

        DiscountGroup.objects.create(so=so, content_object=client,
                                     sales_price_list=spl, discount='4')
        result = GetPrice(user=client, item=pli, spl=spl).process()

        expected = spl.rounding_method.calculate(price * Decimal('2') * Decimal('4'))
        self.assertTupleEqual(result, (expected, bc))

    # def test_client_with_both_groups(self):
    #     pli, spl, client, so, bc = get_packed('pli', 'spl', 'client',
    #                                           'so', 'basic_currency')
    #     RoundingMethod.objects.all().delete()
    #     price = pli.price_set.last().client_price

    #     DiscountGroup.objects.create(so=so, content_object=client,
    #                                  sales_price_list=spl, discount='2')

    #     cg = ClientGroup.objects.create(so=so, title='some group')
    #     cg.clients.add(client)
    #     DiscountGroup.objects.create(so=so, content_object=client,
    #                                  sales_price_list=spl, discount='4')

    #     result = GetPrice(user=client, item=pli, spl=spl).process()
    #     self.assertTupleEqual(result,
    #                           (price * Decimal('2') * Decimal('4'), bc))

    @unittest.skip('price not found/ Refactor for api')
    def test_client_with_both_groups_and_max_discount(self):
        pli, spl, ppl, client, so, supplier, bc = get_packed(
            'pli', 'spl', 'ppl', 'client', 'so', 'supplier', 'basic_currency')
        price = pli.price_set.last().client_price

        DiscountGroup.objects.create(so=so, content_object=client,
                                     sales_price_list=spl, discount='0.3')

        # cuz its not used for now
        # cg = ClientGroup.objects.create(so=so, title='some group')
        # cg.clients.add(client)
        # DiscountGroup.objects.create(so=so, content_object=client,
        #                              sales_price_list=spl, discount='4')

        SupplierDiscountGroup.objects.create(supplier=supplier, pricelist=ppl,
                                             max_discount='0.5',
                                             extra_charge='6')

        result = GetPrice(user=client, item=pli, spl=spl).process()

        expected = spl.rounding_method.calculate(price * Decimal('2') * Decimal('0.5'))
        self.assertTupleEqual(result, (expected, bc))

    @unittest.skip('price not found/ Refactor for api')
    def test_client_with_currency(self):
        pli, spl, client, so, bc = get_packed('pli', 'spl', 'client',
                                              'so', 'basic_currency')

        second = CurrencyFactory()

        CurrencySettingsFactory(first=bc, second=second,
                                value=3, content_object=so)

        result = GetPrice(user=client, item=pli,
                          spl=spl, currency=second).process()

        expected = pli.price_set.last().client_price * Decimal('2') * Decimal('3')
        self.assertTupleEqual(result, (expected, second))

    @unittest.skip('process_sales_price_list do not exist')
    def test_so_with_hidden_price(self):
        so, spl, pli = get_packed('so', 'spl', 'pli')

        self.assertEqual(spl.pricelistitem_set.count(), 1)
        self.assertEqual(spl.price_set.count(), 1)
        self.assertEqual(spl.price_set.filter(hidden=True).count(), 0)

        #process_sales_price_list(spl)

        self.assertEqual(spl.pricelistitem_set.count(), 1)
        self.assertEqual(spl.price_set.count(), 1)
        self.assertEqual(spl.price_set.filter(hidden=True).count(), 0)

        GetPrice(pli, spl, so).process()
        GetPrice(pli, spl, so, get_hidden_price=True).process()

        spl.salespricelistitem_set.get().delete()
        #process_sales_price_list(spl)

        self.assertEqual(spl.pricelistitem_set.count(), 0)
        self.assertEqual(spl.price_set.count(), 1)
        self.assertEqual(spl.price_set.filter(hidden=True).count(), 1)

        with self.assertRaises(NoPriceFound):
            GetPrice(pli, spl, so).process()

        GetPrice(pli, spl, so, get_hidden_price=True).process()

    @unittest.skip('process_sales_price_list do not exist')
    def test_so_with_hidden_price_and_spr(self):
        dc, so, spl, pli = get_packed('dc', 'so', 'spl', 'pli')
        dc.sopriceratio_set.create(sales_price_list=spl, so=so, ratio=10)

        self.assertEqual(spl.pricelistitem_set.count(), 1)
        self.assertEqual(spl.price_set.count(), 1)
        self.assertEqual(spl.price_set.filter(hidden=True).count(), 0)

        #process_sales_price_list(spl)

        self.assertEqual(spl.pricelistitem_set.count(), 1)
        self.assertEqual(spl.price_set.count(), 1)
        self.assertEqual(spl.price_set.filter(hidden=True).count(), 0)

        GetPrice(pli, spl, so).process()
        GetPrice(pli, spl, so, get_hidden_price=True).process()

        spl.salespricelistitem_set.get().delete()
        #process_sales_price_list(spl)

        self.assertEqual(spl.pricelistitem_set.count(), 0)
        self.assertEqual(spl.price_set.count(), 1)
        self.assertEqual(spl.price_set.filter(hidden=True).count(), 1)

        with self.assertRaises(NoPriceFound):
            GetPrice(pli, spl, so).process()

        GetPrice(pli, spl, so, get_hidden_price=True).process()

    @unittest.skip('process_sales_price_list do not exist')
    def test_client_with_hidden_price(self):
        client, spl, pli = get_packed('client', 'spl', 'pli')

        self.assertEqual(spl.pricelistitem_set.count(), 1)
        self.assertEqual(spl.price_set.count(), 1)
        self.assertEqual(spl.price_set.filter(hidden=True).count(), 0)

        #process_sales_price_list(spl)

        self.assertEqual(spl.pricelistitem_set.count(), 1)
        self.assertEqual(spl.price_set.count(), 1)
        self.assertEqual(spl.price_set.filter(hidden=True).count(), 0)

        GetPrice(pli, spl, client).process()
        GetPrice(pli, spl, client, get_hidden_price=True).process()

        spl.salespricelistitem_set.get().delete()
        #process_sales_price_list(spl)

        self.assertEqual(spl.pricelistitem_set.count(), 0)
        self.assertEqual(spl.price_set.count(), 1)
        self.assertEqual(spl.price_set.filter(hidden=True).count(), 1)

        with self.assertRaises(NoPriceFound):
            GetPrice(pli, spl, client).process()

        GetPrice(pli, spl, client, get_hidden_price=True).process()

    # def test_client_with_suppliers_currency_settings(self):
    #     pli, spl, client, so, supplier, bc = get_packed(
    #         'pli', 'spl', 'client', 'so', 'supplier', 'basic_currency')

    #     RoundingMethod.objects.all().delete()

    #     second = CurrencyFactory()

    #     CurrencySettingsFactory(first=pli.currency, second=bc,
    #                             value=25, content_object=supplier)
    #     CurrencySettingsFactory(first=bc, second=second,
    #                             value=3, content_object=so)

    #     result = GetPrice(user=client, item=pli,
    #                       spl=spl, currency=second).process()
    #     price = pli.price_set.last().client_price * Decimal('25') * Decimal('3')
    #     self.assertTupleEqual(result, (price, second))

    @unittest.skip('skip test')
    def test_dc_with_vat_free_ppl(self):
        # this functionality is currently disabled, but this test is still maintained 
        # to crash if something changes
        ppl = PurchasePriceListFactory()
        pli = PriceListItemFactory(price_list=ppl, price=200, original_price=100,
                                   currency=Settings.get_value_by_name('current_currency'))

        spl = SalesPriceListFactory()
        SalesPriceListItemFactory(sales_price_list=spl,
                                  content_object=ppl,
                                  margin=10)

        result = GetPrice(user=spl.dc, item=pli, spl=spl).process()
        self.assertEqual(result[0], Decimal('100'))

        ppl.vat_free = True
        ppl.save(update_fields=['vat_free'])

        result = GetPrice(user=spl.dc, item=pli, spl=spl).process()
        # expected = spl.rounding_method.calculate(Decimal('83.3'))
        expected = Decimal('100')
        self.assertEqual(result[0], expected)

    @unittest.skip('skip test')
    def test_dc_buying_with_min_extra_price(self):
        ppl = PurchasePriceListFactory()
        pli = PriceListItemFactory(price_list=ppl, price=200, original_price=100,
                                   currency=Settings.get_value_by_name('current_currency'))

        spl = SalesPriceListFactory()
        SalesPriceListItemFactory(sales_price_list=spl,
                                  content_object=ppl,
                                  margin=10)

        result = GetPrice(user=spl.dc, item=pli, spl=spl).process()
        self.assertEqual(result[0], Decimal('100'))

        ppl.sell_with_min_extra_price = True
        ppl.save(update_fields=['sell_with_min_extra_price'])

        result = GetPrice(user=spl.dc, item=pli, spl=spl).process()
        self.assertEqual(result[0], Decimal('200'))


@unittest.skip('process_sales_price_list do not exist')
class BDTests(TestCase):
    fixtures = ['permissions', 'position', 'currencies', 'payment_methods', 'payment_terms',
                'rounding_methods', 'settings', 'default_dc', 'ftp_group']

    def test_so(self):
        dc = DCFactory()
        so = SOFactory()
        supplier = SupplierFactory()

        bc = Settings.get_value_by_name('current_currency')

        bd = BasicDiscount.objects.create(code='OPT1',
                                          name='opt1',
                                          description='',
                                          value=1.01)

        ppl = PurchasePriceListFactory(supplier=supplier)
        pli = PriceListItemFactory(price_list=ppl, price=100, currency=bc)

        spl = SalesPriceListFactory(dc=dc)
        spli = spl.salespricelistitem_set.create(content_object=ppl, margin=55)

        spl.margingroup_set.create(margin=5, dc=dc, sales_price_list_item=spli)
        spl.margingroup_set.create(basic_discount=bd, margin=2, dc=dc,
                                   sales_price_list_item=spli)

        #process_sales_price_list(spl)
        spli.margin = 1.5
        spli.save(update_fields=['margin'])
        #process_sales_price_list(spl)
        # just to test price instances ordering.

        bc.rounding_methods.all().delete()

        self.assertEqual(spl.pricelistitem_set.count(), 1)
        self.assertEqual(spl.price_set.count(), 3)
        self.assertEqual(spl.price_set.filter(basic_discount=None).count(), 2)
        # self.assertEqual(spl.price_set.count(), 6)
        # self.assertEqual(spl.price_set.filter(basic_discount=None).count(), 4)

        p1, p2, p3 = spl.price_set.order_by(
            '-timestamp', 'basic_discount', 'margin_group').values_list(
            'client_price', flat=True)[:3]

        self.assertEqual(p1, Decimal('200'))
        self.assertEqual(p2, Decimal('500'))
        self.assertEqual(p3, Decimal('150'))

        result = GetPrice(pli, spl, so).process()
        self.assertEqual(result[0], Decimal('500'))

        dc.sopriceratio_set.create(so=so, ratio=2)
        result = GetPrice(pli, spl, so).process()
        self.assertEqual(result[0], Decimal('1000'))

        dc.sopriceratio_set.create(so=so, ratio=10, basic_discount=bd)
        result = GetPrice(pli, spl, so).process()
        self.assertEqual(result[0], Decimal('2020'))

    def test_so_with_tm_ordering(self):
        dc = DCFactory()
        so = SOFactory()
        supplier = SupplierFactory()

        bc = Settings.get_value_by_name('current_currency')

        bd = BasicDiscount.objects.create(code='OPT1',
                                          name='opt1',
                                          description='',
                                          value=1.01)

        ppl = PurchasePriceListFactory(supplier=supplier)
        pli = PriceListItemFactory(price_list=ppl, price=100, currency=bc)
        tm = pli.part.tm

        spl = SalesPriceListFactory(dc=dc)
        spli = spl.salespricelistitem_set.create(content_object=ppl,
                                                 margin=55)

        spl.margingroup_set.create(margin=5, dc=dc, sales_price_list_item=spli)
        spl.margingroup_set.create(margin=10, dc=dc, sales_price_list_item=spli,
                                   tm=tm)

        spl.margingroup_set.create(basic_discount=bd, margin=2, dc=dc,
                                   sales_price_list_item=spli)
        spl.margingroup_set.create(basic_discount=bd, margin=15, dc=dc,
                                   sales_price_list_item=spli, tm=tm)

        #process_sales_price_list(spl)
        spli.margin = 1.5
        spli.save(update_fields=['margin'])
        #process_sales_price_list(spl)

        bc.rounding_methods.all().delete()

        self.assertEqual(spl.pricelistitem_set.count(), 1)
        self.assertEqual(spl.price_set.count(), 5)
        self.assertEqual(spl.price_set.filter(basic_discount=None).count(), 3)
        # self.assertEqual(spl.price_set.count(), 10)
        # self.assertEqual(spl.price_set.filter(basic_discount=None).count(), 6)

        prices = spl.price_set.order_by('-id').values_list('client_price', flat=True)[:4]
        expected = [500, 1000, 200, 1500]
        self.assertItemsEqual(prices, expected)

        result = GetPrice(pli, spl, so).process()
        self.assertEqual(result[0], Decimal('1000'))

        dc.sopriceratio_set.create(so=so, ratio=2)
        result = GetPrice(pli, spl, so).process()
        self.assertEqual(result[0], Decimal('2000'))

        dc.sopriceratio_set.create(so=so, ratio=10, basic_discount=bd)
        result = GetPrice(pli, spl, so).process()
        self.assertEqual(result[0], Decimal('15150'))

    def test_client(self):
        supplier = SupplierFactory()
        dc = DCFactory()
        so = SOFactory()
        client = EntityClientFactory(current_so=so)

        bc = Settings.get_value_by_name('current_currency')

        bd = BasicDiscount.objects.create(code='OPT1',
                                          name='opt1',
                                          description='',
                                          value=Decimal('1.01'))

        ppl = PurchasePriceListFactory(supplier=supplier)
        pli = PriceListItemFactory(price_list=ppl, price=100, currency=bc)
        tm = pli.part.tm

        spl = SalesPriceListFactory(dc=dc)
        spli = spl.salespricelistitem_set.create(content_object=ppl,
                                                 margin=55)

        spl.margingroup_set.create(margin=5, dc=dc, sales_price_list_item=spli)
        spl.margingroup_set.create(basic_discount=bd, margin=2, dc=dc,
                                   sales_price_list_item=spli)

        #process_sales_price_list(spl)
        spli.margin = 1.5
        spli.save(update_fields=['margin'])
        #process_sales_price_list(spl)

        bc.rounding_methods.all().delete()

        self.assertEqual(spl.pricelistitem_set.count(), 1)
        self.assertEqual(spl.price_set.count(), 3)
        self.assertEqual(spl.price_set.filter(basic_discount=None).count(), 2)
        # self.assertEqual(spl.price_set.count(), 6)
        # self.assertEqual(spl.price_set.filter(basic_discount=None).count(), 4)

        p1, p2, p3 = spl.price_set.order_by(
            '-timestamp', 'basic_discount', 'margin_group').values_list(
            'client_price', flat=True)[:3]

        self.assertEqual(p1, Decimal('200'))
        self.assertEqual(p2, Decimal('500'))
        self.assertEqual(p3, Decimal('150'))

        result = GetPrice(pli, spl, client).process()
        self.assertEqual(result[0], Decimal('500'))

        so.basic_discounts.create(client=client, discount=bd)

        result = GetPrice(pli, spl, client).process()
        self.assertEqual(result[0], Decimal('202'))

        so.discountgroup_set.create(
            content_object=client, discount=10, sales_price_list=spl)

        result = GetPrice(pli, spl, client).process()
        self.assertEqual(result[0], Decimal('5000'))

        dg = so.discountgroup_set.create(
            content_object=client,
            sales_price_list=spl,
            basic_discount=bd,
            discount=2)

        result = GetPrice(pli, spl, client).process()
        self.assertEqual(result[0], Decimal('404'))

        dg.delete()
        result = GetPrice(pli, spl, client).process()
        self.assertEqual(result[0], Decimal('5000'))

        so.discountgroup_set.create(
            content_object=client, sales_price_list=spl, basic_discount=bd,
            tm=tm, discount=2)

        result = GetPrice(pli, spl, client).process()
        self.assertEqual(result[0], Decimal('404'))


# class ACTests(TestCase):

#     def test(self):
#         parts1 = factories.PartFactory.create_batch(3)
#         parts2 = factories.PartFactory.create_batch(2)

#         ac1 = factories.ACFactory()
#         ac2 = factories.ACFactory()

#         ac1.parts = parts1
#         ac2.parts = parts2

#         url = reverse('catalog:automobi_catalog')
#         response = self.client.get(url)
#         self.assertEqual(response.status_code, 200)
#         self.assertEqual(len(response.context_data['object_list']), 5)
#         self.assertEqual(len(response.context_data['items']), 5)

#     def test_ac_filter(self):
#         parts1 = factories.PartFactory.create_batch(3)
#         parts2 = factories.PartFactory.create_batch(2)

#         ac1 = factories.ACFactory()
#         ac2 = factories.ACFactory()

#         ac1.parts = parts1
#         ac2.parts = parts2

#         url = reverse('catalog:automobi_catalog') + '?ac_id=%s' % ac1.id
#         response = self.client.get(url)
#         self.assertEqual(response.status_code, 200)
#         self.assertEqual(len(response.context_data['object_list']), 3)
#         self.assertEqual(len(response.context_data['items']), 3)

#     def test_ac_prop_value_filter(self):
#         parts1 = factories.PartFactory.create_batch(3)
#         parts2 = factories.PartFactory.create_batch(2)

#         ac1 = factories.ACFactory()
#         ac2 = factories.ACFactory()

#         ac1.parts = parts1
#         ac2.parts = parts2

#         ac_prop_value = factories.ACPropertyValueFactory()
#         ac_prop_value.parts = parts2

#         url = reverse('catalog:automobi_catalog') + \
#             '?ac_val=%s' % ac_prop_value.id
#         response = self.client.get(url)
#         self.assertEqual(response.status_code, 200)
#         self.assertEqual(len(response.context_data['object_list']), 2)
#         self.assertEqual(len(response.context_data['items']), 2)

#     def test_2_ac_prop_values(self):
#         parts1 = factories.PartFactory.create_batch(3)
#         parts2 = factories.PartFactory.create_batch(2)

#         ac1 = factories.ACFactory()
#         ac2 = factories.ACFactory()

#         ac1.parts = parts1
#         ac2.parts = parts2

#         ac_prop_value1 = factories.ACPropertyValueFactory()
#         ac_prop_value1.parts = parts1

#         ac_prop_value2 = factories.ACPropertyValueFactory()
#         ac_prop_value2.parts = parts2

#         url = reverse('catalog:automobi_catalog') + \
#             '?ac_val=%s&ac_val=%s' % (ac_prop_value1.id, ac_prop_value2.id)
#         response = self.client.get(url)
#         self.assertEqual(response.status_code, 200)
#         self.assertEqual(len(response.context_data['object_list']), 5)
#         self.assertEqual(len(response.context_data['items']), 5)


@unittest.skip('process_sales_price_list do not exist')
class GetPricePrecisionTests(TestCase):
    fixtures = ['permissions', 'position', 'currencies', 'payment_methods', 'payment_terms',
                'rounding_methods', 'settings', 'default_dc', 'ftp_group']

    def test(self):
        dc = create_role_with_staff(DCFactory)
        uah = Settings.get_value_by_name('current_currency')
        usd = Currency.objects.get(abbrev='дол.')

        uah_per_usd = Decimal('16.9')
        CurrencySettingsFactory(content_object=dc, first=uah,
                                second=usd, value=Decimal('1') / uah_per_usd)

        pli = PriceListItemFactory(price=100, original_price=100, currency=usd)
        spl = SalesPriceListFactory(dc=dc)
        spl.salespricelistitem_set.create(content_object=pli.price_list)

        # RoundingMethod.objects.all().delete()

        #process_sales_price_list(spl)
        self.assertEqual(spl.pricelistitem_set.count(), 1)

        result = GetPrice(pli, spl, dc, usd).process()
        self.assertEqual(result, (Decimal('100'), usd))

        result = GetPrice(pli, spl, dc).process()
        expected = spl.rounding_method.calculate(Decimal('1690.000000003887000000008940'))
        self.assertEqual(result, (expected, uah))


@unittest.skip('skip test')
class GetCurrencySettingsTests(TestCase):
    fixtures = ['permissions', 'position',  'currencies', 'payment_methods', 'payment_terms',
                'rounding_methods', 'settings', 'default_dc', 'ftp_group']

    def test(self):
        dc = create_role_with_staff(DCFactory)
        uah = Settings.get_value_by_name('current_currency')
        usd = Currency.objects.get(abbrev='дол.')

        today = datetime.date.today()
        yesterday = today - datetime.timedelta(days=1)
        s1 = CurrencySettingsFactory(content_object=dc, first=uah, second=usd,
                                     value=0.5, date=today)
        s2 = CurrencySettingsFactory(content_object=dc, first=uah, second=usd,
                                     value=0.25, date=yesterday)

        settings, is_reversed = GetCurrencySettings(
            dc, None, usd, uah).process()
        self.assertTrue(is_reversed)
        self.assertEqual(settings, s1)

        previous_query = {'date__lt': settings.date}
        settings, is_reversed = GetCurrencySettings(
            dc, None, usd, uah, previous_query).process()
        self.assertTrue(is_reversed)
        self.assertEqual(settings, s2)
