from .search_query import subquery_select_final_result, subquery_constants


get_part = """
    SELECT
      PART.id,
      PART.tm_id,
      PART.search_number,
      PART.catalog_number,
      COALESCE(PDT.value, PART.description) AS description,
      Null AS cross_id,
      TM.show_name,
      PART.slug,
      PART.img_quantity
    FROM catalog_part PART
    LEFT JOIN catalog_trademark TM
        ON TM.id = PART.tm_id
    LEFT JOIN catalog_partdescriptiontext PDT
        ON PDT.part_description_id = PART.default_description_id
            AND PDT.language_id = 16
    WHERE {fixed_part_id}

    {cross_part_query}
"""
get_part_cross = """
    UNION ALL

    SELECT
      PART.id,
      PART.tm_id,
      PART.search_number,
      PART.catalog_number,
      COALESCE(PDT.value, PART.description) AS description,
      CR.part_id AS cross_id,
      TM.show_name,
      PART.slug,
      PART.img_quantity
    FROM catalog_cross CR
    LEFT JOIN catalog_part PART
        ON PART.id = CR.cross_id
    LEFT JOIN catalog_partdescriptiontext PDT
        ON PDT.part_description_id = PART.default_description_id
            AND PDT.language_id = 16
    LEFT JOIN catalog_trademark TM
        ON TM.id = PART.tm_id
    WHERE CR.part_id = ANY(
        SELECT
          PART.id
        FROM catalog_part PART
        WHERE %s
    )
    ORDER BY
        cross_id DESC
"""

export_sale_price_list = """
COPY
(
    """ + subquery_select_final_result + """
) TO %(export_file)s
WITH DELIMITER %(export_delimiter)s
FORCE QUOTE *
CSV HEADER
ENCODING %(encoding)s
"""


get_price_data = subquery_select_final_result

dc_query = """
    CASE
        WHEN PGM.margin IS NULL THEN
            purchase_price * spli_margin
        ELSE
            purchase_price * PGM.margin
    END AS price,
    price AS sales_price,
    CAST(COALESCE((
        SELECT value FROM DC_D_VALUE
        WHERE DC_D_VALUE.sender_object_id = item_supplier_id
        AND (DC_D_VALUE.price_list_id = pl_id OR DC_D_VALUE.price_list_id IS NULL)
        AND (DC_D_VALUE.warehouse_id = item_warehouse_id OR DC_D_VALUE.warehouse_id IS NULL)
        AND (DC_D_VALUE.city_id = WH.city_id OR DC_D_VALUE.city_id IS NULL)
        ORDER BY DC_D_VALUE.row ASC
        LIMIT 1
    ), 0) AS integer) + CAST(COALESCE(order_processing_time::INTEGER, 0) AS integer) AS delivery_time,
    CAST(COALESCE((
        SELECT guaranteed_value FROM DC_D_VALUE
        WHERE DC_D_VALUE.sender_object_id = item_supplier_id
        AND (DC_D_VALUE.price_list_id = pl_id OR DC_D_VALUE.price_list_id IS NULL)
        AND (DC_D_VALUE.warehouse_id = item_warehouse_id OR DC_D_VALUE.warehouse_id IS NULL)
        AND (DC_D_VALUE.city_id = WH.city_id OR DC_D_VALUE.city_id IS NULL)
        ORDER BY DC_D_VALUE.row ASC
        LIMIT 1
    ), 0) AS integer) + CAST(COALESCE(guaranteed_order_days::INTEGER, 0) AS integer) AS guaranteed_delivery_time,
"""

so_query = """
    CASE
        WHEN PGM_DEFAULT.margin IS NULL THEN
            price * spli_margin
        ELSE
            price * PGM_DEFAULT.margin
    END AS price,
    CASE
        WHEN PGM.margin IS NULL THEN
            CASE WHEN SPR_QS_VALUE.ratio IS NULL THEN
                price * spli_margin
            ELSE
               price * spli_margin * SPR_QS_VALUE.ratio
            END
        ELSE
            CASE WHEN SPR_QS_VALUE.ratio IS NULL THEN
                price * PGM.margin
            ELSE
                price * PGM.margin * SPR_QS_VALUE.ratio
            END
    END AS sales_price,
    CAST(COALESCE((
        SELECT value FROM DC_D_VALUE
        WHERE DC_D_VALUE.sender_object_id = item_supplier_id
        AND (DC_D_VALUE.price_list_id = pl_id OR DC_D_VALUE.price_list_id IS NULL)
        AND (DC_D_VALUE.warehouse_id = item_warehouse_id OR DC_D_VALUE.warehouse_id IS NULL)
        AND (DC_D_VALUE.city_id = WH.city_id OR DC_D_VALUE.city_id IS NULL)
        ORDER BY DC_D_VALUE.row ASC
        LIMIT 1
    ), 0) AS integer)
    + CAST(COALESCE((OS_D_VALUE.value), 0) AS integer)
    + CAST(COALESCE(order_processing_time::INTEGER, 0) AS integer)
    AS delivery_time,
    CAST(COALESCE((
        SELECT guaranteed_value FROM DC_D_VALUE
        WHERE DC_D_VALUE.sender_object_id = item_supplier_id
        AND (DC_D_VALUE.price_list_id = pl_id OR DC_D_VALUE.price_list_id IS NULL)
        AND (DC_D_VALUE.warehouse_id = item_warehouse_id OR DC_D_VALUE.warehouse_id IS NULL)
        AND (DC_D_VALUE.city_id = WH.city_id OR DC_D_VALUE.city_id IS NULL)
        ORDER BY DC_D_VALUE.row ASC
        LIMIT 1
    ), 0) AS integer)
    + CAST(COALESCE((OS_D_VALUE.guaranteed_value), 0) AS integer)
    + CAST(COALESCE(guaranteed_order_days::INTEGER, 0) AS integer)
    AS guaranteed_delivery_time,
"""

client_query = """
    CASE
        WHEN PGM.margin IS NULL THEN
            CASE WHEN DGQS_VALUE.discount IS NULL THEN
                price * spli_margin
            ELSE
                price * spli_margin * DGQS_VALUE.discount
            END
        ELSE
            CASE WHEN DGQS_VALUE.discount IS NULL THEN
                price * PGM.margin
            ELSE
                price * PGM.margin * DGQS_VALUE.discount
            END
    END AS sales_price,
    price AS price,
     CAST(COALESCE((
        SELECT value FROM DC_D_VALUE
        WHERE DC_D_VALUE.sender_object_id = item_supplier_id
        AND (DC_D_VALUE.price_list_id = pl_id OR DC_D_VALUE.price_list_id IS NULL)
        AND (DC_D_VALUE.warehouse_id = item_warehouse_id OR DC_D_VALUE.warehouse_id IS NULL)
        AND (DC_D_VALUE.city_id = WH.city_id OR DC_D_VALUE.city_id IS NULL)
        ORDER BY DC_D_VALUE.row ASC
        LIMIT 1
    ), 0) AS integer)
    + CAST(COALESCE((OS_D_VALUE.value), 0) AS integer)
    + CAST(COALESCE(order_processing_time::INTEGER, 0) AS integer)
    AS delivery_time,
    CAST(COALESCE((
        SELECT guaranteed_value FROM DC_D_VALUE
        WHERE DC_D_VALUE.sender_object_id = item_supplier_id
        AND (DC_D_VALUE.price_list_id = pl_id OR DC_D_VALUE.price_list_id IS NULL)
        AND (DC_D_VALUE.warehouse_id = item_warehouse_id OR DC_D_VALUE.warehouse_id IS NULL)
        AND (DC_D_VALUE.city_id = WH.city_id OR DC_D_VALUE.city_id IS NULL)
        ORDER BY DC_D_VALUE.row ASC
        LIMIT 1
    ), 0) AS integer)
    + CAST(COALESCE((OS_D_VALUE.guaranteed_value), 0) AS integer)
    + CAST(COALESCE(guaranteed_order_days::INTEGER, 0) AS integer)
    AS guaranteed_delivery_time,
"""

anonymous_query = """
    CASE
        WHEN PGM.margin IS NULL THEN
            price * spli_margin
        ELSE
            price * PGM.margin
    END AS sales_price,
    price AS price,
     CAST(COALESCE((
        SELECT value FROM DC_D_VALUE
        WHERE DC_D_VALUE.sender_object_id = item_supplier_id
        AND (DC_D_VALUE.price_list_id = pl_id OR DC_D_VALUE.price_list_id IS NULL)
        AND (DC_D_VALUE.warehouse_id = item_warehouse_id OR DC_D_VALUE.warehouse_id IS NULL)
        AND (DC_D_VALUE.city_id = WH.city_id OR DC_D_VALUE.city_id IS NULL)
        ORDER BY DC_D_VALUE.row ASC
        LIMIT 1
    ), 0) AS integer)
    + CAST(COALESCE((OS_D_VALUE.value), 0) AS integer)
    + CAST(COALESCE(order_processing_time::INTEGER, 0) AS integer)
    AS delivery_time,
    CAST(COALESCE((
        SELECT guaranteed_value FROM DC_D_VALUE
        WHERE DC_D_VALUE.sender_object_id = item_supplier_id
        AND (DC_D_VALUE.price_list_id = pl_id OR DC_D_VALUE.price_list_id IS NULL)
        AND (DC_D_VALUE.warehouse_id = item_warehouse_id OR DC_D_VALUE.warehouse_id IS NULL)
        AND (DC_D_VALUE.city_id = WH.city_id OR DC_D_VALUE.city_id IS NULL)
        ORDER BY DC_D_VALUE.row ASC
        LIMIT 1
    ), 0) AS integer)
    + CAST(COALESCE((OS_D_VALUE.guaranteed_value), 0) AS integer)
    + CAST(COALESCE(guaranteed_order_days::INTEGER, 0) AS integer)
    AS guaranteed_delivery_time,
"""

supplier_query = """
    CASE
        WHEN PGM.margin IS NULL THEN
            price * 1
        ELSE
            price * 1
    END AS price,
    price AS sales_price,
    CAST(COALESCE((
        SELECT value FROM DC_D_VALUE
        WHERE DC_D_VALUE.sender_object_id = item_supplier_id
        AND (DC_D_VALUE.price_list_id = pl_id OR DC_D_VALUE.price_list_id IS NULL)
        AND (DC_D_VALUE.warehouse_id = item_warehouse_id OR DC_D_VALUE.warehouse_id IS NULL)
        AND (DC_D_VALUE.city_id = WH.city_id OR DC_D_VALUE.city_id IS NULL)
        ORDER BY DC_D_VALUE.row ASC
        LIMIT 1
    ), 0) AS integer) + CAST(COALESCE(order_processing_time::INTEGER, 0) AS integer) AS delivery_time,
    CAST(COALESCE((
        SELECT guaranteed_value FROM DC_D_VALUE
        WHERE DC_D_VALUE.sender_object_id = item_supplier_id
        AND (DC_D_VALUE.price_list_id = pl_id OR DC_D_VALUE.price_list_id IS NULL)
        AND (DC_D_VALUE.warehouse_id = item_warehouse_id OR DC_D_VALUE.warehouse_id IS NULL)
        AND (DC_D_VALUE.city_id = WH.city_id OR DC_D_VALUE.city_id IS NULL)
        ORDER BY DC_D_VALUE.row ASC
        LIMIT 1
    ), 0) AS integer) + CAST(COALESCE(guaranteed_order_days::INTEGER, 0) AS integer) AS guaranteed_delivery_time,
"""

get_unmatched_part_ids = """
SELECT DISTINCT ON (price_list_id, search_product_code, trademark)
    search_product_code,
    product_description,
    trademark,
    price,
    stock,
    multiplicity,
    minimum_order,
    city,
    price_currency_id,
    price_list_id,
    template_id,
    tm_id,
    catalog_trademark_id,
    unm_id,
    server_id
FROM
(
SELECT
        PPL.server_id,
        UNM.search_product_code,
        UNM.product_description,
        CASE
            WHEN STM.catalog_trademark_id IS NOT NULL THEN
                (SELECT show_name FROM catalog_trademark WHERE id = STM.catalog_trademark_id)
            WHEN TM.id IS NOT NULL THEN
                TM.show_name
            ELSE
                UNM.trademark
        END AS trademark,
        UNM.price,
        UNM.stock,
        UNM.multiplicity,
        UNM.minimum_order,
        UNM.city,
        UNM.price_currency_id,
        UNM.price_list_id,
        UNM.template_id,
        CASE
            WHEN STM.catalog_trademark_id IS NOT NULL THEN
                STM.catalog_trademark_id
            WHEN TM.id IS NOT NULL THEN
                TM.id
            ELSE
                NULL
        END AS tm_id,
        CASE
            WHEN STM.catalog_trademark_id IS NOT NULL THEN
                STM.catalog_trademark_id
            WHEN TM.id IS NOT NULL THEN
                TM.id
            ELSE
                NULL
        END AS catalog_trademark_id,
        UNM.id AS unm_id
    FROM ppl_unmatchedproduct UNM
    JOIN ppl_pricelist PPL
        ON PPL.id = UNM.price_list_id
		LEFT JOIN price_generator_suppliertrademarkmapping STM
        ON STM.suppliers_trademark = UNM.trademark
            AND STM.supplier_id = PPL.supplier_id
            AND STM.catalog_trademark_id IS NOT NULL
    LEFT JOIN catalog_trademark TM
        ON TM.full_name = UPPER(TRIM(UNM.trademark))
    WHERE
        CASE
          WHEN %(unmatched_id)s > 0 THEN
            UNM.id = %(unmatched_id)s
          ELSE
            UNM.search_product_code = ANY(%(search_number)s)
        END
        AND UNM.error_type = 'unmatched'
        AND (UNM.trademark IS NOT NULL AND UNM.trademark != '')
    ORDER BY
      UNM.price_list_id,
      UNM.search_product_code,
      STM.catalog_trademark_id,
      UNM.timestamp DESC
) AS query
ORDER BY
      price_list_id,
      search_product_code,
	  trademark
"""


get_catalog = """
WITH PRICE AS (
    """ + subquery_select_final_result + """
)

SELECT
    s.part_id,
    s.catalog_number,
    s.description,
    s.show_name,
    json_agg((s.image)) as image,
    s.price,
    s.ac_id,
    json_agg((CA.id, CA.title, CAV.id, CAV.title)) as prop,
    s.slug,
    CASE WHEN s.max_stock::INT > 100 OR s.min_stock::INT > 100 THEN NULL ELSE s.min_stock END AS min_stock,
	CASE WHEN s.max_stock::INT > 100 THEN NULL ELSE s.max_stock END AS max_stock,
    s.min_delivery,
    s.max_delivery,
    s.is_other_price,
    s.sale_price_list,
	s.price_list_item_id
FROM (
    SELECT
        CACP.part_id AS part_id,
        PART.catalog_number AS catalog_number,
        PART.description AS description,
        TM.show_name AS show_name,
        json_agg((PI.image)) as image,
        min(PRICE.sales_price) as price,
        min(PRICE.delivery_time) as min_delivery,
        max(PRICE.delivery_time) as max_delivery,
        %(category_ids)s AS ac_id,
        PART.slug,
        PCI.min_stock,
        PCI.max_stock,
        PCI.is_other_price,
        PCI.sale_price_list,
		PCI.price_list_item_id
    FROM catalog_ac_parts CACP
	LEFT JOIN PRICE AS PRICE
	    ON PRICE.part_id = CACP.part_id
    LEFT JOIN catalog_part AS PART
        ON PART.id = CACP.part_id
    LEFT JOIN catalog_trademark TM
        ON PART.tm_id = TM.id
    {catalog_filters_query}
    LEFT JOIN catalog_partimage PI
        ON PI.part_id = CACP.part_id
		LEFT JOIN ppl_catalogitem PCI
			ON PRICE.part_id = PCI.part_id
		WHERE CACP.ac_id = %(category_ids)s
    GROUP BY CACP.part_id, TM.show_name, PART.slug,
             PART.catalog_number, PART.description,
			 PCI.min_stock,	PCI.max_stock, PCI.is_other_price,
			 PCI.sale_price_list, PCI.price_list_item_id
    ORDER BY
        {order_by_query}
        show_name ASC
    LIMIT %(limit)s
    OFFSET %(offset)s
) s
LEFT JOIN catalog_acpropertyvalue_parts CAVP
    ON s.part_id = CAVP.part_id
INNER JOIN catalog_acpropertyvalue CAV
    ON CAV.id = CAVP.acpropertyvalue_id
INNER JOIN catalog_acproperty CA
    ON CA.id = CAV.ac_property_id

GROUP BY s.part_id, s.catalog_number, s.description,
         s.show_name, s.price, s.ac_id, s.slug,
         s.min_stock, s.max_stock, s.min_delivery,
		 s.max_delivery, s.is_other_price,
		 s.sale_price_list,	s.price_list_item_id
ORDER BY
{order_by_query}
show_name ASC

"""


get_catalog_count = """
    SELECT
        COUNT(PART.id)
    FROM catalog_part PART
    INNER JOIN catalog_ac_parts CACP
        ON (PART.id = CACP.part_id
             AND CACP.ac_id = ANY(%(category_ids)s)
            )
    {catalog_filters_query}
"""

get_catalog_filters = """
    SELECT
    *
    FROM(
        SELECT
            CA.id,
            CA.title,
            CAP.id AS id2,
            CAP.title AS title2,
            COUNT(CAVP.part_id) AS parts,
            CA.show_by_count,
            CA.show_by_parts,
            row_number() OVER ( PARTITION BY CA.id ORDER BY COUNT(CAVP.part_id) DESC) AS row
        FROM catalog_acproperty CA
        INNER JOIN catalog_acproperty_acs CAA
            ON CAA.acproperty_id = CA.id
            AND CAA.ac_id = ANY(%(category_ids)s)
        INNER JOIN catalog_acpropertyvalue CAP
            ON CAP.ac_property_id = CA.id
        INNER JOIN catalog_acpropertyvalue_parts CAVP
            ON CAVP.acpropertyvalue_id = CAP.id
        INNER JOIN catalog_ac_parts CAPARTS
            ON CAPARTS.part_id = CAVP.part_id
            AND CAPARTS.ac_id = ANY(%(category_ids)s)

        {catalog_part_count}

        GROUP BY CA.id, CAP.id, CAVP.acpropertyvalue_id, CA.title, CAP.title
        ORDER BY
            CA.order ASC,
            CA.title ASC,
            (substring(CAP.title, '^[0-9]+'))::int ,substring(CAP.title, '[^0-9_].*$') ASC
    ) s
    WHERE
    CASE
        WHEN s.show_by_parts > 0 THEN
            s.parts >= s.show_by_parts
        ELSE
            s.row <= 1000
    END

"""

get_catalog_order = """
    WITH SALES_PRICE_ARRAY AS (
        SELECT * from json_populate_recordset(NULL::sales_prices, %(sales_price)s)
    )

    SELECT DISTINCT
        PART.id,
        price
    FROM catalog_part PART
    LEFT JOIN ppl_catalogitem PCI
        ON PCI.part_id = PART.id
    RIGHT JOIN catalog_ac_parts CAP
        ON CAP.part_id = PART.id
        AND CAP.ac_id = ANY(%(category_ids)s)
    INNER JOIN SALES_PRICE_ARRAY AS SPA
      ON PCI.sale_price_list = SPA.id
    {catalog_filters_query}
    WHERE (PCI.active = True OR PCI IS NULL)
    ORDER BY
        {order_by_query}
        PART.id
    LIMIT %(limit)s
    OFFSET %(offset)s
"""