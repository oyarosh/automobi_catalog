from .search import get_part, get_unmatched_part_ids, \
                    get_price_data, dc_query, so_query, \
                    client_query, anonymous_query, supplier_query, \
                    get_catalog, get_catalog_count, get_catalog_filters,\
                    export_sale_price_list, get_catalog_order, get_part_cross

__all__ = ['get_query']

SORTING_FIELD_QUERY_MAP = {
    'price': "ORDER BY sales_price {0}, delivery_time {0}, stock DESC, update_time DESC",
    'delivery': 'ORDER BY delivery_time {0}, sales_price {0}, stock DESC, update_time DESC',
    'stock': 'ORDER BY stock {0}, sales_price {0}, delivery_time {0}, update_time DESC',
    'brand': 'ORDER BY price {0}, delivery_time {0}, stock {0}, update_time DESC'
}

CATALOG_SORTING_FIELD_QUERY_MAP = {
    'price_asc': 'price ASC NULLS LAST,',
    'price_desc': 'price DESC NULLS LAST,',
}


QUERY_MAP = {
    'get_part': get_part,
    'get_unmatched_part_ids': get_unmatched_part_ids,
    'get_price_data': get_price_data,
    'get_catalog': get_catalog,
    'get_catalog_count': get_catalog_count,
    'get_catalog_filters': get_catalog_filters,
    'export_sale_price_list': export_sale_price_list,
    'get_catalog_order': get_catalog_order,

}

PRICE_FIELD_QUERY_MAP = {
    'dc': dc_query,
    'so': so_query,
    'client': client_query,
    'anonymous': anonymous_query,
    'supplier': supplier_query
}


def get_query(action, **kwargs):
    sorting_query = ''
    if kwargs.get('sorting', None):
        sorting_field, sorting_dir = kwargs['sorting']
        sorting_query = SORTING_FIELD_QUERY_MAP[sorting_field].format(sorting_dir)

    server_filter = 'off'
    if kwargs.get('server_filter', None):
        server_filter = kwargs.get('server_filter', None)

    order_by_query = ''
    if kwargs.get('order_by', None):
        order_by_query = CATALOG_SORTING_FIELD_QUERY_MAP[kwargs.get('order_by')].format()

    city_filter = ''
    if kwargs.get('selected_filters', None):
        selected_filters = kwargs.get('selected_filters')
        if selected_filters.get('city', None):
            city_filter = "AND PLI.city = %(city)s"
            kwargs['city'] = selected_filters.get('city')

    so_id_filter = ''
    so_margin_query = ''
    so_left_join = ''
    join_currency_where = ''
    join_currency_order = ''
    if kwargs.get('so_id', None):
        so_id_filter = 'AND OS_D_VALUE.receiver_object_id = %(so_id)s'
        so_margin_query = '''
            SPR_QS AS (
                SELECT DISTINCT ON (sales_price_list_id)
                    id, basic_discount_id, ratio, dc_id, sales_price_list_id
                FROM price_generator_sopriceratio
                WHERE so_id = %(so_id)s
                ORDER BY sales_price_list_id,
                    basic_discount_id IS NULL
            ),
        '''
        so_left_join = '''
            LEFT JOIN SPR_QS AS SPR_QS_VALUE
                ON SPR_QS_VALUE.sales_price_list_id = spl_id
                AND PGM.basic_discount_id = SPR_QS_VALUE.basic_discount_id
        '''
        join_currency_where = '''OR (SCT.model = 'so' AND CCC.object_id = %(so_id)s)'''
        join_currency_order = '''WHEN SCT.model = 'so' THEN 0'''

    client_id_join = ''
    if kwargs.get('client_id', None):
        client_id_join = '''LEFT JOIN price_generator_soclientbasicdiscount AS PGM_CLIENT
                            ON PGM_CLIENT.so_id = %(so_id)s AND PGM_CLIENT.client_id = %(client_id)s
                          LEFT JOIN price_generator_margingroup PGM
                            ON PGM.sales_price_list_id = spl_id
                            AND PGM.basic_discount_id = CASE WHEN DGQS_VALUE.basic_discount_id IS NOT NULL
                                THEN DGQS_VALUE.basic_discount_id
                                ELSE PGM_CLIENT.discount_id END
                        '''
        client_margin_query = '''DGQS AS (
            SELECT DISTINCT ON (sales_price_list_id, tm_id, basic_discount_id)
                DG.id, basic_discount_id, DG.discount, DG.sales_price_list_id, tm_id,
                BD.value as basic_discount_value
            FROM price_generator_discountgroup DG
            INNER JOIN core_apicontenttype CT
                ON CT.id = DG.content_type_id
            LEFT JOIN core_basicdiscount BD
                ON BD.id = DG.basic_discount_id
            WHERE CT.model = 'client'
                AND DG.object_id = %(client_id)s
                AND DG.so_id = %(so_id)s
            ORDER BY sales_price_list_id,
                tm_id,
                basic_discount_id
        ),'''
        client_left_join = '''LEFT JOIN DGQS AS DGQS_VALUE
            ON (DGQS_VALUE.sales_price_list_id = spl_id OR DGQS_VALUE.sales_price_list_id IS NULL)
            AND (DGQS_VALUE.tm_id = TM.id OR DGQS_VALUE.tm_id IS NULL)'''
    else:
        client_id_join = '''LEFT JOIN price_generator_margingroup PGM
                             ON PGM.sales_price_list_id = spl_id
                             AND PGM.basic_discount_id = spa_discount
                        '''
        client_margin_query = ''
        client_left_join = ''

    shortcut_query = ''
    if kwargs.get('shortcut', None):
        shortcut_field = kwargs['shortcut']
        shortcut_query = PRICE_FIELD_QUERY_MAP[shortcut_field].format()
        if shortcut_field == 'anonymous':
            client_id_join = '''LEFT JOIN price_generator_margingroup PGM
                                    ON PGM.sales_price_list_id = spl_id
                                    AND PGM.basic_discount_id = (SELECT object_id FROM core_settings
                                                                 WHERE code_name = 'discount_for_anonymous')
                                '''
        elif shortcut_field == 'so':
            client_id_join += '''
                LEFT JOIN price_generator_margingroup PGM_DEFAULT
                    ON PGM.sales_price_list_id = spl_id
                    AND PGM.basic_discount_id IS NULL
            '''

    part_filter = ''
    if kwargs.get('part_ids', None):
        part_filter = 'AND PLI.part_id = ANY(%(part_ids)s)'
        kwargs['part_ids'] = kwargs.get('part_ids')

    item_filter = ''
    if kwargs.get('item_id', None):
        item_filter = 'AND PLI.id = %(item_id)s'
        kwargs['item_id'] = kwargs.get('item_id')

    catalog_filters_query = ''
    if kwargs.get('catalog_filters', None):
        catalog_filters_data = kwargs.get('catalog_filters')
        for key, item in catalog_filters_data.iteritems():
            key_string = 'CAP_%s' % key
            catalog_filters_query += '''
                INNER JOIN catalog_acpropertyvalue_parts %s
                    ON %s.part_id = PART.id
                    AND %s.acpropertyvalue_id = ANY(ARRAY[%s]) ''' \
                % (key_string, key_string, key_string, ','.join(['%s' % x for x in item]))

    catalog_part_count = ''
    if kwargs.get('catalog_filters_count', None):
        catalog_filters_count_data = kwargs.get('catalog_filters_count')
        for key, item in catalog_filters_count_data.iteritems():
            key_string = 'CAP_%s' % key
            catalog_part_count += '''
                INNER JOIN catalog_acpropertyvalue_parts %s
                    ON %s.part_id = CAVP.part_id
                    AND (%s.acpropertyvalue_id = ANY(ARRAY[%s])
                        OR (
                            %s.acpropertyvalue_id = CAP.id
                            AND CAP.ac_property_id = %s
                        )
                    )
                ''' \
                % (key_string, key_string, key_string,
                   ','.join(['%s' % x for x in item]), key_string, key)

    fixed_part_id = ''
    if kwargs.get('fixed_part_id', None):
        fixed_part_id = 'PART.id = ANY(%(fixed_part_id_filter)s)'
        kwargs['fixed_part_id_filter'] = kwargs.get('fixed_part_id')
    else:
        fixed_part_id = 'PART.search_number = ANY(%(search_number)s)'

    # remove pli active filter for GetPrice
    pli_active_filter = ''
    if kwargs.get('pli_with_unactive', None):
        pli_active_filter = ''
    else:
        pli_active_filter = 'AND PLI.active = True'

    sales_price_query = ''
    if kwargs.get('sales_price_ids', None):
        sales_price_query = """
            SALES_PRICE_ARRAY AS (
                WITH RM AS (
                    SELECT precision, type
                            FROM superuser_roundingmethod
                            WHERE currency_id = 1
                            ORDER BY (
                                    SELECT value
                                    FROM core_settings
                                    WHERE code_name = 'default_rounding_method_type'
                            ) DESC
                            LIMIT 1
                )
                SELECT
                    SPL.id::INT,
                    SPL.code::TEXT,
                    (SELECT precision FROM RM)::NUMERIC AS precision,
                    (SELECT type FROM RM)::TEXT AS type,
                    NULL::INT AS discount,
                    1::INT AS margin
                FROM spl_salespricelist SPL
                WHERE SPL.id = ANY(%(sales_price_ids)s)
            )
        """
    else:
        sales_price_query = """
            SALES_PRICE_ARRAY AS (
                SELECT * from json_populate_recordset(NULL::sales_prices, %(sales_price)s)
            )
        """

    select_fields_query = '''pli_id, spl_id, spl_code, city, fixed_stock, stock,availability,
        absence, update_time, price, part_id, sales_price, delivery_time,
        guaranteed_delivery_time, pl_id, ratio, item_supplier_id, slug, return_to_supplier,
        part_search_number, part_catalog_number, part_description, tm_show_name'''
    if kwargs.get('select_fields', None):
        select_fields_query = kwargs.get('select_fields')

    try:
        item = kwargs['round_price']
    except KeyError:
        kwargs['round_price'] = True

    if kwargs.get('supplier_id', None) and not kwargs.get('so_id', None):
        join_currency_where = '''OR (SCT.model = 'supplier' AND CCC.object_id = %(supplier_id)s)'''
        join_currency_order = '''WHEN SCT.model = 'supplier' THEN 0'''

    if kwargs.get('cross_search', False):
        cross_part_query = get_part_cross % fixed_part_id
    else:
        cross_part_query = '-- This is cut'

    query = QUERY_MAP[action].format(
            cross_part_query=cross_part_query,
            sorting_query=sorting_query,
            city_filter=city_filter,
            shortcut_query=shortcut_query,
            so_id_filter=so_id_filter,
            part_filter=part_filter,
            item_filter=item_filter,
            client_id_join=client_id_join,
            order_by_query=order_by_query,
            catalog_filters_query=catalog_filters_query,
            catalog_part_count=catalog_part_count,
            fixed_part_id=fixed_part_id,
            pli_active_filter=pli_active_filter,
            sales_price_query=sales_price_query,
            select_fields_query=select_fields_query,
            client_margin_query=client_margin_query,
            client_left_join=client_left_join,
            so_margin_query=so_margin_query,
            so_left_join=so_left_join,
            join_currency_where=join_currency_where,
            join_currency_order=join_currency_order,
            server_filter=server_filter
    )

    return query, kwargs

