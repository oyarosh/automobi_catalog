subquery_constants = """
WITH DEFAULT_DC AS (
    SELECT id FROM users_dc WHERE is_default = True LIMIT 1
),
DC_D_VALUE AS (
    SELECT receiver_object_id, sender_object_id, warehouse_id,  price_list_id,value, guaranteed_value, city AS city_id,
        row_number() OVER ( PARTITION BY receiver_object_id, sender_object_id ORDER BY price_list_id NULLS LAST) AS row
    FROM core_deliverytime DT
    INNER JOIN core_apicontenttype SCT
        ON SCT.id = DT.sender_content_type
    WHERE SCT.model = 'supplier'

),
{client_margin_query}
{so_margin_query}
{sales_price_query}
"""

subquery_select_main_query = """
SELECT
    PLI.id AS pli_id,
    SPL.id AS spl_id,
    SPL.code AS spl_code,
    PLI.city,
    PLT.fixed_stock,
    PLI.stock,
    PLI.update_time,
    CASE
        WHEN %(shortcut)s IN('dc', 'supplier') THEN
            PLI.original_price
        ELSE
            PLI.price
    END AS price,
    PLI.part_id,
    PLI.multiplicity,
    PLI.minimum_order,
    PLI.currency_id,
    PLI.price_list_id,
    PLI.template_id,
    SPA_ROUND.id AS spa_id,
    SPA_ROUND.precision AS spa_precision,
    SPA_ROUND.type AS spa_type,
    SPA.discount AS spa_discount,
    SPA.margin AS spa_margin,
    SPLI.margin AS spli_margin,
    PLI.warehouse_id AS item_warehouse_id,
    PL.supplier_id AS item_supplier_id,
    PL.id AS pl_id,
    PL.order_processing_time,
    PL.guaranteed_order_days,
    SPL.dc_id AS spl_dc_id,
    CASE
        WHEN US.split_statistics_by_ppl THEN
            COALESCE((SELECT value FROM supplier_supplyvaluestatistics WHERE ppl_id = PL.id AND supplier_id = PL.supplier_id LIMIT 1), 1)
        ELSE
            US.supply_value
    END AS supply_value,
    PL.time_for_order_day_in_day,
    PL.return_to_supplier,
    PLI.currency_id AS pli_currency_id
FROM ppl_pricelistitem PLI
JOIN ppl_template PLT
    ON PLT.id = PLI.template_id
JOIN ppl_pricelist PL
    ON PL.id = PLI.price_list_id
JOIN ppl_pricelist_templates PPLT
    ON PPLT.pricelist_id = PLI.price_list_id
    AND PPLT.template_id = PLI.template_id
JOIN spl_salespricelistitem SPLI
    ON SPLI.object_id = PL.id
    AND SPLI.content_type_id = 102
    AND SPLI.deleted = False
JOIN spl_salespricelist SPL
    ON SPL.id = SPLI.sales_price_list_id
JOIN SALES_PRICE_ARRAY AS SPA
    ON SPLI.sales_price_list_id = SPA.id
JOIN core_roundingmethod AS SPA_ROUND
    ON SPA_ROUND.currency_id = %(currency_id)s
JOIN users_supplier2 AS US
    ON PL.supplier_id = US.id
WHERE
    1=1
    {part_filter}
    {item_filter}
    {pli_active_filter}
    {city_filter}

    AND PL.deleted = False
    AND SPL.deleted = False
    AND
    CASE WHEN '{server_filter}' = 'on' THEN
         PLI.server_id = PL.server_id
    ELSE
        1=1
    END
ORDER BY

    SPL.id,
    PLI.part_id,
    PLI.id,
    PLI.update_time DESC,
    PLI.city IS NULL,
    PLI.warehouse_id IS NULL
"""
subquery_select_main_query1 = """
SELECT
	pli_id, spl_id, spl_code, city, fixed_stock, stock,
	update_time, query.price,
	CASE
		WHEN PLS.weight_calculate IS NOT NULL AND PLS.weight_calculate AND %(shortcut)s NOT IN('supplier') THEN
			CASE
				WHEN PLSW.price IS NOT NULL THEN
					CASE
						WHEN PART.weight_volume IS NOT NULL AND PART.weight_volume > 0 THEN
							CASE
                                WHEN COALESCE(PART.weight, DEFAULT_WEIGHT.weight, NULL) IS NOT NULL AND COALESCE(PART.weight, DEFAULT_WEIGHT.weight, NULL) > 0 THEN
                                    CASE
                                        WHEN COALESCE(PART.weight, DEFAULT_WEIGHT.weight, 0) <= PART.weight_volume THEN
                                            CASE WHEN PLS.weight_option = 'min' AND COALESCE(PART.weight, DEFAULT_WEIGHT.weight, 0) != 0 THEN
                                                (COALESCE(PART.weight, DEFAULT_WEIGHT.weight, 0) * PLSW.price) * COALESCE(PLS.gross_rate, 1)
                                            WHEN PLS.weight_option = 'max' THEN
                                                (COALESCE(PART.weight, DEFAULT_WEIGHT.weight, 0) * COALESCE(PLS.gross_rate, 1) * PLSW.price + (PART.weight_volume - (COALESCE(PART.weight, DEFAULT_WEIGHT.weight, 0) * COALESCE(PLS.gross_rate, 1))) * PLSW.price_value)
                                            ELSE
                                                (COALESCE(PART.weight, DEFAULT_WEIGHT.weight, 0) * COALESCE(PLS.gross_rate, 1) * PLSW.price + (PART.weight_volume - (COALESCE(PART.weight, DEFAULT_WEIGHT.weight, 0) * COALESCE(PLS.gross_rate, 1))) * PLSW.price_value)
                                            END
                                        ELSE
                                            CASE WHEN PLS.weight_option = 'in' AND PART.weight_volume != 0 THEN
                                                (COALESCE(PART.weight, DEFAULT_WEIGHT.weight, 0) * COALESCE(PLS.gross_rate, 1) * PLSW.price + (PART.weight_volume - (COALESCE(PART.weight, DEFAULT_WEIGHT.weight, 0) * COALESCE(PLS.gross_rate, 1))) * PLSW.price_value)
                                            WHEN PLS.weight_option = 'max' THEN
                                                (COALESCE(PART.weight, DEFAULT_WEIGHT.weight, 0) * PLSW.price) * COALESCE(PLS.gross_rate, 1)
                                            ELSE
                                                (COALESCE(PART.weight, DEFAULT_WEIGHT.weight, 0) * PLSW.price) * COALESCE(PLS.gross_rate, 1)
                                            END
                                        END
                                ELSE
                                    PLSW.price_value * PART.weight_volume * COALESCE(PLS.gross_rate, 1)
                                END
						ELSE
							CASE
								WHEN COALESCE(PART.weight, DEFAULT_WEIGHT.weight, NULL) IS NOT NULL AND COALESCE(PART.weight, DEFAULT_WEIGHT.weight, NULL) > 0 THEN
									PLSW.price * COALESCE(PART.weight, DEFAULT_WEIGHT.weight, NULL) * COALESCE(PLS.gross_rate, 1)
								ELSE
									0
							END
						END
				ELSE
					0
			END
		ELSE
			0
	END AS item_weight_price,
	part_id, multiplicity, minimum_order, pli_currency_id, query.price_list_id,
	template_id, spa_id, spa_precision, spa_type, spa_discount,
	spa_margin, spli_margin, item_warehouse_id, item_supplier_id, pl_id, order_processing_time,
	guaranteed_order_days, spl_dc_id,
	CASE WHEN query.supply_value = 0 THEN
	    100
	ELSE
	    ROUND(query.supply_value * 100)
	END AS availability,
	CASE WHEN query.supply_value = 0 THEN
	    0
	ELSE
        ROUND(100 - (query.supply_value * 100))
    END AS absence,
    supply_value, time_for_order_day_in_day, return_to_supplier

FROM (
    """ + subquery_select_main_query + """
) AS query

JOIN catalog_part AS PART
	ON PART.id = part_id
LEFT JOIN price_generator_weightpricesettings AS DEFAULT_WEIGHT
    ON DEFAULT_WEIGHT.dc_id=spl_dc_id
	AND pli_currency_id = DEFAULT_WEIGHT.weight_price_currency_id
	AND query.price >= DEFAULT_WEIGHT.min_price
	AND query.price < DEFAULT_WEIGHT.max_price
LEFT JOIN price_generator_pricelistsettings AS PLS
	ON PLS.dc_id=spl_dc_id
	AND PLS.price_list_id = pl_id
LEFT JOIN price_generator_pricelistweightprice AS PLSW
	ON PLSW.dc_id=spl_dc_id
	AND PLSW.price_list_id = pl_id
	AND PLSW.currency_id = pli_currency_id
"""

subquery_select_main_query2 = """
    SELECT
        pli_id, spl_id, spl_code, city, fixed_stock, stock, availability, absence,
        update_time,
        CASE
            WHEN pli_currency_id != %(currency_id)s AND %(currency_id)s = %(basic_currency_id)s THEN
                CASE
                    WHEN CR.currency_ratio IS NOT NULL THEN
                        CASE WHEN %(shortcut)s IN('dc') THEN
                            (price * CR.currency_ratio) / CUR_ARRAY.value
                        ELSE
                            (price * CR.currency_ratio + item_weight_price) / CUR_ARRAY.value
                        END
                    ELSE
                        CASE WHEN %(shortcut)s IN('dc') THEN
                            price / CUR_ARRAY.value
                        ELSE
                            (price + item_weight_price) / CUR_ARRAY.value
                        END
                END
            WHEN  pli_currency_id != %(currency_id)s AND %(currency_id)s != %(basic_currency_id)s THEN
                CASE
                    WHEN CR.currency_ratio IS NOT NULL THEN
                        CASE WHEN %(shortcut)s IN('dc') THEN
                            (price * CR.currency_ratio) / COALESCE(CUR_ARRAY.value, 1) * CUR_ARRAY_BASIC.value
                        ELSE
                            (price * CR.currency_ratio + item_weight_price) / COALESCE(CUR_ARRAY.value, 1) * CUR_ARRAY_BASIC.value
                        END
                    ELSE
                        CASE WHEN %(shortcut)s IN('dc') THEN
                            price / COALESCE(CUR_ARRAY.value, 1) * CUR_ARRAY_BASIC.value
                        ELSE
                            (price + item_weight_price) / COALESCE(CUR_ARRAY.value, 1) * CUR_ARRAY_BASIC.value
                        END

                END
            ELSE
                CASE
                    WHEN CR.currency_ratio IS NOT NULL THEN
                        CASE WHEN %(shortcut)s IN('dc') THEN
                            price * CR.currency_ratio
                        ELSE
                            price * CR.currency_ratio + item_weight_price
                        END
                    ELSE
                        CASE WHEN %(shortcut)s IN('dc') THEN
                            price
                        ELSE
                            price + item_weight_price
                        END
                END
        END as price,
        CASE
            WHEN pli_currency_id != %(currency_id)s AND %(currency_id)s = %(basic_currency_id)s THEN
                CASE
                    WHEN CR.currency_ratio IS NOT NULL THEN
                        (price * CR.currency_ratio + item_weight_price) / CUR_ARRAY.value
                    ELSE
                        (price + item_weight_price) / CUR_ARRAY.value
                END
            WHEN  pli_currency_id != %(currency_id)s AND %(currency_id)s != %(basic_currency_id)s THEN
                CASE
                    WHEN CR.currency_ratio IS NOT NULL THEN
                        (price * CR.currency_ratio + item_weight_price) / COALESCE(CUR_ARRAY.value, 1) * CUR_ARRAY_BASIC.value
                    ELSE
                        (price + item_weight_price) / COALESCE(CUR_ARRAY.value, 1) * CUR_ARRAY_BASIC.value
                END
            ELSE
                CASE
                    WHEN CR.currency_ratio IS NOT NULL THEN
                        price * CR.currency_ratio + item_weight_price
                    ELSE
                        price + item_weight_price
                END
        END as purchase_price,
        part_id, multiplicity, minimum_order, pli_currency_id, price_list_id,
        template_id, spa_id, spa_precision, spa_type, spa_discount,
        spa_margin, CUR_ARRAY.value AS cur_value, spli_margin,
        item_warehouse_id, item_supplier_id, pl_id, order_processing_time,
        guaranteed_order_days, spl_dc_id, supply_value,
        time_for_order_day_in_day, return_to_supplier
    FROM
    (
        """ + subquery_select_main_query1 + """
    ) AS query2
    LEFT JOIN DEFAULT_DC AS DEFAULT_DC ON True
    LEFT JOIN LATERAL (
        SELECT CCC.value
        FROM core_currencysettings AS CCC
        INNER JOIN core_apicontenttype SCT
            ON SCT.id = CCC.content_type_id
        WHERE
            (
                (SCT.model = 'dc' AND CCC.object_id = spl_dc_id)
                {join_currency_where}
                OR (SCT.model = 'dc' AND CCC.object_id = DEFAULT_DC.id)
            )
            AND CCC.second_id = pli_currency_id
            AND CCC.date <= now()::DATE
        ORDER BY
            CASE
                {join_currency_order}
                WHEN SCT.model = 'dc' AND CCC.object_id != DEFAULT_DC.id THEN
                    1
                WHEN SCT.model = 'dc' AND CCC.object_id = DEFAULT_DC.id THEN
                    2
            END NULLS LAST,
            date DESC
        LIMIT 1
    ) CUR_ARRAY ON TRUE
    LEFT JOIN LATERAL (
        SELECT CCC.value
        FROM core_currencysettings AS CCC
        INNER JOIN core_apicontenttype SCT
            ON SCT.id = CCC.content_type_id
        WHERE
            (
                (SCT.model = 'dc' AND CCC.object_id = spl_dc_id)
                {join_currency_where}
                OR (SCT.model = 'dc' AND CCC.object_id = DEFAULT_DC.id)
            )
            AND CCC.second_id = %(currency_id)s
            AND CCC.date <= now()::DATE
        ORDER BY
            CASE
                {join_currency_order}
                WHEN SCT.model = 'dc' AND CCC.object_id != DEFAULT_DC.id THEN
                    1
                WHEN SCT.model = 'dc' AND CCC.object_id = DEFAULT_DC.id THEN
                    2
            END NULLS LAST,
            date DESC
        LIMIT 1
    ) CUR_ARRAY_BASIC ON TRUE
    LEFT JOIN LATERAL (
        SELECT CR.id, CR.currency_ratio, CR.timestamp, CR.currency_id, CR.dc_id, CR.supplier_id
        FROM cabinet_supplierdccurrencyrelation CR
        WHERE CR.dc_id = spl_dc_id
            AND CR.supplier_id = item_supplier_id
            AND CR.currency_id = pli_currency_id
            AND CR.timestamp <= update_time
        ORDER BY
            timestamp DESC
        LIMIT 1
    ) AS CR ON TRUE
"""


subquery_select_margin_formating = """
SELECT DISTINCT ON (pli_id, spl_id, part_id, pl_id, item_supplier_id, spl_dc_id, price, spa_discount)
    pli_id, spl_id, spl_code, city, fixed_stock, stock, availability,
    absence, update_time, sales_price, price, delivery_time, guaranteed_delivery_time,
    part_id, spa_precision, spa_type, pl_id, pl_supply_value,
    spl_dc_id, item_supplier_id, time_for_order_day_in_day,
    slug, return_to_supplier, part_search_number, part_catalog_number,
    part_description, part_slug, tm_show_name, order_processing_time
FROM (
SELECT
    pli_id, spl_id, spl_code, city, fixed_stock, stock, availability,
    absence, update_time,
    {shortcut_query}
    part_id, spa_precision, spa_type, pl_id, supply_value AS pl_supply_value,
    spl_dc_id, item_supplier_id, time_for_order_day_in_day,
    PART.slug AS slug,
    return_to_supplier,
    PART.search_number AS part_search_number,
    PART.catalog_number AS part_catalog_number,
    PART.description AS part_description,
    PART.slug AS part_slug,
    TM.show_name AS tm_show_name,
    spa_discount, order_processing_time
FROM (
    """ + subquery_select_main_query2 + """
) AS main_query

LEFT JOIN catalog_part PART
    ON part_id = PART.id
LEFT JOIN catalog_trademark TM
    ON PART.tm_id = TM.id

{client_left_join}
{client_id_join}
LEFT JOIN price_generator_warehouse WH
    ON WH.id = item_warehouse_id

    {so_left_join}


LEFT JOIN (
    SELECT receiver_object_id, sender_object_id, warehouse_id,  price_list_id,value, guaranteed_value, city AS city_id
        FROM core_deliverytime DT
        INNER JOIN core_apicontenttype SCT
            ON SCT.id = DT.sender_content_type
        INNER JOIN core_apicontenttype RCT
            ON RCT.id = DT.receiver_content_type
        WHERE SCT.model = 'dc'
            AND RCT.model = 'so'
        ORDER BY DT.price_list_id IS NOT NULL,
            DT.city IS NOT NULL,
            DT.warehouse_id IS NOT NULL
    ) AS OS_D_VALUE
    ON (
        OS_D_VALUE.sender_object_id = spl_dc_id
        {so_id_filter}
        AND (OS_D_VALUE.warehouse_id = item_warehouse_id OR OS_D_VALUE.warehouse_id IS NULL)
        AND (OS_D_VALUE.price_list_id = pl_id OR OS_D_VALUE.price_list_id IS NULL)
        AND (OS_D_VALUE.city_id = WH.city_id OR OS_D_VALUE.city_id IS NULL)
    )
WHERE
    spa_id IS NOT NULL
    AND (PGM.tm_id IS NULL OR PGM.tm_id = TM.id)
    AND (PGM.warehouse_id IS NULL OR PGM.warehouse_id = item_warehouse_id)
    AND (PGM.city_id IS NULL OR PGM.city_id = WH.city_id)
    AND (PGM.deleted = False OR PGM.deleted IS NULL)
ORDER BY
    pli_id, spl_id, part_id, pl_id, item_supplier_id, spl_dc_id, price, spa_discount,
    0
    + (CASE WHEN PGM.tm_id IS NULL THEN 0 ELSE 1 END)
    + (CASE WHEN PGM.warehouse_id IS NULL THEN 0 ELSE 1 END)
    + (CASE WHEN PGM.city_id IS NULL THEN 0 ELSE 1 END)
    DESC
) AS query2
"""

subquery_select_round_formating = """
SELECT
    pli_id, spl_id, spl_code, city, fixed_stock, stock,
    availability, absence, update_time,
    CASE
        WHEN NOT %(round_price)s THEN
            price
        WHEN spa_type IS NULL THEN
            ROUND(price, 4)
        WHEN spa_type = 'closest' THEN
            ROUND(ROUND(price / spa_precision) * spa_precision, 4)
        WHEN spa_type = 'max' THEN
            ROUND(CEIL(price / spa_precision) * spa_precision, 4)
        WHEN spa_type = 'min' THEN
            ROUND(FLOOR(price / spa_precision) * spa_precision, 4)
    END AS price,
    part_id,
    CASE
        WHEN NOT %(round_price)s THEN
            sales_price
        WHEN spa_type IS NULL THEN
            ROUND(sales_price, 4)
        WHEN spa_type = 'closest' THEN
            ROUND(ROUND(sales_price / spa_precision) * spa_precision, 4)
        WHEN spa_type = 'max' THEN
            ROUND(CEIL(sales_price / spa_precision) * spa_precision, 4)
        WHEN spa_type = 'min' THEN
            ROUND(FLOOR(sales_price / spa_precision) * spa_precision, 4)
    END AS sales_price,
    CASE
        WHEN CAST(COALESCE(order_processing_time::INTEGER, 0) AS integer) = 0
            AND time_for_order_day_in_day IS NOT NULL
            AND LOCALTIME > time_for_order_day_in_day
                THEN delivery_time + 1
        ELSE
                delivery_time
    END AS delivery_time,

    CASE
        WHEN CAST(COALESCE(order_processing_time::INTEGER, 0) AS integer) = 0
            AND time_for_order_day_in_day IS NOT NULL
            AND LOCALTIME > time_for_order_day_in_day
                THEN guaranteed_delivery_time + 1
        ELSE
                guaranteed_delivery_time
    END AS guaranteed_delivery_time,
    pl_id,
    COUNT(*) OVER (PARTITION BY spl_id, part_id) AS best_count,
    (CASE
        WHEN price_value = 'min' THEN
            CASE WHEN MIN(sales_price) OVER (PARTITION BY spl_id, part_id) = sales_price THEN
                UPFS.price_weight
            ELSE 0 END
        WHEN price_value = 'max' THEN
            CASE WHEN MAX(sales_price) OVER (PARTITION BY spl_id, part_id) = sales_price THEN
                UPFS.price_weight
            ELSE 0 END
    ELSE 0 END
    +
    CASE WHEN pl_supply_value >= UPFS.supply_value THEN
        UPFS.supply_weight
    ELSE 0 END
    +
    CASE WHEN delivery_time <= UPFS.delivery_value THEN
        UPFS.delivery_weight
    ELSE 0 END
    ) AS ratio,
    item_supplier_id,
    slug,
    return_to_supplier,
    part_search_number, part_catalog_number, part_description, tm_show_name, part_slug
FROM
(
    """ + subquery_select_margin_formating + """
) AS query
LEFT JOIN spl_priceformationsettings UPFS
    ON spl_dc_id = UPFS.dc_id
"""


subquery_select_final_result = subquery_constants + """
SELECT
    {select_fields_query}
FROM
(
    SELECT DISTINCT ON (spl_id, part_id)
        pli_id, spl_id, spl_code, city, fixed_stock, stock, availability,
        absence, update_time, price, part_id, sales_price, delivery_time,
        guaranteed_delivery_time, pl_id, ratio, item_supplier_id, slug, return_to_supplier,
        part_search_number, part_catalog_number, part_description, tm_show_name, part_slug
    FROM
    (
        """ +subquery_select_round_formating+ """
    ) as result
    ORDER BY spl_id, part_id, ratio DESC
) as final_result
{sorting_query}
"""